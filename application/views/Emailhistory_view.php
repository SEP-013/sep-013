<?php  
        $session_data = $this->session->userdata('logged_in');
        $UserType= $session_data['UserType'];
        ?>
            
        <div class="content">
        
        <div class="header">
            <?php if($UserType==0):?>
            <h1 class="page-title">Admin Dashboard</h1>
            <?php endif;?>
            <?php if($UserType==1):?>
            <h1 class="page-title">Chair Panel</h1>
            <?php endif;?>
            <?php if($UserType==2):?>
            <h1 class="page-title">Reviewer Panel</h1>
            <?php endif;?>
            <?php if($UserType==3):?>
            <h1 class="page-title">Author Panel</h1>
            <?php endif;?>
        </div>
          <ul class="breadcrumb">
            <li><a href="#"></a> <span class="divider"></span></li>
            <li><a href="#"></a> <span class="divider"></span></li>
            <li class="active"></li>
        </ul>
             

        <div class="container-fluid">
            <div class="row-fluid">
                    

    <div class="well">
    <ul class="nav nav-tabs">
      <li class="active"><a href="" data-toggle="tab">Conference Details</a></li>
      
    </ul>
    <div id="myTabContent" class="tab-content">
      <div class="tab-pane active in" id="home">         
    <?php                  foreach ($row as $result){ ?>
          <table class="table table-bordered table-hover">
              <tr>
                <td>Send to</td>
                <td><?php echo $result->SentTo;?></td>
             </tr>     
              <tr>
                <td>Subject</td>
                <td><?php echo $result->Subject;?></td>
             </tr>
              <tr>
                <td>Message</td>
                <td><?php echo $result->Message;?></td>
             </tr>
             <tr>
                <td> Date</td>
                <td><?php echo $result->Date;?></td>
             </tr>
          </table>
   <?php } ?>  
      </div>
      <div class="tab-pane fade" id="profile">
         
      </div>
  </div>

</div>
  <script src="<?php echo base_url(); ?>lib/bootstrap/js/bootstrap.js"></script>
    <script src="<?php echo base_url(); ?>lib/jquery-1.7.2.min.js"></script>

