<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
<script type="text/javascript" src=" https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
  
 <?php  
        $session_data = $this->session->userdata('logged_in');
        $UserType= $session_data['UserType'];
        ?>
            
        <div class="content">
        
        <div class="header">
            <?php if($UserType==0):?>
            <h1 class="page-title">Admin Dashboard</h1>
            <?php endif;?>
            <?php if($UserType==1):?>
            <h1 class="page-title">Chair Panel</h1>
            <?php endif;?>
            <?php if($UserType==2):?>
            <h1 class="page-title">Reviewer Panel</h1>
            <?php endif;?>
            <?php if($UserType==3):?>
            <h1 class="page-title">Author Panel</h1>
            <?php endif;?>
        </div>
          <ul class="breadcrumb">
            <li><a href="#"></a> <span class="divider"></span></li>
            <li><a href="#"></a> <span class="divider"></span></li>
            <li class="active"></li>
        </ul>
          
        <div class="container-fluid">
            <div class="row-fluid">
                    

    <div class="well">
    <ul class="nav nav-tabs">
      <li class="active"><a href="" data-toggle="tab">Review Form Configuration </a></li>
      
    </ul>
          <a href="<?php echo site_url('import_controller/export');?>">Site Setup     |      </a>
        <a href="<?php echo site_url('SiteSetup_Controller/index');?>">Manage Deadline    |      </a>
        <a href="<?php echo site_url('siteSetup_controller/loadtrack');?>">Add Track   |      </a>
         <a href="<?php echo site_url('reviewConf_controller/load');?>">Configure Review Form  </a><br/> <br/>  

    <div id="myTabContent" class="tab-content">
      <div class="tab-pane active in" id="home">
          
       
        <?php echo form_open_multipart('siteSetup_controller/Addtrack');
           
          ?>
           <?php echo $this->session->flashdata('feedback');?>
           <?php if(validation_errors()):?>
             <div class='alert alert-danger alert-dismissable'><?php echo validation_errors(); ?></div>
         <?php endif;?>
         
        
         <input type="button" id="addField" value="Add another track" class="btn btn-primary"/>
         <br>
         <label>Track Name</label>
        <div id="fields">
        <input type="text"  name="fields[track][]" id="fields[track][]" class="input-xlarge"> 
        
       
         <br/>
        </div>
        
        
       <input type="submit" value="Submit" id="submit" name="submit" class="btn btn-primary">
    
    </form>                                
    <script>
        $(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $("#fields"); //Fields wrapper
    var add_button      = $("#addField"); //Add button ID
    
    var x = 3; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div><label>Track Name</label><input type="text" name="fields[track][]" class="input-xlarge"/><a href="#" class="remove_field">Remove</a><br>\n</div>');
			
        }
    });
   
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x-=2;
    })
});
        </script>
    
      </div>
      <div class="tab-pane fade" id="profile">
         
      </div>
  </div>

</div>
  <script src="<?php echo base_url(); ?>lib/bootstrap/js/bootstrap.js"></script>
    <script src="<?php echo base_url(); ?>lib/jquery-1.7.2.min.js"></script>

            