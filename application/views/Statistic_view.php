  

<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
<?php  
        $session_data = $this->session->userdata('logged_in');
        $UserType= $session_data['UserType'];
        ?>
            
        <div class="content">
        
        <div class="header">
            <?php if($UserType==0):?>
            <h1 class="page-title">Admin Dashboard</h1>
            <?php endif;?>
            <?php if($UserType==1):?>
            <h1 class="page-title">Chair Panel</h1>
            <?php endif;?>
            <?php if($UserType==2):?>
            <h1 class="page-title">Reviewer Panel</h1>
            <?php endif;?>
            <?php if($UserType==3):?>
            <h1 class="page-title">Author Panel</h1>
            <?php endif;?>
        </div>
          <ul class="breadcrumb">
            <li><a href="#"></a> <span class="divider"></span></li>
            <li><a href="#"></a> <span class="divider"></span></li>
            <li class="active"></li>
        </ul>
             

        <div class="container-fluid">
            <div class="row-fluid">
                    

    <div class="well">
    <ul class="nav nav-tabs">
      <li class="active"><a href="" data-toggle="tab">Statistics</a></li>
      
    </ul>
    <div id="myTabContent" class="tab-content">
      <div class="tab-pane active in" id="home">
         <font color="#16875C" size="+1"><u><b>Authors and Reviewers</b></u> </font><br><br>

              
        <?php if ($Reviewer){ ?>
             <table class="table table-bordered table-hover">
             <thead>
			      <tr>
			        <th>Reviewer id</th>
			        <th>Reveiwer UserName</th>         
			      </tr>
               </thead>

				<?php $countofreviewers=0; foreach($Reviewer as $result)
							  {
								  $usertype=$result->UserType;
								  $total=0; ?>
								  
					 	<?php		if($usertype==2){ 
							 	?>		  
									<tr> <td> <?php echo $result->id;?> </td>
                                   <td> <?php echo $result->UserName;
										$countofreviewers =$countofreviewers+1; 	?></td></tr>
                    		<?php   }  ?>        
            		<?php } 
		if(!$usertype){?>
        				<tr><td>No Reviewers</td></tr>
 				<?php }?>
                    </table>
 
    								<table class="table table-bordered table-hover">
             <thead>

			      <tr>
			        <th>Author id</th>
			        <th>Author UserName</th>         
			      </tr>
               </thead>
 					<?php $countofauthors=0; foreach($Reviewer as $result)
							  {
								  $usertype=$result->UserType;
					 			if($usertype==3){ 
							 	?>		  
									<tr> <td> <?php echo $result->id;?> </td>
                                   <td> <?php echo $result->UserName;
										$countofauthors =$countofauthors+1; ?></td></tr>
                    		<?php   }  ?>        
            		<?php } if(!$usertype){?>
        				<tr><td>No authors</td></tr>
 				<?php }?>
                    </table>


<br>
<?php echo "count of authors :",$countofauthors;
		
		echo "<br>";
		echo "count of reviewers :",$countofreviewers;
		echo "<br>";
		echo "<br>";
	if($Reviewer){
		$total = $countofauthors+$countofreviewers;
		$author=($countofauthors/$total)*100;
		echo "Authors :",$author," %";
		echo "<br>";
		$reviewer=($countofreviewers/$total)*100;
		echo "Reviewers :",$reviewer," %";
	}
	if(!$Reviewer){echo "no display";}
		?>			 
<?php } ?>                   
   
    <html>
  <head>
    <!--Load the AJAX API-->
            
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">

      // Load the Visualization API and the piechart package.
      google.load('visualization', '1.0', {'packages':['corechart']});

      // Set a callback to run when the Google Visualization API is loaded.
      google.setOnLoadCallback(drawChart);

      // Callback that creates and populates a data table,
      // instantiates the pie chart, passes in the data and
      // draws it.
      function drawChart() {

        // Create the data table.
		var reviewer=<?php echo $reviewer;?>;
		var author=<?php echo $author;?>;
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Slices');
        data.addRows([
          ['Reviewer', reviewer],
		  ['Author', author],
         
        ]);

        // Set chart options
        var options = {'title':'Authors and Reveiwers in this conference',
                       is3D: true,};

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
        chart.draw(data, options);
      }
    </script>
  </head>

  <body>
    <!--Div that will hold the pie chart-->
    <div id="piechart_3d"></div>
  </body>
</html>
    
   <font color="#16875C" size="+1"><u><b>PAPERS</b></u> </font><br><br>
             
         
    <?php if($papers1){
		
		$countofactive=0;
		$countofdelete=0; 
				foreach($papers1 as $paper)
				{
					echo "<table> <tr>";
			$status=$paper->Status;
				if($status=='A'){
								echo "<td>";
								echo $paper->ID," Active";
								echo "<br>";
								$countofactive=$countofactive+1;
								echo "</td>";
								}	
				
				elseif($status=='D')
								{
								echo "<td>";
								echo $paper->ID," Deleted";
								echo "<br>";
								$countofdelete=$countofdelete+1;
								echo "</td>";
								}
						
							echo"</tr>";
						echo"</table>";
				}		
				echo "<br>";
			echo $countofactive," active papers.";
				echo "<br>";
			echo $countofdelete," deleted papers";
				echo "<br>";
		}
		
		if(!$papers1) echo "No papers";
	
	if($papers1){	
	$totalpapers=$countofactive+$countofdelete;
	$activepercent=($countofactive/$total)/100;
	$deletepercent=($countofdelete/$total)/100;
	echo "Papers Active",$activepercent," %";
	echo "<br>";
	echo "Papers Deleted",$deletepercent," %";
	}
		?>
        <head>
    <!--Load the AJAX API-->
            
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">

      // Load the Visualization API and the piechart package.
      google.load('visualization', '1.0', {'packages':['corechart']});

      // Set a callback to run when the Google Visualization API is loaded.
      google.setOnLoadCallback(drawChart);

      // Callback that creates and populates a data table,
      // instantiates the pie chart, passes in the data and
      // draws it.
      function drawChart() {

			    // Create the data table.2
		var author=<?php echo $author;?>;
		var reviewer=<?php echo $reviewer;?>;
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Slices');
        data.addRows([
          ['Authors', author],
		  ['Reviewers', reviewer],
         
        ]);

        // Create the data table.
		var active=<?php echo $activepercent;?>;
		var deletes=<?php echo $deletepercent;?>;
        var data2 = new google.visualization.DataTable();
        data2.addColumn('string', 'Topping');
        data2.addColumn('number', 'Slices');
        data2.addRows([
          ['active', active],
		  ['deleted', deletes],
         
        ]);
	

        // Set chart options
        var options = {'title':'Authors and Reviewers for this conference',"width": 630,
                "height": 440,
                       is3D: true,};
		var options2 = {'title':'papers Active and deleted in this conference',"width": 630,
                "height": 440,
                       is3D: true,};
		   

        // Instantiate and draw our chart, passing in some options.
         var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
        chart.draw(data, options);
		var chart2 = new google.visualization.PieChart(document.getElementById('piechart_3d2'));
        chart2.draw(data2, options2);
		    
      }
    </script>
  </head>

  <body>
    <!--Div that will hold the pie chart-->
    <div id="piechart_3d"></div>
    <div id="piechart_3d2"></div>
    
  </body>
        
     
    
 <?php ////////////////////////////// ?>       
         </div>
      <div class="tab-pane fade" id="profile">
         
      </div>
  </div>

</div>
  <script src="<?php echo base_url(); ?>lib/bootstrap/js/bootstrap.js"></script>
    <script src="<?php echo base_url(); ?>lib/jquery-1.7.2.min.js"></script>
