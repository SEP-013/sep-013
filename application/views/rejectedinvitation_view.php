<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
<?php  
        $session_data = $this->session->userdata('logged_in');
        $UserType= $session_data['UserType'];
        ?>
            
        <div class="content">
        
        <div class="header">
            <?php if($UserType==0):?>
            <h1 class="page-title">Admin Dashboard</h1>
            <?php endif;?>
            <?php if($UserType==1):?>
            <h1 class="page-title">Chair Panel</h1>
            <?php endif;?>
            <?php if($UserType==2):?>
            <h1 class="page-title">Reviewer Panel</h1>
            <?php endif;?>
            <?php if($UserType==3):?>
            <h1 class="page-title">Author Panel</h1>
            <?php endif;?>
        </div>
          <ul class="breadcrumb">
            <li><a href="#"></a> <span class="divider"></span></li>
            <li><a href="#"></a> <span class="divider"></span></li>
            <li class="active"></li>
        </ul>
             

        <div class="container-fluid">
            <div class="row-fluid">
                    

    <div class="well">
    <ul class="nav nav-tabs">
      <li class="active"><a href="" data-toggle="tab">Add reviewers</a></li>
      
    </ul>
        <a href="<?php echo site_url('import_controller/index');?>">Pending Request    |      </a>
        <a href="<?php echo site_url('import_controller/AcceptReq');?>">Accepted  Request   |      </a>
         <a href="<?php echo site_url('import_controller/RejectReq');?>">Rejected  Request  </a><br/> <br/>
    <div id="myTabContent" class="tab-content">
      <div class="tab-pane active in" id="home">
              
       
      
      <?php if (isset($error)): ?>
    <div class="alert alert-error"><?php echo $error; ?></div>
    <?php endif; ?>
    <?php if ($this->session->flashdata('success') == TRUE): ?>
    <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
    <?php endif; ?>
         
           
         
        <br><br>
    <table class="table table-striped table-hover table-bordered">
    <caption>Rejected Request </caption>
    <thead>
    <tr>
    <th>First Name</th>
    <th>Last Name</th>
    <th>Phone</th>
    <th>Email</th>
    </tr>
    </thead>
    <tbody>
    <?php if ($addressbook == FALSE): ?>
    <tr><td colspan="4">There are currently No Pending request</td></tr>
    <?php else: ?>
    <?php foreach ($addressbook as $row): ?>
    <tr>
    <td><?php echo $row['firstname']; ?></td>
    <td><?php echo $row['lastname']; ?></td>
    <td><?php echo $row['phone']; ?></td>
    <td><?php echo $row['email']; ?></td>
    </tr>
    <?php endforeach; ?>
    <?php endif; ?>
    </tbody>
    </table>
        
      </div>
      <div class="tab-pane fade" id="profile">
          
      </div>
  </div>

</div>
  <script src="<?php echo base_url(); ?>lib/bootstrap/js/bootstrap.js"></script>
    <script src="<?php echo base_url(); ?>lib/jquery-1.7.2.min.js"></script>
