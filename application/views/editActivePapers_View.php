<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
<?php  
        $session_data = $this->session->userdata('logged_in');
        $UserType= $session_data['UserType'];
        ?>
            
        <div class="content">
        
        <div class="header">
            <?php if($UserType==0):?>
            <h1 class="page-title">Admin Dashboard</h1>
            <?php endif;?>
            <?php if($UserType==1):?>
            <h1 class="page-title">Chair Panel</h1>
            <?php endif;?>
            <?php if($UserType==2):?>
            <h1 class="page-title">Reviewer Panel</h1>
            <?php endif;?>
            <?php if($UserType==3):?>
            <h1 class="page-title">Author Panel</h1>
            <?php endif;?>
        </div>
          <ul class="breadcrumb">
            <li><a href="#"></a> <span class="divider"></span></li>
            <li><a href="#"></a> <span class="divider"></span></li>
            <li class="active"></li>
        </ul>
             

        <div class="container-fluid">
            <div class="row-fluid">
                    

    <div class="well">
    <ul class="nav nav-tabs">
      <li class="active"><a href="" data-toggle="tab">Edit Papers</a></li>
      
    </ul>
    <div id="myTabContent" class="tab-content">
      <div class="tab-pane active in" id="home">
              
         <?php echo form_open('chairViewPapers_controller/edit/.$id'); ?>
         <?php if(validation_errors()):?>
             <div class='alert alert-danger alert-dismissable'><?php echo validation_errors(); ?></div>
        <?php endif;?>
         <label>Title</label>
        <input type="text" value="<?php echo $Title;?>" id="title" name="title" class="input-xlarge">  
        <label>Author</label>
        <input type="text" value="<?php echo $Author;?>" id="author" name="author" class="input-xlarge">       
         <label>Primary Contact</label>
        <input type="text" value="<?php echo $Primarycontact;?>" id="pc" name="pc" class="input-xlarge"> <br/><br/>      
        <input type="submit" value="Save" id="submit" name="submit" class="btn btn-primary">    
        
      
        
    </form>                                
    
      </div>
      <div class="tab-pane fade" id="profile">
         
      </div>
  </div>

</div>
  <script src="<?php echo base_url(); ?>lib/bootstrap/js/bootstrap.js"></script>
    <script src="<?php echo base_url(); ?>lib/jquery-1.7.2.min.js"></script>
