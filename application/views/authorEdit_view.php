<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
<?php  
        $session_data = $this->session->userdata('logged_in');
        $UserType= $session_data['UserType'];
        ?>
            
        <div class="content">
        
        <div class="header">
            <?php if($UserType==0):?>
            <h1 class="page-title">Admin Dashboard</h1>
            <?php endif;?>
            <?php if($UserType==1):?>
            <h1 class="page-title">Chair Panel</h1>
            <?php endif;?>
            <?php if($UserType==2):?>
            <h1 class="page-title">Reviewer Panel</h1>
            <?php endif;?>
            <?php if($UserType==3):?>
            <h1 class="page-title">Author Panel</h1>
            <?php endif;?>
        </div>
          <ul class="breadcrumb">
            <li><a href="#"></a> <span class="divider"></span></li>
            <li><a href="#"></a> <span class="divider"></span></li>
            <li class="active"></li>
        </ul>
             

        <div class="container-fluid">
            <div class="row-fluid">
                    

    <div class="well">
    <ul class="nav nav-tabs">
      <li class="active"><a href="" data-toggle="tab">Edit Papers</a></li>
      
    </ul>
    <div id="myTabContent" class="tab-content">
      <div class="tab-pane active in" id="home">
               <?php echo $this->session->flashdata('feedback');?>
         <?php if ($row){ ?>
                                      <table class="table table-bordered table-hover">
             <thead>

			      <tr>
			        <th>Paper ID</th>
			        <th>File Name</th>
			        <th>Title</th>
                                 <th>Abstract</th>
                               <th>Primary Contact</th>
                              
                               <th>Action</th>
			         </tr>
               </thead>
              
                 <?php foreach($row as $result)
				  {
                                    $dcision=$result->Decission;
			 ?>
			        <tr>
                                    <td><?php echo $result->ID;?></td>
                                    <td><?php echo $result->FileName;?></td>
                                    <td><?php echo $result->Title;?></td>
                                     <td><?php echo $result->Abstract;?></td>
                                    <td><?php echo $result->Primarycontact;?></td>
                                    
                                    <td>
                                     <div class="btn-group">
                                        <a class="btn btn" href="<?php echo site_url('authorPaperEdit_controller/load/'.$result->ID);?>"><i class="icon-pencil icon-white"></i> Edit</a>
                                       <a class="btn btn" href="<?php echo site_url('authorPaperEdit_controller/download/'.$result->ID);?>"><i class="icon-download icon-white"></i> Download</a>
                                       <a class="btn btn-danger" href="<?php echo site_url('authorPaperEdit_controller/Delete/'.$result->ID);?>"><i class="icon-trash icon-white"></i> Delete</a>
                                        </div></td>
                                </tr>
                 <?php }?>
                <?php }?>        
        </table>
                   <?php 
                                       if (!$row) {
                                        echo 'No papers Submitted currentlly';
                                         }?>           
        
      
        
    </form>                                
    
      </div>
      <div class="tab-pane fade" id="profile">
         
      </div>
  </div>

</div>
  <script src="<?php echo base_url(); ?>lib/bootstrap/js/bootstrap.js"></script>
    <script src="<?php echo base_url(); ?>lib/jquery-1.7.2.min.js"></script>
