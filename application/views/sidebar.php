<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
        <?php  
        $session_data = $this->session->userdata('logged_in');
        $UserType= $session_data['UserType'];
        ?>

         <div class="sidebar-nav">
           <?php if($UserType==0): ?>
             <a href="#dashboard-menu" class="nav-header" data-toggle="collapse"><i class="icon-dashboard"></i>Conference</a>
        <ul id="dashboard-menu" class="nav nav-list collapse in">
            <li><a href="<?php echo site_url('req_controller/pendingreq');?>">Pending Conference</a></li>
            <li ><a href="<?php echo site_url('req_controller/ApprovedConference');?>">Approved Conference</a></li>
            <li><a href="<?php echo site_url('req_controller/RejectedConference');?>">Rejected Conference</a></li> 
        </ul>  
       
             <?php endif;?>
         <?php if($UserType==1): ?>
       
             <a href="#dashboard-menu" class="nav-header" data-toggle="collapse">Conference Status</a>
        <ul id="dashboard-menu" class="nav nav-list collapse in">
            <li><a href="<?php echo site_url('siteSetup_controller/index');?>">Site Setup</a></li>            
                
              <li><a href="<?php echo site_url('import_controller/Bulkemail');?>">Send Email</a></li> 
              <li><a href="<?php echo site_url('View_statistics_Controller/statistics');?>">View Statics</a></li> 
              
        </ul>
             
            
             
              <a href="#error-menu" class="nav-header collapsed" data-toggle="collapse"><i class="icon-user"></i>Manage Users<i class="icon-chevron-up"></i></a>
                <ul id="error-menu" class="nav nav-list collapse">
                    <li ><a href="<?php echo site_url('viewReviwer_controller/AddReviewer');?>">Invite a Single Reviewer</a></li>
                    <li ><a href="<?php echo site_url('import_controller/index');?>">Bulk Invite Reviewers</a></li>
                    <li ><a href="<?php echo site_url('viewReviwer_controller/getReviwer');?>">View Reviewers</a></li>
                    <li ><a href="<?php echo site_url('autor_controller/index');?>">View Authors</a></li>
                    <li ><a href="<?php echo site_url('impersonate_controller/index');?>">Impersonate a User</a></li>
                </ul>
              
               <a href="<?php echo site_url('chairViewPapers_controller/index');?>" class="nav-header" ><i class="icon-copy"></i>Active Papers</a>
                <a href="<?php echo site_url('chairViewPapers_controller/DeletedPapers');?>" class="nav-header" ><i class="icon-copy"></i>Deleted Papers</a>
              <a href="<?php echo site_url('paperbids_controller/papersforbid');?>" class="nav-header" ><i class="icon-copy"></i>View Bid Counts</a>
              <a href="<?php echo site_url('task_Controller/emailhistory');?>" class="nav-header" ><i class="icon-question-sign"></i>View Email History</a>
              <a href="<?php echo site_url('paperSub_Controller/cameraview');?>" class="nav-header" ><i class="icon-question-sign"></i>View Camera Ready Papers</a>
             
          
             
          
                <a href="<?php echo site_url('assignPaper_controller/PaperTitle');?>" class="nav-header" ><i class="icon-question-sign"></i>Assign Papers</a>
                <a href="<?php echo site_url('assignPaper_controller/PapersForUnassigned');?>" class="nav-header" ><i class="icon-question-sign"></i>Unassign Papers</a>
                 <a href="<?php echo site_url('task_controller/AddCommittee');?>" class="nav-header" ><i class="icon-question-sign"></i>Add committee members</a>
              <a href="<?php echo site_url('task_controller/CommitteEmail');?>" class="nav-header" ><i class="icon-question-sign"></i>Create Task</a>
               <a href="<?php echo site_url('reviewConf_controller/InsertQuestions');?>" class="nav-header" ><i class="icon-question-sign"></i>Configure review panel</a>
               
              <?php endif;?>
               <?php if($UserType==2): ?>
              <a href="#" class="nav-header" ><i class="icon-question-sign"></i>Conference Status</a>
              <a href="#error-menu" class="nav-header collapsed" data-toggle="collapse"><i class="icon-exclamation-sign"></i>Manage Reviews <i class="icon-chevron-up"></i></a>
                <ul id="error-menu" class="nav nav-list collapse">
                    <li ><a href="#">My Subject Area</a></li>
                    <li ><a href="<?php echo site_url('assignPaper_controller/GetAssignedPaper');?>">Download Assigned Papers</a></li>
                    <li ><a href="<?php echo site_url('postRev_controller/index');?>">Detailed Reviews and discussions</a></li>
                     <li ><a href="<?php echo site_url('paperbids_controller/DisplayForBids');?>">Bid on submission</a></li>
                </ul>
              <a href="#" class="nav-header" ><i class="icon-question-sign"></i>Manage Notes</a>
             
              <?php endif;?>
               <?php if($UserType==3): ?>
             
              <a href="<?php echo site_url('siteSetup_controller/AuthorDeadLine');?>" class="nav-header" ><i class="icon-question-sign"></i>Conference Status</a>
              <a href="#error-menu" class="nav-header collapsed" data-toggle="collapse"><i class="icon-exclamation-sign"></i>Manage Submissions <i class="icon-chevron-up"></i></a>
                <ul id="error-menu" class="nav nav-list collapse">
                    <li ><a href="<?php echo site_url('paperSub_controller/index');?>">Create New Paper Submission</a></li>
                    <li ><a href="<?php echo site_url('authorPaperEdit_controller/GetPaper');?>">Edit Submission</a></li>        
                </ul>
              <a href="<?php echo site_url('note_controller/index');?>" class="nav-header" ><i class="icon-question-sign"></i>Manage Notes</a>
                <a href="<?php echo site_url('SumittedPaperSummary_Controller/index');?>" class="nav-header" ><i class="icon-question-sign"></i>Submitted Paper Summary</a>
              <?php endif;?>
               <?php if($UserType==4): ?>
             
              <a href="<?php echo site_url('siteSetup_controller/AuthorDeadLine');?>" class="nav-header" ><i class="icon-question-sign"></i>Conference Status</a>
              <a href="<?php echo site_url('task_controller/GetTask');?>" class="nav-header" ><i class="icon-question-sign"></i>Task Details</a>
              <?php endif;?>
    </div>

  <script src="<?php echo base_url(); ?>lib/bootstrap/js/bootstrap.js"></script>
    <script src="<?php echo base_url(); ?>lib/jquery-1.7.2.min.js"></script>
    
    

