<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
<?php  
        $session_data = $this->session->userdata('logged_in');
        $UserType= $session_data['UserType'];
        ?>
            
        <div class="content">
        
        <div class="header">
            <?php if($UserType==0):?>
            <h1 class="page-title">Admin Dashboard</h1>
            <?php endif;?>
            <?php if($UserType==1):?>
            <h1 class="page-title">Chair Panel</h1>
            <?php endif;?>
            <?php if($UserType==2):?>
            <h1 class="page-title">Reviewer Panel</h1>
            <?php endif;?>
            <?php if($UserType==3):?>
            <h1 class="page-title">Author Panel</h1>
            <?php endif;?>
        </div>
          <ul class="breadcrumb">
            <li><a href="#"></a> <span class="divider"></span></li>
            <li><a href="#"></a> <span class="divider"></span></li>
            <li class="active"></li>
        </ul>
             

        <div class="container-fluid">
            <div class="row-fluid">
                    

    <div class="well">
    <ul class="nav nav-tabs">
      <li class="active"><a href="#home" data-toggle="tab">View Bids</a></li>
      
    </ul>
    <div id="myTabContent" class="tab-content">
        
      <div class="tab-pane active in" id="home">
           
               <?php if ($paper){ ?>
          <?php echo $this->session->flashdata('feedback');?>
                                      <table class="table table-bordered table-hover">
             <thead>

			      <tr>
			        <th>ID</th>
			        <th>Title</th>
                                <th>Action</th>
			         </tr>
               </thead>
              
                 <?php foreach($paper as $result)
				  {
			 ?>
			        <tr>
                                    <td><?php echo $result->ID;?></td>
                                    <td><?php echo $result->Title;?></td>
                                    
                                    <td>
                                        <div class="btn-group">
                                            <a class="btn btn-success" href="<?php echo site_url('paperbids_controller/Viewbids/'.$result->ID);?>"><i class="icon-check icon-white"></i>View Bids</a>
                                           
                                        </div>
                                    </td>
                                                
                                         </tr>
               <?php }?>
               <?php }?>
                                       
        </table>
                   <?php 
                     
                                             if (!$paper) {
                                        echo 'No papers submitted';
                                         }?>             
      <?php $links= $this->pagination->create_links(); ?>
       <tr><td colspan="5" style="text-align:center"><?php echo $links;?></td></tr>
                                              
      </div>
      <div class="tab-pane fade" id="profile">
          
      </div>
  </div>

</div>
  <script src="<?php echo base_url(); ?>lib/bootstrap/js/bootstrap.js"></script>
    <script src="<?php echo base_url(); ?>lib/jquery-1.7.2.min.js"></script>