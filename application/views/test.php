<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
 <div class="content">
        
        <div class="header">
            
            <h1 class="page-title">Edit User</h1>
        </div>
        
                <ul class="breadcrumb">
            <li><a href="index.html">Home</a> <span class="divider">|</span></li>
            <li><a href="users.html">Users</a> <span class="divider">|</span></li>
            <li class="active">User</li>
        </ul>

        <div class="container-fluid">
            <div class="row-fluid">
                    

<div class="well">
    <ul class="nav nav-tabs">
      <li class="active"><a href="#home" data-toggle="tab">Profile</a></li>
      <li><a href="#profile" data-toggle="tab">Password</a></li>
    </ul>
    <div id="myTabContent" class="tab-content">
      <div class="tab-pane active in" id="home">
           <?php if ($row){ ?>
                                      <table class="table table-bordered table-hover">
             <thead>

			      <tr>
			        <th>ID</th>
			        <th>Name</th>
			        <th>Date</th>
			        <th>Email</th>
                               <th>Subject</th>
                                <th>Action</th>
			         </tr>
               </thead>
              
                 <?php foreach($row as $result)
				  {
			 ?>
			        <tr>
                                    <td><?php echo $result->id;?></td>
                                    <td><?php echo $result->ConName;?></td>
                                    <td><?php echo $result->ConDate;?></td>
                                    <td><?php echo $result->Email;?></td>
                                    <td><?php echo $result->SubArea;?></td>
                                    <td>
                                        <div class="btn-group">
                                            <a class="btn btn-success btn-flat" href="<?php echo site_url('req_controller/index/'.$result->id);?>"><i class="icon-check icon-white"></i>Approve</a>
                                             <a class="btn btn-danger btn-flat" href="<?php echo site_url('req_controller/reject/'.$result->id);?>"><i class="icon-remove icon-white"></i>Reject</a>
                                        </div>
                                    </td>
                                                
                                         </tr>
               <?php }?>
               <?php }?>
                                       
        </table>
                   <?php 
                     
                                             if (!$row) {
                                        echo 'No pending requst for approve';
                                         }?>      
          <p>TESTING<p>
      </div>
      <div class="tab-pane fade" id="profile">
    <form id="tab2">
        <?php echo $name; ?>
    </form>
      </div>
  </div>

</div>
     <script src="<?php echo base_url(); ?>lib/bootstrap/js/bootstrap.js"></script>
    <script src="<?php echo base_url(); ?>lib/jquery-1.7.2.min.js"></script>