<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>


       <?php  
        $session_data = $this->session->userdata('logged_in');
        $UserType= $session_data['UserType'];
        ?>
            
        <div class="content">
        
        <div class="header">
            <?php if($UserType==0):?>
            <h1 class="page-title">Admin Dashboard</h1>
            <?php endif;?>
            <?php if($UserType==1):?>
            <h1 class="page-title">Chair Panel</h1>
            <?php endif;?>
            <?php if($UserType==2):?>
            <h1 class="page-title">Reviewer Panel</h1>
            <?php endif;?>
            <?php if($UserType==3):?>
            <h1 class="page-title">Author Panel</h1>
            <?php endif;?>
        </div>
          <ul class="breadcrumb">
            <li><a href="#"></a> <span class="divider"></span></li>
            <li><a href="#"></a> <span class="divider"></span></li>
            <li class="active"></li>
        </ul>
             

        <div class="container-fluid">
            <div class="row-fluid">
                    

    <div class="well">
    <ul class="nav nav-tabs">
      <li class="active"><a href="#home" data-toggle="tab">Add Notes</a></li>
     <li><a href="#profile" data-toggle="tab">View Notes</a></li>
    </ul>
    <div id="myTabContent" class="tab-content">
      <div class="tab-pane active in" id="home">         
           <?php if(validation_errors()):?>
                              <div class="alert alert-info">
                                       
                                        <?php echo validation_errors(); ?>
                              </div>
                                      <?php endif;?>
            <?php echo $this->session->flashdata('feedback');?>
           <?php echo form_open('note_controller/DisplayPaperTitle'); ?>
          
                 <label>Select The paper</label>
                 <select name="Title" id="Title" class="input-xlarge">
                     <?php
                         foreach($Title as $row)
                        {
                            echo '<option value="'.$row->Title.'">'.$row->Title.'</option>';
                        }
                  ?>
                 </select>
                 <label>Note</label>
                <textarea name="Note"id="Note" rows="3" class="input-xlarge"></textarea><br/>
                <input type="submit" value="Save Note" id="submit" name="submit" class="btn btn-primary">
                 </form>
   
      </div>
      <div class="tab-pane fade" id="profile">
           
      
             <table class="table table-bordered table-hover">
                       <thead>

			      <tr>
			        <th>Paper Title</th>
			        <th>Note</th>
			        <th>Action</th>     
			     </tr>
                       </thead>
           <?php foreach ($tt as $result){ ?> 
                                <?php $id=$result->id; ?>
                        <tr>
                             <td><?php echo $result->Title;?></td>
                             <td><?php echo $result->Note;?></td>
                             <td> <a class="btn btn-danger" href="<?php echo site_url('note_controller/deleteNote/'.$result->id);?>"><i class="icon-delete icon-white"></i>Delete</a></td>
                        </tr>
      <?php  }?>
             </table>
    
      </div>
  </div>

</div>
  <script src="<?php echo base_url(); ?>lib/bootstrap/js/bootstrap.js"></script>
    <script src="<?php echo base_url(); ?>lib/jquery-1.7.2.min.js"></script>
