<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
         <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
          <link href="<?php echo base_url(); ?>lib/bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">
           <link href="<?php echo base_url(); ?>lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
          <link href="<?php echo base_url(); ?>stylesheets/theme.css" rel="stylesheet" media="screen">
          <link href="<?php echo base_url(); ?>lib/font-awesome/css/font-awesome.css" rel="stylesheet">
            <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
            <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
         <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
          <link rel="stylesheet" type="text/css" href="/jquery.datetimepicker.css"/>
   <link href="<?php echo base_url(); ?>lib/bootstrap/css/jquery.datetimepicker.css" rel="stylesheet" media="screen">
     <script src="<?php echo base_url(); ?>lib/country.js"></script>
     <link rel="shortcut icon" href="<?php echo base_url(); ?>../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(); ?>../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(); ?>../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(); ?>../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>../assets/ico/apple-touch-icon-57-precomposed.png">
     <style type="text/css">
        #line-chart {
            height:300px;
            width:800px;
            margin: 0px auto;
            margin-top: 1em;
        }
        .brand { font-family: georgia, serif; }
        .brand .first {
            color: #ccc;
            font-style: italic;
        }
        .brand .second {
            color: #fff;
            font-weight: bold;
        }
    </style>
    </head>
    <body>
         
      <div class="navbar">
        <div class="navbar-inner">
                <ul class="nav pull-right">
                    
                </ul>
                <a class="brand" href="index.html"><span class="first">SLIIT CONFERENCE MANAGEMENT SYSTEM</span> <span class="second"></span></a>
        </div>
    </div>
    


    

    
        <div class="row-fluid">
    <div class="dialog">
        
        <div class="block" style="float:left;">
            <p class="block-heading"><b>Request For a Conference<b/></p>
              <?php if(validation_errors()):?>
            <div class="alert-danger">
                                       
                                        <?php echo validation_errors(); ?>
                                    </div>
                                      <?php endif;?>
            <div class="block-body">
                 <?php echo form_open('conferenceRequest_controller/ConReq'); ?>
                <label><b>Name</b></label><hr/>
                 <label> Conference Name(*)</label>
                 <input type="text" name="ConName" id="ConName" class="span14">
                 <label> Conference Short Name(*)</label>
                 <input type="text" name="SName" id="SName" class="span14"><br/><br/>
                  <label><b>Event Information</b></label><hr/>
                  <label>Web Site(*)</label>
                 <input type="text" name="Web" id="Web" class="span12">
                 <label>Conference Date(*)</label>
                 <input type="text" name="Date" id="Date" class="span12">
                  <label>Country(*)</label>
                  <select class="dropdown" id="country" name ="country"></select><br/><br/>
                 <script language="javascript">
                populateCountries("country", "state");
                </script>
               
                <label><b>Conference organizers</b></label><hr/>
                <label>Organizer Name (*)</label>
                 <input type="text" name="Oname" id="Oname"  class="span12">
                 <label>Contact Number (*)</label>
                 <input type="text" name="Cnum" id="Cnum"  class="span12">
                 <label>Email (*)</label>
                 <input type="text" name="Email" id="Email"  class="span12"><br/><br/>
                 <label><b>Research Area(s)</b></label><hr/>
                 <table>
                     
                 <tr>
                 <td><input type="checkbox" name="r1">
                 <label>Archeology and Anthropology &nbsp; &nbsp; &nbsp;</label></td>
                 <td><input type="checkbox" name="r2">
                 <label>Architecture &nbsp; &nbsp; &nbsp;</label></td>
                 <td><input type="checkbox" name="r3">
                 <label>Astronomy</label></td>
                 </tr>
                 
                 <tr>
                 <td><input type="checkbox" name="r4">
                 <label>Bioengineering &nbsp; &nbsp; &nbsp;</label></td>
                 <td><input type="checkbox" name="r5">
                 <label>Biological Sciences &nbsp; &nbsp; &nbsp;</label></td>
                 <td><input type="checkbox" name="r6">
                 <label>Business, Management and Marketing</label></td>
                 </tr>
                 
                  <tr>
                 <td><input type="checkbox" name="r7">
                 <label>Chemistry &nbsp; &nbsp; &nbsp;</label></td>
                 <td><input type="checkbox" name="r8">
                 <label>Clinical Medicine &nbsp; &nbsp; &nbsp;</label></td>
                 <td><input type="checkbox" name="r9">
                 <label>Computer Science</label></td>
                 </tr>
                 
                 <tr>
                 <td><input type="checkbox" name="r10">
                 <label>Earth and Environmental Sciences &nbsp; &nbsp; &nbsp;</label></td>
                 <td><input type="checkbox" name="r11">
                 <label>Economics &nbsp; &nbsp; &nbsp;</label></td>
                 <td><input type="checkbox" name="r12">
                 <label>Education</label></td>
                 </tr>
                 
                 <tr>
                 <td><input type="checkbox" name="r13">
                 <label>Electrical and Mechanical Engineering &nbsp; &nbsp; &nbsp;</label></td>
                 <td><input type="checkbox" name="r14">
                 <label>Geography	 &nbsp; &nbsp; &nbsp;</label></td>
                 <td><input type="checkbox" name="r15">
                 <label>History</label></td>
                 </tr>
                 
                 
                 <tr>
                 <td><input type="checkbox" name="r16">
                 <label>Language, Literature and Linguistics &nbsp; &nbsp; &nbsp;</label></td>
                 <td><input type="checkbox" name="r17">
                 <label>Law &nbsp; &nbsp; &nbsp;</label></td>
                 <td><input type="checkbox" name="r18">
                 <label>Materials Science</label></td>
                 </tr>
                 
                 <tr>
                 <td><input type="checkbox" name="r19">
                 <label>Mathematics and Statistics &nbsp; &nbsp; &nbsp;</label></td>
                 <td><input type="checkbox" name="r20">
                 <label>Media and Journalism &nbsp; &nbsp; &nbsp;</label></td>
                 <td><input type="checkbox" name="r21">
                 <label>Philosophy</label></td>
                 </tr>
                 
                 <tr>
                 <td><input type="checkbox" name="r22">
                 <label>Physics &nbsp; &nbsp; &nbsp;</label></td>
                 <td><input type="checkbox" name="r23">
                 <label>Politics and Sociology &nbsp; &nbsp; &nbsp;</label></td>
                 <td><input type="checkbox" name="r24">
                 <label>Psychology</label></td>
                 </tr>
                 
                 <tr>
                 <td><input type="checkbox" name="r25">
                 <label>Religion &nbsp; &nbsp; &nbsp;</label></td>
                 <td><input type="checkbox" name="r26">
                 <label>Sports and Recreation &nbsp; &nbsp; &nbsp;</label></td>
                 <td><input type="checkbox" name="r27">
                 <label>Visual and Performing Arts</label></td>
                 </tr>
                 
      
                 </table><br/><br/>
                 <label><b>Any Other Information</b> </label><hr/>
                 <textarea style="width:550px;" name="info"id="info" rows="10" clos="30"></textarea><br/><br/>
                <input  class="btn btn-primary" type="submit" id="submit" name="submit" value="Submit Request">
                </form>
            </div>
        </div>
     
    </div>
</div>
 <p class="pull-right" style=""><a href="<?php echo site_url('home/admin');?>" target="blank">Admin Login </a></p>
  
  <script src="<?php echo base_url(); ?>lib/jquery.js"></script>
  <script src="<?php echo base_url(); ?>lib/jquery.datetimepicker.js"></script>
 <script>
    jQuery('#Date').datetimepicker({
    format:'Y-m-d'
    });
      </script> 
    </body>
</html>

