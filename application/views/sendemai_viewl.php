<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
<?php  
        $session_data = $this->session->userdata('logged_in');
        $UserType= $session_data['UserType'];
        ?>
            
        <div class="content">
        
        <div class="header">
            <?php if($UserType==0):?>
            <h1 class="page-title">Admin Dashboard</h1>
            <?php endif;?>
            <?php if($UserType==1):?>
            <h1 class="page-title">Chair Panel</h1>
            <?php endif;?>
            <?php if($UserType==2):?>
            <h1 class="page-title">Reviewer Panel</h1>
            <?php endif;?>
            <?php if($UserType==3):?>
            <h1 class="page-title">Author Panel</h1>
            <?php endif;?>
        </div>
          <ul class="breadcrumb">
            <li><a href="#"></a> <span class="divider"></span></li>
            <li><a href="#"></a> <span class="divider"></span></li>
            <li class="active"></li>
        </ul>
             

        <div class="container-fluid">
            <div class="row-fluid">
                    

    <div class="well">
        <p>Send Email </p><hr/>
    <div id="myTabContent" class="tab-content">
      <div class="tab-pane active in" id="home">
           <?php if(validation_errors()):?>
                              <div class="alert alert-info">
                                       
                                        <?php echo validation_errors(); ?>
                              </div>
                                      <?php endif;?>
            <?php echo $this->session->flashdata('feedback');?>
            <?php echo form_open('import_controller/Bulkemail');?>
           
              </div>        
              <label>Select user type</label>       
              <select name="type" class="topbar">
                                       <option value="1">Reviewer</option>
                                       <option value="2">Author</option>
                                     
                                     </select>
                     <label>Subject</label>      
                       <input type="text"  id="subject" name="subject" class="input-xlarge">
                     <label>Message</label> 
                       <textarea name="msg" id="msg" cols="100" rows="7" class="input-xlarge"></textarea><br/></br>                  
                   <input type="submit" value="Send" id="submit" name="submit" class="btn btn-primary">
            </form> 
           
      </div>
      <div class="tab-pane fade" id="profile">
          
      </div>
  </div>

</div>
  <script src="<?php echo base_url(); ?>lib/bootstrap/js/bootstrap.js"></script>
    <script src="<?php echo base_url(); ?>lib/jquery-1.7.2.min.js"></script>
