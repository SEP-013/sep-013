<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
<?php  
        $session_data = $this->session->userdata('logged_in');
        $UserType= $session_data['UserType'];
        ?>
            
        <div class="content">
        
        <div class="header">
            <?php if($UserType==0):?>
            <h1 class="page-title">Admin Dashboard</h1>
            <?php endif;?>
            <?php if($UserType==1):?>
            <h1 class="page-title">Chair Panel</h1>
            <?php endif;?>
            <?php if($UserType==2):?>
            <h1 class="page-title">Reviewer Panel</h1>
            <?php endif;?>
            <?php if($UserType==3):?>
            <h1 class="page-title">Author Panel</h1>
            <?php endif;?>
        </div>
          <ul class="breadcrumb">
            <li><a href="#"></a> <span class="divider"></span></li>
            <li><a href="#"></a> <span class="divider"></span></li>
            <li class="active"></li>
        </ul>
             

        <div class="container-fluid">
            <div class="row-fluid">
                    

    <div class="well">
        <p>Assign  Papers </p><hr/>
    <div id="myTabContent" class="tab-content">
      <div class="tab-pane active in" id="home">
           <?php if(validation_errors()):?>
                              <div class="alert alert-info">
                                       
                                        <?php echo validation_errors(); ?>
                              </div>
                                      <?php endif;?>
         <?php echo $this->session->flashdata('feedback');?>
                      <?php echo form_open('assignPaper_controller/AssignPaper');?>
              <label>Select The Paper</label>       
                <select name="Paper" id="Paper" class="span8">
                      
                     <?php
                         foreach($title as $row)
                       {
                            echo '<option value="'.$row->Title.'">' .$row->ID.'-'.$row->Title.'</option>';
                        }
                  ?>
                    </select>
                     <label>Select reviewer</label>      
                      <select name="email" id="email" class="span4">
                          <?php
                         foreach($email as $row)
                       {
                            echo '<option value="'.$row->Email.'">'.$row->Email.'</option>';
                       }
                  ?>
                    </select><br/><br/>                     
                   <input type="submit" value="Assign Paper" id="submit" name="submit" class="btn btn-primary">
            </form> 
      </div>
      <div class="tab-pane fade" id="profile">
          
      </div>
  </div>

</div>
  <script src="<?php echo base_url(); ?>lib/bootstrap/js/bootstrap.js"></script>
    <script src="<?php echo base_url(); ?>lib/jquery-1.7.2.min.js"></script>
