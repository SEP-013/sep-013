  

<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
<?php  
        $session_data = $this->session->userdata('logged_in');
        $UserType= $session_data['UserType'];
        ?>
            
        <div class="content">
        
        <div class="header">
            <?php if($UserType==0):?>
            <h1 class="page-title">Admin Dashboard</h1>
            <?php endif;?>
            <?php if($UserType==1):?>
            <h1 class="page-title">Chair Panel</h1>
            <?php endif;?>
            <?php if($UserType==2):?>
            <h1 class="page-title">Reviewer Panel</h1>
            <?php endif;?>
            <?php if($UserType==3):?>
            <h1 class="page-title">Author Panel</h1>
            <?php endif;?>
        </div>
          <ul class="breadcrumb">
            <li><a href="#"></a> <span class="divider"></span></li>
            <li><a href="#"></a> <span class="divider"></span></li>
            <li class="active"></li>
        </ul>
             

        <div class="container-fluid">
            <div class="row-fluid">
                    

    <div class="well">
    <ul class="nav nav-tabs">
      <li class="active"><a href="" data-toggle="tab">Active and Deletion of uploads of the Author</a></li>
      
    </ul>
    <div id="myTabContent" class="tab-content">
      <div class="tab-pane active in" id="home">
              
            <?php $countofauthorupload=0;
					 if ($authorupload){ ?>
   			  <table class="table table-bordered table-hover">
            	 <thead>
			      <tr>
			        <th>paper(s) uploaded ID</th>
			        <th>paper(s) Title</th>         
                    <th>paper(s) Filename</th>         
			      </tr>
               </thead>
 
 						<?php  foreach($authorupload as $result)
							  {
							 	?>		  
									<tr> <td> <?php echo $result->ID;?> </td>
                                   <td> <?php echo $result->Title; ?> </td>
									<td> <?php	echo $result->FileName;
										$countofauthorupload =$countofauthorupload+1; 	?></td></tr>
            			<?php } ?>	
                        
                         <?php 
                                       if (!$authorupload) {
                                        echo 'No papers submitted';
                                         }?>   
                    </table>
 
    								


<br>
		 
<?php } echo  

"Total No. of upload :",$countofauthorupload;
		
		echo "<br>";
		echo "<br>";?>

			<br>
<br>

<?php $countofdelete=0; if ($authordelete){ ?>
 
                <table class="table table-bordered table-hover">
            	 <thead>
			      <tr>
			        <th>paper(s) deleted ID</th>
			        <th>paper(s) title</th>
                    <th>paper(s) Filename</th>         
                      
			      </tr>
               </thead>
 
 						<?php foreach($authordelete as $result)
							  {
							 	?>		  
									<tr> <td> <?php echo $result->ID;?> </td>
                                   <td> <?php echo $result->Title; ?> </td>
									<td><?php echo $result->FileName;	
										$countofdelete =$countofdelete+1; 	?></td></tr>
            			<?php } ?>	
                         <?php 
                                       if (!$authordelete) {
                                        echo 'No papers deleted';
                                         }?>   
 				
                    </table>
 <br>
			 
<?php } echo "Total No. of delete : ",$countofdelete;
		
		echo "<br>";
		echo "<br>"; ?>  

		<?php if($authorupload && $authordelete){ $total=$countofauthorupload+$countofdelete;
				$uploadspercent=($countofauthorupload/$total)*100;
				$deletespercent=($countofdelete/$total)*100;
		}
		?>


<html>
  <head>
    <!--Load the AJAX API-->
            
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">

      // Load the Visualization API and the piechart package.
      google.load('visualization', '1.0', {'packages':['corechart']});

      // Set a callback to run when the Google Visualization API is loaded.
      google.setOnLoadCallback(drawChart);

      // Callback that creates and populates a data table,
      // instantiates the pie chart, passes in the data and
      // draws it.
      function drawChart() {

        // Create the data table.
		var uploads=<?php echo $uploadspercent;?>;
		var deletes=<?php echo $deletespercent;?>;
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Slices');
        data.addRows([
          ['Uploads', uploads],
		  ['Deletes', deletes],
         
        ]);

        // Set chart options
        var options = {'title':'Active and deletion of uploads','width':500,
                           'height':500,
                       is3D: true,};

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
        chart.draw(data, options);
      }
    </script>
  </head>

  <body>
    <!--Div that will hold the pie chart-->
    <div id="piechart_3d"></div>
  </body>
</html>
    

</div>
      <div class="tab-pane fade" id="profile">
         
      </div>
  </div>

</div>
  <script src="<?php echo base_url(); ?>lib/bootstrap/js/bootstrap.js"></script>
    <script src="<?php echo base_url(); ?>lib/jquery-1.7.2.min.js"></script>

  


