<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
<html>
    <head>
        <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
          <link href="<?php echo base_url(); ?>lib/bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">
           <link href="<?php echo base_url(); ?>lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
          <link href="<?php echo base_url(); ?>stylesheets/theme.css" rel="stylesheet" media="screen">
          <link href="<?php echo base_url(); ?>lib/font-awesome/css/font-awesome.css" rel="stylesheet">
 
     <link rel="shortcut icon" href="<?php echo base_url(); ?>../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(); ?>../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(); ?>../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(); ?>../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>../assets/ico/apple-touch-icon-57-precomposed.png">
    
    
   
 
    <style type="text/css">
        #line-chart {
            height:300px;
            width:800px;
            margin: 0px auto;
            margin-top: 1em;
        }
        .brand { font-family: georgia, serif; }
        .brand .first {
            color: #ccc;
            font-style: italic;
        }
        .brand .second {
            color: #fff;
            font-weight: bold;
        }
    </style>
    </head>
    <body>
        <?php
         $session_data = $this->session->userdata('logged_in');
         $Email = $session_data['Email'];
         $ConName= $session_data['ConName'];
         $UserType= $session_data['UserType'];
         
        ?>
        
        
       <div class="navbar">
        <div class="navbar-inner">
                <ul class="nav pull-right">
                    
                    
                    <li id="fat-menu" class="dropdown">
                        <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-user"></i><?php echo $Email; ?>
                            <i class="icon-caret-down"></i>
                        </a>

                        <ul class="dropdown-menu">
                            <li><a tabindex="-1" href="<?php echo site_url('Settings_Controller/settings');?>">My Account</a></li>
                            <li class="divider"></li>
     
                            <li class="divider visible-phone"></li>
                           <?php if($UserType==0): ?>
                             <li><a tabindex="-1" href="<?php echo site_url('home/admin');?>">Logout</a></li>
                           <?php endif;?>
                        <?php if(($UserType==1)||($UserType==2)||($UserType==3)||($UserType==4)) : ?>     
                            <li><a tabindex="-1" href="<?php echo site_url('home/logout');?>">Logout</a></li>
                            <?php endif; ?>
                        </ul>
                    </li>
                    
                </ul>
            <a class="brand" href="index.html"><span class="first"></span> <span class="second"><?php if($UserType!=0) echo $ConName;else{
echo 'CONFERENCE MANAGEMENT SYSTEM';}?></span></a>
        </div>
    </div>
    


    
   

    
   
    


    <script src="lib/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript">
        $("[rel=tooltip]").tooltip();
        $(function() {
            $('.demo-cancel-click').click(function(){return false;});
        });
    </script>
    </body>
</html>
