<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
<?php  
        $session_data = $this->session->userdata('logged_in');
        $UserType= $session_data['UserType'];
        ?>
            
        <div class="content">
        
        <div class="header">
            <?php if($UserType==0):?>
            <h1 class="page-title">Admin Dashboard</h1>
            <?php endif;?>
            <?php if($UserType==1):?>
            <h1 class="page-title">Chair Panel</h1>
            <?php endif;?>
            <?php if($UserType==2):?>
            <h1 class="page-title">Reviewer Panel</h1>
            <?php endif;?>
            <?php if($UserType==3):?>
            <h1 class="page-title">Author Panel</h1>
            <?php endif;?>
        </div>
          <ul class="breadcrumb">
            <li><a href="#"></a> <span class="divider"></span></li>
            <li><a href="#"></a> <span class="divider"></span></li>
            <li class="active"></li>
        </ul>
             

        <div class="container-fluid">
            <div class="row-fluid">
                    

    <div class="well">
        <p>Bids </p><hr/>
    <div id="myTabContent" class="tab-content">
      <div class="tab-pane active in" id="home">
               <?php if ($bids){ ?>
                                      <table class="table table-bordered table-hover">
             <thead>

			      <tr>
			        <th>Name</th>
			        <th>Email</th>
			        <th>Bid</th>
			      
                               
               </thead>
              
                 <?php foreach($bids as $result)
				  {
                                    $bid=$result->bids;
			 ?>
			        <tr>
                                    <td><?php echo $result->UserName;?></td>
                                    <td><?php echo $result->Email;?></td>
                                    
                                    <td><?php if($bid=='1'){
                                            echo 'In Conflict';
                                    }
                                    elseif ($bid=='2') {
                                         echo 'Low/No Expertise';
                                    }
                                     elseif ($bid=='3') {
                                         echo 'Medium/Topic Knowledge';
                                    }
                                     elseif ($bid=='4') {
                                         echo 'High/Very Interested';
                                    }
                                    
                                    ?> </td>  
                                    
                                </tr>
                 <?php }?>
                <?php }?>        
        </table>
                   <?php 
                                       if (!$bids) {
                                        echo 'No bids submitted currently';
                                         }?>             
      
                                              
      </div>
      <div class="tab-pane fade" id="profile">
          
      </div>
  </div>

</div>
  <script src="<?php echo base_url(); ?>lib/bootstrap/js/bootstrap.js"></script>
    <script src="<?php echo base_url(); ?>lib/jquery-1.7.2.min.js"></script>