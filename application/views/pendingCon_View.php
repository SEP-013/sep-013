<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
 <?php  
        $session_data = $this->session->userdata('logged_in');
        $UserType= $session_data['UserType'];
        ?>
            
        <div class="content">
        
        <div class="header">
            <?php if($UserType==0):?>
            <h1 class="page-title">Admin Dashboard</h1>
            <?php endif;?>
            <?php if($UserType==1):?>
            <h1 class="page-title">Chair Panel</h1>
            <?php endif;?>
            <?php if($UserType==2):?>
            <h1 class="page-title">Reviewer Panel</h1>
            <?php endif;?>
            <?php if($UserType==3):?>
            <h1 class="page-title">Author Panel</h1>
            <?php endif;?>
        </div>
          <ul class="breadcrumb">
            <li><a href="#"></a> <span class="divider"></span></li>
            <li><a href="#"></a> <span class="divider"></span></li>
            <li class="active"></li>
        </ul>
             

        <div class="container-fluid">
            <div class="row-fluid">
                    

    <div class="well">
    <ul class="nav nav-tabs">
      <li class="active"><a href="#home" data-toggle="tab">Requests</a></li>
      
    </ul>
    <div id="myTabContent" class="tab-content">
        
      <div class="tab-pane active in" id="home">
           
               <?php if ($row){ ?>
          <?php echo $this->session->flashdata('feedback');?>
                                      <table class="table table-bordered table-hover">
             <thead>

			      <tr>
			        <th>ID</th>
			        <th>Conference Name</th>
                                <th>Short Name</th>
			        <th>Date</th>
                               
                                <th>Web Site</th>
                                <th>Country</th>
                                <th>Organizer Name</th>
                                 <th>Contact Number</th> 
			        <th>Email</th>
                                <th>Action</th>
                                
			         </tr>
               </thead>
              
                 <?php foreach($row as $result)
				  {
			 ?>
			        <tr>
                                    <td><?php echo $result->id;?></td>
                                    <td><?php echo $result->ConName;?></td>
                                    <td><?php echo $result->ShortName;?></td>
                                    <td><?php echo $result->ConDate;?></td>
                                    <td><?php echo $result->Web;?></td>
                                    <td><?php echo $result->Country;?></td>
                                    <td><?php echo $result->Oname;?></td>
                                    <td><?php echo $result->Onum;?></td>
                                    
                                    <td><?php echo $result->Email;?></td>
                                   
                                    <td>
                                        <div class="btn-group">
                                            <a class="btn btn-success" href="<?php echo site_url('req_controller/index/'.$result->id);?>"><i class="icon-check icon-white"></i>Approve</a>
                                             <a class="btn btn-danger" href="<?php echo site_url('req_controller/reject/'.$result->id);?>"><i class="icon-remove icon-white"></i>Reject</a>
                                        </div>
                                    </td>
                                                
                                         </tr>
               <?php }?>
               <?php }?>
                                       
        </table>
                   <?php 
                     
                                             if (!$row) {
                                        echo 'No pending requst for approve';
                                         }?>             
      <?php $links= $this->pagination->create_links(); ?>
       <tr><td colspan="5" style="text-align:center"><?php echo $links;?></td></tr>
                                              
      </div>
      <div class="tab-pane fade" id="profile">
          
      </div>
  </div>

</div>
  <script src="<?php echo base_url(); ?>lib/bootstrap/js/bootstrap.js"></script>
    <script src="<?php echo base_url(); ?>lib/jquery-1.7.2.min.js"></script>