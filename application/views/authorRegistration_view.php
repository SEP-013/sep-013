<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
         <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
          <link href="<?php echo base_url(); ?>lib/bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">
           <link href="<?php echo base_url(); ?>lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
          <link href="<?php echo base_url(); ?>stylesheets/theme.css" rel="stylesheet" media="screen">
          <link href="<?php echo base_url(); ?>lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    
     <link rel="shortcut icon" href="<?php echo base_url(); ?>../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(); ?>../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(); ?>../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(); ?>../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>../assets/ico/apple-touch-icon-57-precomposed.png">
     <style type="text/css">
        #line-chart {
            height:300px;
            width:800px;
            margin: 0px auto;
            margin-top: 1em;
        }
        .brand { font-family: georgia, serif; }
        .brand .first {
            color: #ccc;
            font-style: italic;
        }
        .brand .second {
            color: #fff;
            font-weight: bold;
        }
    </style>
    </head>
    <body>
      <div class="navbar">
        <div class="navbar-inner">
                <ul class="nav pull-right">
                    
                </ul>
                <a class="brand" href="index.html"><span class="first">SLIIT CONFERENCE MANAGEMENT SYSTEM</span> <span class="second"></span></a>
        </div>
    </div>
    


    

    
        <div class="row-fluid">
    <div class="dialog">
        <div class="block">
            <p class="block-heading">Sign Up</p>
              <?php if(validation_errors()):?>
                                    <div class="alert alert-info">
                                       
                                        <?php echo validation_errors(); ?>
                                    </div>
                                      <?php endif;?>
            <div class="block-body">
                 <?php echo form_open('authourReg_controller/loadConName'); ?>
                 <label>Select The Conference</label>
                 <select name="ConName" id="ConName" class="span12">
                     <?php
                         foreach($groups as $row)
                        {
                            echo '<option value="'.$row->ConName.'">'.$row->ConName.'</option>';
                        }
                  ?>
                 </select>
                 <label>Email</label>
                 <input type="text" name="Email" id="Email" class="span12">
                 <label>User Name</label>
                 <input type="text" name="uname" id="uname"  class="span12">
                 <label>Contact Number</label>
                 <input type="text" name="phone" id="phone" class="span12">
                 <label>Password</label>
                 <input type="password"name="pwd" id="pwd" class="span12">
                 <label>Re Enter Password</label>
                 <input type="password"name="rpwd" id="rpwd" class="span12">
                <input  class="btn btn-primary" type="submit" id="submit" name="submit" value="Sign Up">
                </form>
            </div>
        </div>
       
    </div>
</div>


    </body>
</html>
