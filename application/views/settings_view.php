<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
 <?php  
        $session_data = $this->session->userdata('logged_in');
        $UserType= $session_data['UserType'];
        ?>
            
        <div class="content">
        
        <div class="header">
            <?php if($UserType==0):?>
            <h1 class="page-title">Admin Dashboard</h1>
            <?php endif;?>
            <?php if($UserType==1):?>
            <h1 class="page-title">Chair Panel</h1>
            <?php endif;?>
            <?php if($UserType==2):?>
            <h1 class="page-title">Reviewer Panel</h1>
            <?php endif;?>
            <?php if($UserType==3):?>
            <h1 class="page-title">Author Panel</h1>
            <?php endif;?>
        </div>
          <ul class="breadcrumb">
            <li><a href="#"></a> <span class="divider"></span></li>
            <li><a href="#"></a> <span class="divider"></span></li>
            <li class="active"></li>
        </ul>
             

        <div class="container-fluid">
            <div class="row-fluid">
                    

    <div class="well">
    <ul class="nav nav-tabs">
      <li class="active"><a href="" data-toggle="tab">Account Settings</a></li>
      
    </ul>
    <div id="myTabContent" class="tab-content">
      <div class="tab-pane active in" id="home">
              
         <?php echo form_open('settings_controller/index'); ?>
          <?php if(validation_errors()):?>
             <div class='alert alert-danger alert-dismissable'><?php echo validation_errors(); ?></div>
         <?php endif;?>
               <?php echo $this->session->flashdata('feedback');?>
         
        <label>Email</label>
        <input type="text" value="<?php echo $Email;?>" id="email" name="email" class="input-xlarge">
        <label>User Name</label>
        <input type="text" value="<?php echo $UserName;?>" id="username" name="username" class="input-xlarge">
        <label>Phone Number</label>
        <input type="text" value="<?php echo $Phone;?>" id="Phone" name="Phone" class="input-xlarge"><br/><br/>
       <input type="submit" value="Save" id="submit" name="submit" class="btn btn-primary">
    </form>                                
    <h3>Change Password</h3><hr/>
     <?php echo form_open('settings_controller/ChangePwd'); ?>
         
                  
              <label>New Password</label>
              <input type="password"  id="pwd" name="pwd" class="input-xlarge">
               <label>Re-Enter New Password</label>
              <input type="password"  id="npwd" name="npwd" class="input-xlarge"><br/><br/>
              <input type="submit" value="Change Password" id="submit" name="submit" class="btn btn-primary">
            </form>   
      </div>
      <div class="tab-pane fade" id="profile">
          
      </div>
  </div>

</div>
  <script src="<?php echo base_url(); ?>lib/bootstrap/js/bootstrap.js"></script>
    <script src="<?php echo base_url(); ?>lib/jquery-1.7.2.min.js"></script>
