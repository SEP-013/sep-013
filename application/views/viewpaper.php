<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
<?php  
        $session_data = $this->session->userdata('logged_in');
        $UserType= $session_data['UserType'];
        ?>
            
        <div class="content">
        
        <div class="header">
            <?php if($UserType==0):?>
            <h1 class="page-title">Admin Dashboard</h1>
            <?php endif;?>
            <?php if($UserType==1):?>
            <h1 class="page-title">Chair Panel</h1>
            <?php endif;?>
            <?php if($UserType==2):?>
            <h1 class="page-title">Reviewer Panel</h1>
            <?php endif;?>
            <?php if($UserType==3):?>
            <h1 class="page-title">Author Panel</h1>
            <?php endif;?>
        </div>
          <ul class="breadcrumb">
            <li><a href="#"></a> <span class="divider"></span></li>
            <li><a href="#"></a> <span class="divider"></span></li>
            <li class="active"></li>
        </ul>
             

        <div class="container-fluid">
            <div class="row-fluid">
                    

    <div class="well">
        <p>Paper Details </p><hr/>
    <div id="myTabContent" class="tab-content">
      <div class="tab-pane active in" id="home">
               <?php if ($row){ ?>
                                      <table class="table table-bordered table-hover">
            
              
                 <?php foreach($row as $result)
				  {
                                   
			 ?>
			        <tr>
                                    <td>Paper ID</td>
                                    <td><?php echo $result->ID;?></td>
                                </tr>
                                <tr>
                                    <td>Title</td>
                                    <td><?php echo $result->Title;?></td>
                                </tr>
                                <tr>
                                    <td>Abstract</td>
                                    <td><?php echo $result->Abstract;?></td>
                                </tr>
                                <tr>
                                    <td>Primary Contact</td>
                                    <td><?php echo $result->Primarycontact;?></td>
                                </tr>
                                     
                                   
                                
                 <?php }?>
                <?php }?>        
        </table>
                   <?php 
                                       if (!$row) {
                                        echo 'No papers Submitted currentlly';
                                         }?>             
      
                                              
      </div>
      <div class="tab-pane fade" id="profile">
          
      </div>
  </div>

</div>
  <script src="<?php echo base_url(); ?>lib/bootstrap/js/bootstrap.js"></script>
    <script src="<?php echo base_url(); ?>lib/jquery-1.7.2.min.js"></script>
