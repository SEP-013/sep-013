<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
<?php  
        $session_data = $this->session->userdata('logged_in');
        $UserType= $session_data['UserType'];
        ?>
            
        <div class="content">
        
        <div class="header">
            <?php if($UserType==0):?>
            <h1 class="page-title">Admin Dashboard</h1>
            <?php endif;?>
            <?php if($UserType==1):?>
            <h1 class="page-title">Chair Panel</h1>
            <?php endif;?>
            <?php if($UserType==2):?>
            <h1 class="page-title">Reviewer Panel</h1>
            <?php endif;?>
            <?php if($UserType==3):?>
            <h1 class="page-title">Author Panel</h1>
            <?php endif;?>
        </div>
          <ul class="breadcrumb">
            <li><a href="#"></a> <span class="divider"></span></li>
            <li><a href="#"></a> <span class="divider"></span></li>
            <li class="active"></li>
        </ul>
             

        <div class="container-fluid">
            <div class="row-fluid">
                    

    <div class="well">
    <ul class="nav nav-tabs">
      <li class="active"><a href="" data-toggle="tab">Manage Deadlines</a></li>
      
    </ul>
         <a href="<?php echo site_url('import_controller/export');?>">Site Setup     |      </a>
        <a href="<?php echo site_url('SiteSetup_Controller/index');?>">Manage Deadline    |      </a>
        <a href="<?php echo site_url('siteSetup_controller/loadtrack');?>">Add Track   |      </a>
         <a href="<?php echo site_url('reviewConf_controller/load');?>">Configure Review Form  </a><br/> <br/>
    <div id="myTabContent" class="tab-content">
      <div class="tab-pane active in" id="home">
        <?php echo $this->session->flashdata('feedback');?>
          <?php echo $this->session->flashdata('feedback1');?>
          <?php echo $this->session->flashdata('feedback2');?>
          <?php echo $this->session->flashdata('feedback3');?>
          <?php echo $this->session->flashdata('feedback4');?>
	   <?php echo form_open_multipart('SiteSetup_Controller/index');
        ?>
        

           <?php if ($row){ ?>
           <?php foreach ($row as $result) {?> 
                        <?php $val=$result->PaperSub;?><br/>
                       <?php $val_1=$vall= $result->EditPaper;?><br/>
                       <?php $val_2=$valll= $result->ReviewSub;?>
                       <?php $val_3= $result->ReviewBid;?>
                       <?php $val_4= $result->CamSub;?>
                       
                       <?php $from= $result->fromPaperSub;
					   		 $from1=$result->fromEditPaper;
							 $from2=$result->fromReviewSub;
							 $from3=$result->fromReviewBid;
							 $from4=$result->fromCamSub;
							
							$status_sub=$result->status;
							$statusedit=$result->statusedit;
							$statusreview=$result->statusreview;
							$statusbid=$result->statusbid;
							$statuscam=$result->statuscam;
							 ?>          
                         
                  <table><tr>
  				             <td>    New Paper Submission from </td> <td><?php echo "<input type='text'id='from' name='from' value='$from' />"; ?>             </td>   <td>    before
                    <?php echo "<input type='text'id='datetimepicker' name='datetimepicker' value='$val' />"; ?> <select name="bid" id="bid">
          			
        <option <?php if($status_sub==0) echo "selected=\"selected\"" ?> value="0">Disabled</option>
		<option <?php if($status_sub==1) echo "selected=\"selected\"" ?> value="1">Enabled</option>
		
	</select>
    				</td>	
                    	</tr>
        
        		<tr>
 						     <td> Edit Submission from </td><td><?php echo "<input type='text'id='from1' name='from1' value='$from1' />"; ?> 
          
                  </td><td>before
                    <?php echo "<input type='text'id='datetimepicker-1' name='datetimepicker-1' value='$val_1' />";?> <select name="bid1" id="bid1">
         <option <?php if($statusedit==0) echo "selected=\"selected\"" ?> value="0">Disabled</option>
		<option <?php if($statusedit==1) echo "selected=\"selected\"" ?> value="1">Enabled</option>
		
	</select>
    			</td>
                </tr>
        
       					<tr>
                 				<td> Review Submission from</td><td> <?php echo "<input type='text'id='from2' name='from2' value='$from2' />"; ?> 
                                
                    </td><td>before
                    <?php echo "<input type='text'id='datetimepicker-2' name='datetimepicker-2' value='$val_2' />";?>
                    <select name="bid2" id="bid2">
         <option <?php if($statusreview==0) echo "selected=\"selected\"" ?> value="0">Disabled</option>
		<option  <?php if($statusreview==1) echo "selected=\"selected\"" ?> value="1">Enabled</option>
		
	</select>
        			</td>	
                    	</tr>
        
     		   <tr>
 															<td> Review bid from </td><td>      <?php echo "<input type='text'id='from3' name='from3' value='$from3' />"; ?> 
                                
               			</td><td>before
                    <?php echo "<input type='text'id='datetimepicker-3' name='datetimepicker-3' value='$val_3' />";?> <select name="bid3" id="bid3">
        <option <?php if($statusbid==0) echo "selected=\"selected\"" ?> value="0">Disabled</option>
		<option <?php if($statusbid==1) echo "selected=\"selected\"" ?> value="1">Enabled</option>
		
	</select>
    		   </td>
                </tr>
        	
       			        <tr>
  						    	<td> Camera Ready Paper Submission from </td><td><?php echo "<input type='text'id='from4' name='from4' value='$from4' />"; ?> 
                                
                     </td><td>before
                    <?php echo "<input type='text'id='datetimepicker-4' name='datetimepicker-4' value='$val_4' />";?>
        	    <select name="bid4" id="bid4">
        <option <?php if($statuscam==0) echo "selected=\"selected\"" ?> value="0">Disabled</option>
		<option <?php if($statuscam==1) echo "selected=\"selected\"" ?> value="1">Enabled</option>
		
	</select>
    				</td>	
                    	</tr>
    </table>
                    <br/>
                    <br/>
                    <?php } 
					
					?>
           
                      <input type="submit" value="Save" id="save" name="save" class="btn btn-primary">
        <?php }?>   
    </form>                                
  
      </div>
      <div class="tab-pane fade" id="profile">
         
      </div>
  </div>
  
</div>
  <script src="<?php echo base_url(); ?>lib/bootstrap/js/bootstrap.js"></script>
    <script src="<?php echo base_url(); ?>lib/jquery-1.7.2.min.js"></script>
  <link rel="stylesheet" type="text/css" href="/jquery.datetimepicker.css"/>
   <link href="<?php echo base_url(); ?>lib/bootstrap/css/jquery.datetimepicker.css" rel="stylesheet" media="screen">
  <script src="<?php echo base_url(); ?>lib/jquery.js"></script>
  <script src="<?php echo base_url(); ?>lib/jquery.datetimepicker.js"></script>
 <script>
    jQuery('#datetimepicker').datetimepicker({
    format:'Y-m-d H:i:s'
    });
      </script> 
      <script>
    jQuery('#datetimepicker-1').datetimepicker({
    format:'Y-m-d H:i:s'
    });
      </script> 
       <script>
    jQuery('#datetimepicker-2').datetimepicker({
    format:'Y-m-d H:i:s'
    });
      </script> 
        <script>
    jQuery('#datetimepicker-3').datetimepicker({
    format:'Y-m-d H:i:s'
    });
      </script> 
         <script>
    jQuery('#datetimepicker-4').datetimepicker({
    format:'Y-m-d H:i:s'
    });
      </script> 
        <script>
    jQuery('#datetimepicker-5').datetimepicker({
    format:'Y-m-d H:i:s'
    });
      </script>
           <script>
    jQuery('#from').datetimepicker({
    format:'Y-m-d H:i:s'
    });
      </script>  
         <script>
    jQuery('#from1').datetimepicker({
    format:'Y-m-d H:i:s'
    });
      </script>  
         <script>
    jQuery('#from2').datetimepicker({
    format:'Y-m-d H:i:s'
    });
      </script>  
         <script>
    jQuery('#from3').datetimepicker({
    format:'Y-m-d H:i:s'
    });
      </script>  
         <script>
    jQuery('#from4').datetimepicker({
    format:'Y-m-d H:i:s'
    });
      </script>  
      