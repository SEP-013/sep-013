<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
<?php
$session_data = $this->session->userdata('logged_in');
$UserType = $session_data['UserType'];
?>

<div class="content">

    <div class="header">
        <?php if ($UserType == 0): ?>
            <h1 class="page-title">Admin Dashboard</h1>
        <?php endif; ?>
        <?php if ($UserType == 1): ?>
            <h1 class="page-title">Chair Panel</h1>
        <?php endif; ?>
        <?php if ($UserType == 2): ?>
            <h1 class="page-title">Reviewer Panel</h1>
        <?php endif; ?>
        <?php if ($UserType == 3): ?>
            <h1 class="page-title">Author Panel</h1>
        <?php endif; ?>
    </div>
    <ul class="breadcrumb">
        <li><a href="#"></a> <span class="divider"></span></li>
        <li><a href="#"></a> <span class="divider"></span></li>
        <li class="active"></li>
    </ul>


    <div class="container-fluid">
        <div class="row-fluid">


            <div class="well">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#home" data-toggle="tab">Edit Paper</a></li>
                    <li ><a href="#authors" data-toggle="tab">Edit Paper Authors</a></li>
                    <li ><a href="#tracks" data-toggle="tab">Edit Paper Tracks</a></li>

                </ul>
                <div id="myTabContent" class="tab-content">
                    <div class="tab-pane active in" id="home">
                        <?php if (validation_errors()): ?>
                            <div class="alert alert-info">

                                <?php echo validation_errors(); ?>
                            </div>
                        <?php endif; ?>


                        <?php echo form_open('authorPaperEdit_controller/Edit_paper');
                        ?>
                        <?php foreach ($row as $result) { ?> 
                            <?php $val_3 = $result->ID; ?><br/>
                            <?php $val = $result->Title; ?><br/>
                            <?php $val_1 = $vall = $result->Abstract; ?><br/>
                            <?php $val_2 = $valll = $result->Primarycontact; ?>

                            <?php echo "<input type='hidden' id='id' name='id' value='$val_3'required='required' />"; ?>
                            <label>Paper Title</label>
                            <?php echo "<input type='text'id='title' name='title' value='$val' />"; ?>
                            <label>Abstract</label>
                            <label id="lblcount"style="color:Red;font-weight:bold;">Number of Characters Left:200</label>
                            <textarea style="width:450px;"name="abstract" id="abstract" cols="10" rows="8"onkeyup="LimtCharacters(this,200,'lblcount');" required="required"><?php echo $result->Abstract; ?></textarea>
                            <label>Primary Contact</label>
                            <?php echo "<input type='text'id='pc' name='pc' value='$val_2' />"; ?><br/><br/>
                        <?php } ?>

                        <input type="submit" value="Save" id="submit" name="submit" class="btn btn-primary">

                        </form>                                
                              <script>
                                 function LimtCharacters(txtMsg, CharLength, indicator) {
                                    chars = txtMsg.value.length;
                                    document.getElementById(indicator).innerHTML = CharLength - chars;
                                    if (chars > CharLength) {
                                        txtMsg.value = txtMsg.value.substring(0, CharLength);
                                    }
                                }
   
                            </script>

                    </div>

                    <div class="tab-pane fade" id="authors">
                        <?php if (validation_errors()): ?>
                            <div class="alert alert-info">

                                <?php echo validation_errors(); ?>
                            </div>
                        <?php endif; ?>


                        <?php echo form_open('authorPaperEdit_controller/Edit_PaperAuthor');
                        ?>


                        <div class="fields">
                            <label>Author(s)</label>
                            <select  id="authorList" name="authorList" class="input-xlarge" required>
                                <option value=-1>Select from here</option>
                                    <?php
                                    foreach ($authors as $author) {
                                        echo '<option value=' . $author->id . '>' . $author->Name . '</option>';
                                    }
                                    ?> 

                            </select> <br>

                            <label>Name</label>
                            <input type="text" id="nameText"  name="nameText" class="input-xlarge" required> <br>
                            <label>Email</label>
                            <input type="text"  id="emailText"  name="emailText" class="input-xlarge" required> <br>
                            <label>Contact Number</label>
                            <input type="text"  id="numberText"  name="numberText" class="input-xlarge" required> <br><hr/>


                            <input type="submit" value="Save" id="authorSubmit" name="submit" class="btn btn-primary">

                            </form>                                
                          


                        </div>



                    </div>

                    <div class="tab-pane fade" id="tracks">
                        <?php if (validation_errors()): ?>
                            <div class="alert alert-info">

                                <?php echo validation_errors(); ?>
                            </div>
                        <?php endif; ?>


                        <?php echo form_open('authorPaperEdit_controller/Edit_PaperTracks');
                        ?>


                        <div class="fields">
                             <label><b>Track(s)</b></label>
                            <hr/>
                            
                            
                            <?php foreach($track as $r){ 
                                  if($r['selected'] == 1){ $checked = 'checked';}else{ $checked = '';}
                                
                            echo"<input type='checkbox' name='fields[track][]' value=".$r['trackName']."  ". $checked." >";
                            echo "&nbsp;";
                            echo $r['trackName'];
                            echo "&nbsp; &nbsp; &nbsp;<br><br>";
                           } ?>
                            
                            
                         <?php foreach ($row as $res) { ?> 
                            <?php $val_3 = $res->ID; ?><br/>
                            <?php $val = $result->Title; ?><br/>
                            <?php echo "<input type='text' id='id' name='id' value='$val_3'required='required' style='Display:none' />"; ?><br/><br/>
                        <?php echo "<input type='text'id='title' name='title' value='$val' style='Display:none'/>"; ?>
                                <?php } ?>
                            
                            <hr/>


                            <input type="submit" value="Save" id="authorSubmit" name="submit" class="btn btn-primary">

                            </form>                                
                                

                        </div>



                    </div>
                    
                    
                </div>
                <script src="<?php echo base_url(); ?>lib/bootstrap/js/bootstrap.js"></script>
                <script src="<?php echo base_url(); ?>lib/jquery-1.7.2.min.js"></script>

                <script type="text/javascript">
                    $(document).ready(function() {

                        $('#authorSubmit').hide(true);
                        $('#authorList').val("-1");
                        $('#authorSubmit').show(false);

                        $('#authorList').change(function() {
                            var id = $('#authorList').val();
                            
                            $('#nameText').val("");
                            $('#emailText').val("");
                            $('#numberText').val("");
                            
                            if (id > 0)
                            {
                                $('#authorSubmit').show(true);
                                //alert(id);
                                
                                $.ajax({
                                    type: 'post',
                                    url: '../Get_PaperAuthor/' + id,
                                    dataType: 'json',
                                    success: function(output) {
                                        $.each(output, function(key, val) {
                                            
                                            $('#nameText').val(val['Name']);
                                            $('#emailText').val(val['Email']);
                                            $('#numberText').val(val['Phone']);

                                        })
                                    }
                                });
                                
                                
                            } else {
                                $('#authorSubmit').hide(true);
                            }

                        });

                    });

                </script>