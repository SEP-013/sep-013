<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
  <script type="text/javascript" src=" https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
  
 <?php  
        $session_data = $this->session->userdata('logged_in');
        $UserType= $session_data['UserType'];
        ?>
            
        <div class="content">
        
        <div class="header">
            <?php if($UserType==0):?>
            <h1 class="page-title">Admin Dashboard</h1>
            <?php endif;?>
            <?php if($UserType==1):?>
            <h1 class="page-title">Chair Panel</h1>
            <?php endif;?>
            <?php if($UserType==2):?>
            <h1 class="page-title">Reviewer Panel</h1>
            <?php endif;?>
            <?php if($UserType==3):?>
            <h1 class="page-title">Author Panel</h1>
            <?php endif;?>
        </div>
          <ul class="breadcrumb">
            <li><a href="#"></a> <span class="divider"></span></li>
            <li><a href="#"></a> <span class="divider"></span></li>
            <li class="active"></li>
        </ul>
             

        <div class="container-fluid">
            <div class="row-fluid">
                    

    <div class="well">
    <ul class="nav nav-tabs">
      <li class="active"><a href="" data-toggle="tab">Review Form Configuration </a></li>
      
    </ul>
    <div id="myTabContent" class="tab-content">
      <div class="tab-pane active in" id="home">
          
       
        <?php echo form_open_multipart('reviewConf_controller/InsertQuestions');
           
          ?>
           <?php echo $this->session->flashdata('feedback');?>
           <?php if(validation_errors()):?>
             <div class='alert alert-danger alert-dismissable'><?php echo validation_errors(); ?></div>
         <?php endif;?>
         
        <label>Enter Question</label>
        <div id="fields">
        <input type="text"   name="fields[stop][]" class="input-xlarge"> 
        
        <input type="button" id="addField" value="Add more qoestion" class="btn btn-primary"/>
         <br/>
        </div>
        
        
       <input type="submit" value="Submit" id="submit" name="submit" class="btn btn-primary">
    
    </form>                                
      <script type="text/javascript">
    $(document).ready(function() {
        $('#addField').click(function() {
            $('#fields').append(
                $('<input type="text" name="fields[stop][]" class="input-xlarge"/>'),
               
               
                $('<br/>')
            );
        });
    });
</script>
    
      </div>
      <div class="tab-pane fade" id="profile">
         
      </div>
  </div>

</div>
  <script src="<?php echo base_url(); ?>lib/bootstrap/js/bootstrap.js"></script>
    <script src="<?php echo base_url(); ?>lib/jquery-1.7.2.min.js"></script>

            