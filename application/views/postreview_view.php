<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
<?php  
        $session_data = $this->session->userdata('logged_in');
        $UserType= $session_data['UserType'];
        ?>
            
        <div class="content">
        
        <div class="header">
            <?php if($UserType==0):?>
            <h1 class="page-title">Admin Dashboard</h1>
            <?php endif;?>
            <?php if($UserType==1):?>
            <h1 class="page-title">Chair Panel</h1>
            <?php endif;?>
            <?php if($UserType==2):?>
            <h1 class="page-title">Reviewer Panel</h1>
            <?php endif;?>
            <?php if($UserType==3):?>
            <h1 class="page-title">Author Panel</h1>
            <?php endif;?>
        </div>
          <ul class="breadcrumb">
            <li><a href="#"></a> <span class="divider"></span></li>
            <li><a href="#"></a> <span class="divider"></span></li>
            <li class="active"></li>
        </ul>

        <div class="container-fluid">
            <div class="row-fluid">
                   
    <div class="well">
        <p>Post Reviews </p><hr/>
    <div id="myTabContent" class="tab-content">
      <div class="tab-pane active in" id="home">
           <?php if(validation_errors()):?>
                              <div class="alert alert-info">
                                       
                                        <?php echo validation_errors(); ?>
                              </div>
              <?php endif;?>
         <?php echo $this->session->flashdata('feedback');?>
                      
                        <?php  echo form_open('postRev_Controller/InsertAns'); ?>
                         <label>Select paper Name </label>
                      <select name="pid" id="pid">
                          <option value="0" selected="selected">Select a paper</option>
                          <?php

                         foreach($pid as $row)
                       {
                            echo '<option value="'.$row->pid.'">'.$row->Title.'</option>';
                       }
                     ?>
                          
                    </select><br/><br/>  
                     <label>Your recommendation </label>
                    <select name="dec" id="dec">
                        <option value ="1">Accept</option>
                        <option value ="2">Reject</option>
                        <option value ="3">Neutral</option>
                    </select><br><br>
                    <?php
                        foreach($que as $row){
                           
                            $id=$row->id;
                            echo $row->question;echo '<br>';
                            echo '<textarea  name="ans[stop][]" id="ans" class="input-xlarge"></textarea> <br>';
                            //echo $id;
                            echo "<input type='hidden' value=$id id='id[qid][]' name='id[qid][]' class='input-xlarge'><br>";
                        }
                    ?>
                     
                    <input type="submit" value="Save" id="submit" name="submit" class="btn btn-primary"></form>
                     <!--- <label>Detailed comments</label>
                        <label>( Required, Visible To Authors After Decision Notification )</label>
                      <textarea name="dcom"id="dcom" cols="50" rows="5" class="input-xlarge"></textarea><br/><br/>
                      
                      <label>Comments to Program Chair</label>
                        <label>( Required)</label>
                       <textarea name="ccom"id="ccom" cols="50" rows="5" class="input-xlarge"></textarea><br/><br/>
                                       

                <button id="btn_a" type="submit" name="Accept" class="btn btn-success"><i class="icon-ok-sign input-xlarge"></i> Accept</button>
                <button id="btn_r" type="submit"  name="Reject"  class="btn btn-danger"><i class="icon-white icon-remove-circle input-xlarge"></i> Reject</button>
                <input type="submit" class="btn btn-danger" value="Reject"><i class="icon-white icon-remove-circle input-xlarge"></i>Reject</input>
                <input type="submit" value="Save" id="submit" name="submit" class="btn btn-primary">-->
            </form>
      </div>
      <div class="tab-pane fade" id="profile">
          
      </div>
  </div>

</div>
  <script src="<?php echo base_url(); ?>lib/bootstrap/js/bootstrap.js"></script>
    <script src="<?php echo base_url(); ?>lib/jquery-1.7.2.min.js"></script>
