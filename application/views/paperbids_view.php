<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
<?php  
        $session_data = $this->session->userdata('logged_in');
        $UserType= $session_data['UserType'];
        ?>
            
        <div class="content">
        
        <div class="header">
            <?php if($UserType==0):?>
            <h1 class="page-title">Admin Dashboard</h1>
            <?php endif;?>
            <?php if($UserType==1):?>
            <h1 class="page-title">Chair Panel</h1>
            <?php endif;?>
            <?php if($UserType==2):?>
            <h1 class="page-title">Reviewer Panel</h1>
            <?php endif;?>
            <?php if($UserType==3):?>
            <h1 class="page-title">Author Panel</h1>
            <?php endif;?>
        </div>
          <ul class="breadcrumb">
            <li><a href="#"></a> <span class="divider"></span></li>
            <li><a href="#"></a> <span class="divider"></span></li>
            <li class="active"></li>
        </ul>
             

        <div class="container-fluid">
            <div class="row-fluid">
                    

    <div class="well">
        <p>Bid Papers </p><hr/>
    <div id="myTabContent" class="tab-content">
      <div class="tab-pane active in" id="home">
            <?php echo $this->session->flashdata('feedback');?>
          <?php echo form_open('paperbids_controller/bid');?>
               <?php if ($paper){ ?>
                                  
              
               
			        
                                   
                                    <label>Select paper</label>
                                     <select name="paper" id="paper" class="topbar">
                                       <?php
                                            foreach($paper as $row)
                                             {
                                                 echo '<option value="'.$row->ID.'">'.$row->ID.'-'.$row->Title.'</option>';
                                             } ?>
                                     </select>
                                    
                                  
                                  <label>Select bid option</label>
                                  <select name="bid" id="bid" class="topbar">
                                      <option value="1">In conflict</option>
                                      <option value="2">Low/No Expertise</option>
                                      <option value="3">Medium/Topic Knowledge</option>
                                      <option value="4">High/Very Interested</option>
                                     </select><br/><br/>
                                  <input type="submit" value="Bid Paper" id="submit" name="submit" class="btn btn-primary">
                                      <?php }?>        
                                  </form>  
                   
       
                                     <?php 
                                       if (!$paper) {
                                        echo 'No papers submitted to bid currentlly';
                                         }?>             
      
                                              
      </div>
      <div class="tab-pane fade" id="profile">
          
      </div>
  </div>

</div>
  <script src="<?php echo base_url(); ?>lib/bootstrap/js/bootstrap.js"></script>
    <script src="<?php echo base_url(); ?>lib/jquery-1.7.2.min.js"></script>
