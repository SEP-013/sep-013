  
  
 <?php  
        $session_data = $this->session->userdata('logged_in');
        $UserType= $session_data['UserType'];
        ?>
            
        <div class="content">
        
        <div class="header">
            <?php if($UserType==0):?>
            <h1 class="page-title">Admin Dashboard</h1>
            <?php endif;?>
            <?php if($UserType==1):?>
            <h1 class="page-title">Chair Panel</h1>
            <?php endif;?>
            <?php if($UserType==2):?>
            <h1 class="page-title">Reviewer Panel</h1>
            <?php endif;?>
            <?php if($UserType==3):?>
            <h1 class="page-title">Author Panel</h1>
            <?php endif;?>
        </div>
          <ul class="breadcrumb">
            <li><a href="#"></a> <span class="divider"></span></li>
            <li><a href="#"></a> <span class="divider"></span></li>
            <li class="active"></li>
        </ul>
             

        <div class="container-fluid">
            <div class="row-fluid">
                    

    <div class="well">
    <ul class="nav nav-tabs">
      <li class="active"><a href="" data-toggle="tab">Paper Submission</a></li>
      
    </ul>
    <div id="myTabContent" class="tab-content">
      <div class="tab-pane active in" id="home">
         <?php if(isset($error)) { ?>
          <div class="alert alert-danger">
          
               <strong>Error!</strong> <?php echo $error; ?> 
          </div>
         <?php } ?>
        <?php echo form_open_multipart('import_controller/index');
           
          ?>
           <?php echo $this->session->flashdata('feedback');?>
          <?php if(validation_errors()):?>
             <div class='alert alert-danger alert-dismissable'><?php echo validation_errors(); ?></div>
         <?php endif;?>
        <form method="post" action="<?php echo base_url() ?>csv/importcsv" enctype="multipart/form-data">
           
        <input type="file" name="userfile" ><br><br>
        <input type="submit" name="submit" value="UPLOAD" class="btn btn-primary">
        
</form>
    
    </form>                                
    
    
      </div>
      <div class="tab-pane fade" id="profile">
         
      </div>
  </div>

</div>
  <script src="<?php echo base_url(); ?>lib/bootstrap/js/bootstrap.js"></script>
    <script src="<?php echo base_url(); ?>lib/jquery-1.7.2.min.js"></script>
