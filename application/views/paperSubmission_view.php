<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>

    
   <script type="text/javascript" src=" https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
  
 <?php  
        $session_data = $this->session->userdata('logged_in');
        $UserType= $session_data['UserType'];
        ?>
            
        <div class="content">
        
        <div class="header">
            <?php if($UserType==0):?>
            <h1 class="page-title">Admin Dashboard</h1>
            <?php endif;?>
            <?php if($UserType==1):?>
            <h1 class="page-title">Chair Panel</h1>
            <?php endif;?>
            <?php if($UserType==2):?>
            <h1 class="page-title">Reviewer Panel</h1>
            <?php endif;?>
            <?php if($UserType==3):?>
            <h1 class="page-title">Author Panel</h1>
            <?php endif;?>
        </div>
          <ul class="breadcrumb">
            <li><a href="#"></a> <span class="divider"></span></li>
            <li><a href="#"></a> <span class="divider"></span></li>
            <li class="active"></li>
        </ul>
             

        <div class="container-fluid">
            <div class="row-fluid">
                    

    <div class="well">
    <ul class="nav nav-tabs">
      <li class="active"><a href="" data-toggle="tab">Paper Submission</a></li>
      
    </ul>
    <div id="myTabContent" class="tab-content">
      <div class="tab-pane active in" id="home">
         <?php if(isset($error)) { ?>
          <div class="alert alert-danger">
          
               <strong>Error!</strong> <?php echo $error; ?> 
          </div>
         <?php } ?>
        <?php echo form_open_multipart('upload/do_upload');
           
          ?>
           <?php echo $this->session->flashdata('feedback');?>
          <?php if(validation_errors()):?>
             <div class='alert alert-danger alert-dismissable'><?php echo validation_errors(); ?></div>
         <?php endif;?>
             
         
        <label>Title</label>
        <input type="text"  id="title" name="title" class="input-xlarge">
        <label>Abstract (Maximum length 200) </label>
       
       <label id="lblcount"style="color:Red;font-weight:bold;">Number of Characters Left:200</label>
        <textarea style="width:450px;"name="abstract"id="abstract" rows="8" cols="10" onkeyup="LimtCharacters(this,200,'lblcount');" class="input-xlarge"></textarea><hr/>
        <label>Author(s)</label>
       
        <button id="add_field_button" class="btn btn-primary">Add More Authors</button>
        <div class="fields">
         <label>Name</label>
         <input type="text"   name="fields[stop][]" class="input-xlarge"> <br>
         <label>Email</label>
         <input type="text"   name="fields1[email][]" class="input-xlarge"> <br>
          <label>Contact Number</label>
         <input type="text"   name="fields2[num][]" class="input-xlarge"> <br><hr/>
        
        
        
        </div>
         <label>Primary Contact</label>
        <input type="text"  id="primarycontact" name="primarycontact" class="input-xlarge"><br>
        <?php foreach($track as $row){ 
        echo"<input type='checkbox' name='fields[track][]' value=$row->trackname>";
        echo "&nbsp;";
        echo $row->trackname;
        echo "&nbsp; &nbsp; &nbsp;<br><br>";
       } ?>
        <label>Upload File</label>
        <input type="file"  id="userfile" name="userfile" class="input-xlarge"><br/><br/>
       <input type="submit" value="Submit" id="upload" name="upload" class="btn btn-primary">
    
    </form>                                
      <script type="text/javascript">
   $(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".fields"); //Fields wrapper
    var add_button      = $("#add_field_button"); //Add button ID
   
    var x = 3; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div><label>Name</label><input type="text" name="fields[stop][]" class="input-xlarge"/><br>\n\
            <label>Email</label> <input type="text" name="fields1[email][]" class="input-xlarge"/><br><div>\n\
             <label>Contact Number</label><input type="text" name="fields[num][]" class="input-xlarge">\n\
             </div><a href="#" class="remove_field">Remove</a><hr>');
			
        }
    });
   
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x-=2;
    })
});
 function LimtCharacters(txtMsg, CharLength, indicator) {
chars = txtMsg.value.length;
document.getElementById(indicator).innerHTML = CharLength - chars;
if (chars > CharLength) {
txtMsg.value = txtMsg.value.substring(0, CharLength);
}
}
   
</script>


    
      </div>
      <div class="tab-pane fade" id="profile">
         
      </div>
  </div>

</div>
  <script src="<?php echo base_url(); ?>lib/bootstrap/js/bootstrap.js"></script>
    <script src="<?php echo base_url(); ?>lib/jquery-1.7.2.min.js"></script>

            