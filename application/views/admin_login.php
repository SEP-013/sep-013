<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
         <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
          <link href="<?php echo base_url(); ?>lib/bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">
           <link href="<?php echo base_url(); ?>lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
          <link href="<?php echo base_url(); ?>stylesheets/theme.css" rel="stylesheet" media="screen">
          <link href="<?php echo base_url(); ?>lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    
     <link rel="shortcut icon" href="<?php echo base_url(); ?>../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(); ?>../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(); ?>../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(); ?>../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>../assets/ico/apple-touch-icon-57-precomposed.png">
     <style type="text/css">
        #line-chart {
            height:300px;
            width:800px;
            margin: 0px auto;
            margin-top: 1em;
        }
        .brand { font-family: georgia, serif; }
        .brand .first {
            color: #ccc;
            font-style: italic;
        }
        .brand .second {
            color: #fff;
            font-weight: bold;
        }
    </style>
    </head>
    <body>
      <div class="navbar">
        <div class="navbar-inner">
                <ul class="nav pull-right">
                    
                </ul>
                <a class="brand" href="index.html"><span class="first">SLIIT CONFERENCE MANAGEMENT SYSTEM</span> <span class="second"></span></a>
        </div>
    </div>
    

   
    
        <div class="row-fluid">
    <div class="dialog">
        <h1 style="color:black;">Admin Panel</h1>
        <div class="block">
            <p class="block-heading">Sign In</p>
              <?php if(validation_errors()):?>
                                    <div class="alert alert-info">
                                       
                                        <?php echo validation_errors(); ?>
                                    </div>
                                      <?php endif;?>
            <div class="block-body">
                 <?php echo form_open('Login_Controller'); ?>
                    <label>Email</label>
                    <input type="text"name="Email" id="Email" class="span12">
                    <label>Password</label>
                    <input type="password" name="password" type="password" class="span12">
                    <input  class="btn btn-primary pull-right" type="submit" value="Login">
                    <label class="remember-me"><input type="checkbox"> Remember me</label>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
        <p class="pull-right" style=""><a href="<?php echo site_url('authourReg_controller/index');?>" target="blank">New Author? </a></p>
        <p><a href="<?php echo site_url('conferenceRequest_controller/index');?>">New Conference?</a></p>
    </div>
</div>


    </body>
</html>
