<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
<?php  
        $session_data = $this->session->userdata('logged_in');
        $UserType= $session_data['UserType'];
        ?>
            
        <div class="content">
        
        <div class="header">
            <?php if($UserType==0):?>
            <h1 class="page-title">Admin Dashboard</h1>
            <?php endif;?>
            <?php if($UserType==1):?>
            <h1 class="page-title">Chair Panel</h1>
            <?php endif;?>
            <?php if($UserType==2):?>
            <h1 class="page-title">Reviewer Panel</h1>
            <?php endif;?>
            <?php if($UserType==3):?>
            <h1 class="page-title">Author Panel</h1>
            <?php endif;?>
        </div>
          <ul class="breadcrumb">
            <li><a href="#"></a> <span class="divider"></span></li>
            <li><a href="#"></a> <span class="divider"></span></li>
            <li class="active"></li>
        </ul>
             

        <div class="container-fluid">
            <div class="row-fluid">
                    

    <div class="well">
    <ul class="nav nav-tabs">
      <li class="active"><a href="" data-toggle="tab">View Authors</a></li>
      
    </ul>
    <div id="myTabContent" class="tab-content">
      <div class="tab-pane active in" id="home">
          <?php echo form_open('viewReviwer_controller/getAuthors'); ?>
                 <label>Select The paper</label>
                 <select name="Paper" id="Paper" class="span8">
                      
                     <?php
                         foreach($title as $row)
                       {
                            echo '<option id="pid" name="pid" value="'.$row->ID.'">' .$row->ID.'-'.$row->Title.'</option>';
                        }
                  ?>
                    </select>
                 <input type="submit" value="Save" id="submit" name="submit" class="btn btn-primary">
                   </form>      
        <?php if ($Author){ ?>
                                      <table class="table table-bordered table-hover">
             <thead>

			      <tr>
			        <th>Name</th>
			        <th>Email</th>
			        <th>Phone Number</th>
                                 <th>Action</th>
			      </tr>
               </thead>
              
                 <?php foreach($Author as $result)
				  {
                                    
			 ?>
                         
			        <tr>
                                    <td><?php echo $result->Name;?></td>
                                    <td><?php echo $result->Email;?></td>
                                    <td><?php echo $result->Phone;?></td>
                                     
                                    
                                    <td>
                                     <div class="btn-group">
                                        <a class="btn btn" href="<?php echo site_url('viewReviwer_controller/Email/'.$result->id);?>"><i class="icon-pencil icon-white"></i> Send Email</a>
                                       
                                        </div></td>
                                </tr>
                 <?php }?>
                <?php }?>        
        </table>
                   <?php 
                                       if (!$Author) {
                                        echo 'No Authors Registerd currentlly';
                                         }?>           
        
      
        
                            
    
      </div>
      <div class="tab-pane fade" id="profile">
         
      </div>
  </div>

</div>
  <script src="<?php echo base_url(); ?>lib/bootstrap/js/bootstrap.js"></script>
    <script src="<?php echo base_url(); ?>lib/jquery-1.7.2.min.js"></script>
