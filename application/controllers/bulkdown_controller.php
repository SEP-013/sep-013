<?php
class Bulkdown_Controller extends CI_Controller{
    function __construct() {
        parent::__construct();
        $this->load->helper('form');
        $this->load->library('form_validation');
       $this->load->library('zip');
    }
    
    
    function bulkdownload()
    {
      
        $this->load->model('bulkdown_model');
        $row=$this->bulkdown_model->GetFileName();
        
        $files_array = array();        

        
        for($i=0;$i<count($row);$i++)
        {   
                    array_push($files_array,$row[$i]['FileName']);    
        }

     
        $data['files'] = $files_array;
        
        $this->load->view('header');
        $this->load->view('sidebar');
        $this->load->view('bulkview',$data);
        $this->load->view('footer');
    }
    function bulk(){
         
   
       $this->form_validation->set_rules('val', 'val','trim|required|xss_clean','groupchecked','required');
       //$this->load->library('submit');
       
        if($this->form_validation->run())
            {
           
             
                $file_path = './upload/';
                $zip_file_name = 'papers.zip';
                  
/*@var $selected_files type */
            $selected_files = $_POST['files'];
                
               
                if($selected_files)
                {
                foreach($selected_files as $file)
                    {
                   
                    $this->zip->read_file($file_path.$file);
                        
                    }
                   
                $this->zip->download($zip_file_name);
             
            }
        else{
            
           //$data['failmsg'] = "Please select some files...";
            
           // $error = array('error' => $this->form_validation->display_errors());
            // echo 'failled';
          $this->load->view('header');
        $this->load->view('sidebar');
        $this->load->view('bulk_error');
        $this->load->view('footer');
        }
        
        }
        
        
    }
   
 
}
?>
