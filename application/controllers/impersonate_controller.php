<?php
 
class impersonate_controller extends CI_Controller{
    function test(){
            $this->load->view('header');
              $this->load->view('sidebar');
            $this->load->view('impersonate_view');
             $this->load->view('footer');
        }
    
            
    function index(){
        
        $this->load->library('form_validation');
        $this->form_validation->set_rules('Email', 'Email', 'trim|required|xss_clean|callback_check_database');
       // $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_check_database');

        if($this->form_validation->run() == FALSE)
        {
             $this->load->view('header');
              $this->load->view('sidebar');
            $this->load->view('impersonate_view');
             $this->load->view('footer');
        }
      else{
            redirect('home/index');
     }
    
    }
    function check_database()
 {
   $this->load->model('login_model');
   $Email= $this->input->post('Email');
  

   //query the database
   $result = $this->login_model->Impersonate($Email);

   if($result)
   {
     $sess_array = array();
     foreach($result as $row)
     {
       $sess_array = array(
         'id' => $row->id,
         'Email' => $row->Email,
         'UserType'=>$row->UserType,
           'ConID'=>$row->ConID,
            'ConName'=>$row->ConName,
          // 'PaperDeadLine'=>$row->PaperDeadLine,
           'UserName'=>$row->UserName,
           'Phone'=>$row->Phone
       );
       $this->session->set_userdata('logged_in', $sess_array);
     }
     return TRUE;
   }
   else
   {
     $this->form_validation->set_message('check_database', 'Invalid Email');
     return false;
   }
 }
}

?>
