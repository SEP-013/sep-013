<?php
class viewReviwer_controller extends CI_Controller{
    function getReviwer(){
        $this->load->model('viewReviwer_model');
        $data['Reviwer']= $this->viewReviwer_model->getReviwers();
        $this->load->view('header');
        $this->load->view('sidebar');
        $this->load->view('viewReviwer_view',$data);
        $this->load->view('footer');
        
    }
    function Email($id){
        $this->load->model('viewReviwer_model');
        $data['row']= $this->viewReviwer_model->getEmail($id);
        $this->load->view('header');
        $this->load->view('sidebar');
        $this->load->view('EmailReviwer_View',$data);
        $this->load->view('footer');
         if($this->input->post('Send')){
               $this->load->library('form_validation');
               $this->form_validation->set_rules('email', 'Email', 'required|email');
               $this->form_validation->set_rules('subject', 'Subject', 'required');
               $this->form_validation->set_rules('msg', 'Message', 'required'); 
               if($this->form_validation->run() == FALSE){
                  redirect('viewReviwer_controller/getReviwer');
               }
               else{
                    $this->viewReviwer_model->InsertEmail();
                     $this->session->set_flashdata('feedback', '<div class="alert alert-success" <a class="close pull-right" data-dismiss="alert">×</a>Email Sent Sucsessfully....!!</div>');
                    redirect('viewReviwer_controller/test');
                   // redirect('refresh')
                    // $this->load->view('emailtemp_View');
               }
               }
             
         }
         function test(){
              $this->load->view('header');
              $this->load->view('sidebar');
              $this->load->view('emailtemp_View');
               $this->load->view('footer');
         }
                 
                 function getAuthors(){
        $this->load->model('viewReviwer_model');
         $this->load->model('assignPaper_model');
         $data['title']=$this->assignPaper_model->GetPaperTitle();
         $data['email']=$this->assignPaper_model->GetRewiver();
        $data['Author']= $this->viewReviwer_model-> getAuthors();
        $this->load->view('header');
        $this->load->view('sidebar');
        $this->load->view('viewAuthor_view',$data);
        $this->load->view('footer');
         if($this->input->post('submit')){
             
         $this->load->view('header');
        $this->load->view('sidebar');
        $this->load->view('viewAuthor_view',$data);
        $this->load->view('footer');
         }
        
             
         }
         function AddReviewer(){
              // $this->load->library('form_validation');
              $this->load->library('email','form_validation');
               $this->load->view('header');		
                       $this->load->view('sidebar');
		       $this->load->view('addReviwers');
                        $this->load->view('footer');
             $this->load->model('viewReviwer_model');
             if($this->input->post('submit')){
               
	          $this->form_validation->set_rules('Email','Email','required|valid_email');
                 if($this->form_validation->run()==FALSE)
	         {
		      // $this->load->view('header');		
                      // $this->load->view('sidebar');
		      // $this->load->view('AddReviwers');
                       // $this->load->view('footer');
                      $this->session->set_flashdata('feedback', '<div class="alert alert-danger" <a class="close pull-right" data-dismiss="alert">×</a>Failed....!!</div>');
                       redirect('ciewReviwer_controller/msg');
		}
                else{
                    $email=$this->input->post('email');
		    $type='2';
                    $pwd=$this->get_random_password();
                    $this->viewReviwer_model->add($email,$type,$pwd);
                    $this->load->view('addReviwers');
                    $this->email->from('your@example.com');
                    $this->email->to($email);
                    $this->email->subject('Login Details');
                    $this->email->message("thank you\n\n" .$pwd.  "\n\ntest");
                    $this->email->send();
                    $this->session->set_flashdata('feedback', '<div class="alert alert-success" <a class="close pull-right" data-dismiss="alert">×</a>Reviwer Added Sucsessfully....!!</div>');
                    redirect('viewReviwer_controller/msg');
                }
                    
             }
             
         }
         function get_random_password($chars_min=6, $chars_max=8, $use_upper_case=false, $include_numbers=false, $include_special_chars=false)
        {
        $length = rand($chars_min, $chars_max);
        $selection = 'aeuoyibcdfghjklmnpqrstvwxz';
        if($include_numbers) {
            $selection .= "1234567890";
        }
        if($include_special_chars) {
            $selection .= "!@\"#$%&[]{}?|";
        }

        $password = "";
        for($i=0; $i<$length; $i++) {
            $current_letter = $use_upper_case ? (rand(0,1) ? strtoupper($selection[(rand() % strlen($selection))]) : $selection[(rand() % strlen($selection))]) : $selection[(rand() % strlen($selection))];            
            $password .=  $current_letter;
        }                

        return $password;
       }
       function msg(){
           $this->load->view('header');
          $this->load->view('sidebar');
           $this->load->view('addReviwers');
             $this->load->view('footer');
           
       }
       function export(){
       $this->load->library('export');
       $this->load->model('viewReviwer_model');
      $sql = $this->viewReviwer_model->ExportRevs();
      $this->export->to_excel($sql, 'viewReviwer_view'); 
       }
       
        function PaperTitle(){
        $this->load->model('assignPaper_model');
        $data['title']=$this->assignPaper_model->GetPaperTitle();
         $data['email']=$this->assignPaper_model->GetRewiver();
        $this->load->view('header');
        $this->load->view('sidebar');
        $this->load->view('viewAuthor_view',$data);
        $this->load->view('footer');
    }
    }


?>
