<?php
class note_controller extends CI_Controller{
    function index(){
         $this->load->model('notes_model');
          $data['Title'] = $this->notes_model->GetPaperTitle();
          $data['tt'] = $this->notes_model->GetNotes();
          $this->load->view('header');  
          $this->load->view('sidebar');  
          $this->load->view('addNotes',$data);  
          $this->load->view('footer');
    }
            function DisplayPaperTitle(){
             $this->load->model('notes_model');
             $data['Title'] = $this->notes_model->GetPaperTitle();
          if($this->input->post('submit')){
             $this->load->library('form_validation');
             $this->form_validation->set_rules('Note', 'Note', 'required');
             if($this->form_validation->run() == FALSE)
              {
                   $this->load->view('header');  
                   $this->load->view('sidebar');  
                   $this->load->view('addNotes',$data);  
                   $this->load->view('footer');                    
                  // redirect('Note_Controller/index');
              }
              else{
                   $this->notes_model->AddNotes(); 
                   $this->session->set_flashdata('feedback', '<div class="alert alert-success" <a class="close pull-right" data-dismiss="alert">×</a>Note Added Sucsessfully....!!</div>');
                    redirect('note_controller/index');
                         
                  
                   
              }
          }
}
function deleteNote($id){
    $this->load->model('notes_model');
    $data['Title'] = $this->notes_model->DeleteNote($id);
    redirect('note_controller/index');
    
}
}

?>
