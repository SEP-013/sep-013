<?php
class postRev_Controller extends CI_Controller{
    function index(){
        $this->load->model('postRev_model');
        $this->load->model('reviewConf_model');
       
        $data['pid']=$this->postRev_model->GetAssignedPaperID();
        $data['Title']=$this->postRev_model->GetAssignedPaperID();
        $data['que']=$this->reviewConf_model->GetQuestions(); 
         $data['dis']=$this->reviewConf_model->GetDis();   
         
        $this->load->view('header');
        $this->load->view('sidebar');
        $this->load->view('postreview_view',$data);
        $this->load->view('footer');
         
        
    }
   function InsertAns(){
         if($this->input->post('submit')){
             $this->form_validation->set_rules('ans[stop][]','Comment for chair','required');
              $this->load->model('postRev_model');
              $dec=  $this->input->post('dec');
         if($this->form_validation->run()==FALSE){
              $this->load->model('postRev_model');
       
               $this->session->set_flashdata('feedback', '<div id="alrt" class="alert alert-success" <a class="close pull-right" data-dismiss="alert">×</a>Your Review cant add....!!</div>');
             redirect('postRev_Controller/index');
         }
          else{
         $numFields = count($_POST['ans']['stop']);
         $num=count($_POST['id']['qid']);
         for ($i = 0; $i < $numFields; $i++){
         $ans=$_POST['ans']['stop'][$i];
         $num1=$_POST['id']['qid'][$i];
         $pid=$this->input->post('pid');
         $this->load->model('reviewConf_model');
         $this->postRev_model->SetRevSub($pid);
         $this->reviewConf_model->InsertAns($ans,$num1,$pid);
         $data= $this->postRev_model->GetSubmittedReviewCount($pid);
         $countA= $this->postRev_model-> GetAccept($pid);
         $countR= $this->postRev_model-> GetReject($pid);
         $countN= $this->postRev_model-> GetNeaturel($pid);
         if(($data==0)&&($countA>$countR)&&($countA>$countN)){
              $this->postRev_model->SetPaperStatus($pid);
          }
        
        
         } 
        $this->session->set_flashdata('feedback', '<div id="alrt" class="alert alert-success" <a class="close pull-right" data-dismiss="alert">×</a>Your Review Added Sucsessfully....!!</div>');
        redirect('postRev_Controller/index');
          
        }
         }
   }
   
         
    function GetComments(){
        $this->load->model('postRev_model');
        $data['comments']=$this->postRev_model->GetComments();
        $data['papers']=$this->postRev_model->GetReviewedPapers();
        $this->load->view('header');
        $this->load->view('sidebar');
        $this->load->view('ViewRview_View',$data);
        $this->load->view('footer');
    }
    
    function GetPaperComments(){

        if(isset($_POST['data'])){
            $this->load->model('postRev_model');
            $data=$this->postRev_model->GetPaperComments($_POST['data']);
            foreach ($data as $key => $value) {
                $emp[$key] = $value;
            }
            $data=$this->postRev_model->GetAssignedPaperCount($_POST['data']);
            $a =0;
            foreach ($data as $key => $value) {
                $role[$a] = $value->count;
                $a++;
            }
            $emp['total']=$role[0];
            echo json_encode($emp);

        }
    }
    function SetPaperStatus(){

        if(isset($_POST['Accept'])){

            $this->load->model('postRev_model');
            $status='A';
            $data=$this->postRev_model->SetPaperStatus($_POST['Accept'],$status);
            echo($data);

        }
        if(isset($_POST['Reject'])){

            $this->load->model('postRev_model');
            $status='R';
            $data=$this->postRev_model->SetPaperStatus($_POST['Reject'],$status);
            echo($data);
        }
    }
   
}

?>
