<?php

class AuthourReg_Controller extends CI_Controller{
    function index(){
         $this->load->model('author_model');
         $data['groups'] = $this->author_model->getConName();
         $this->load->view('authorRegistration_view',$data);  
    }
            
    function loadConName(){
         $this->load->model('author_model');
         $data['groups'] = $this->author_model->getConName();
         if($this->input->post('submit')){
              $this->load->library('form_validation');
             $this->form_validation->set_rules('Email', 'Email', 'required|callback_CheckAuthor');
              $this->form_validation->set_rules('uname', 'Username', 'required');
              $this->form_validation->set_rules('phone','Contact Number','required');
             $this->form_validation->set_rules('pwd', 'Password', 'required|matches[rpwd]');
             $this->form_validation->set_rules('rpwd', 'Password Confirmation', 'required');
              if($this->form_validation->run() == FALSE)
              {
                  $this->load->view('authorRegistration_view',$data);                     
              }
             else{
                 $this->author_model->signup();
                // $this->load->view('AuthorRegistration_View',$data);   
                redirect('authourReg_controller/test');
                  
             }
         }
          
    }
    function test(){
        $this->load->view('login_view');
    }
            function CheckAuthor(){
           $this->load->model('author_model');
        $result = $this->author_model->CheckAuthorExist();
 
        if($result)
        {
            return TRUE;    
        }
 
        else
        {
            $this->form_validation->set_message('CheckAuthor', 'An Author with this email has already been added to the system! Please try with a new one!');
            return FALSE;
        }
    }
}
?>
