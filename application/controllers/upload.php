<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class upload extends CI_Controller {
    
    function index()
	{
		$this->load->view('upload_form');
	}
        
         public function do_upload()
         {
                        $this->load->library('form_validation');
                        $this->form_validation->set_rules('title', 'Title','required', 'trim|required|xss_clean');
                        $this->form_validation->set_rules('abstract', 'Abstract','required', 'trim|required|xss_clean|max_length[200]');  
                        $this->form_validation->set_rules('fields[stop][]', 'Author Email','required');
                        $this->form_validation->set_rules('fields1[email][]', 'Author Name','required|valid_email');
                        $this->form_validation->set_rules('fields2[num][]', 'Author Phone','required|regex_match[/^[0-9().-]+$/]');
                        $this->form_validation->set_rules('primarycontact', 'Primarycontact','required|valid_email');  
                         $session_data = $this->session->userdata('logged_in');	
                        $data['Email'] = $session_data['Email'];
                        $data['ConName']=$session_data['ConName'];
                        if (!$this->form_validation->run())
                                 {
                                      $this->load->view('header');
                                     $this->load->view('sidebar');
                                     $this->load->view('paperSubmission_view',$data);
                                       $this->load->view('footer');
                                     //$this->form_validation->set_message('Incorrect username or password');
                                //echo "heloo";
                                 }
                        
                           else {
                               
                               
                               
                $config['upload_path'] = 'application/views/folder/';
		$config['allowed_types'] = 'pdf|xml|txt';
		$config['max_size']	= '1000';
		$config['max_width']  = '1024';
		$config['max_height']  = '768';

                //$this->upload->initialize($config);
		$this->load->library('upload', $config);
                    
		if (!$this->upload->do_upload())
		{
			$error = array('error' => $this->upload->display_errors());
                         $this->load->view('header');
                         $this->load->view('sidebar');
			$this->load->view('paperSubmission_view',$error);
                         $this->load->view('footer');
                                 
                                       
		}
		else
		{           
			$data = $this->upload->data();
                        
                        $filename = $data['file_name'];
                        $title   =   $this->input->post('title');
                        $abstract   =   $this->input->post('abstract');
                        $author   =   $this->input->post('author');        
                        $this->load->model('file_model');
                         $datas = $this->file_model->insert_file($filename,$title, $abstract,$author);
                        $this->file_model->MultipleAuthors();
                        $this->file_model->Insert_Track();
                         if($datas)
                                 {               
                                    // echo "you have successfully made your paper submission";
                                    $this->session->set_flashdata('feedback', '<div class="alert alert-success" <a class="close pull-right" data-dismiss="alert">×</a>you have successfully made your paper submission....!!</div>');
                                    redirect('upload/test');
                                }
                                  else
                                     {
                                           echo "faillled";
                                     }
              
                         
                           }
		}
         }
         function test(){
              $this->load->model('paperSub_model');
             //$this->load->view('paperSub_Controller/index');
              $this->load->model('paperSub_Model');
              $name=$this->paperSub_Model->getConName();
              $data['track']=$this->paperSub_model->GetTrack();
              $today= date('Y-m-d H:i:s');
               if($name < $today){
    
                     $this->load->view('header');
                     $this->load->view('sidebar');
                     $this->load->view('DeadLine_View');
                      $this->load->view('footer');
             }
                    elseif($name > $today)     
             {
                     $this->load->view('header');
                     $this->load->view('sidebar');
                     $this->load->view('paperSubmission_View',$data);
                    $this->load->view('footer');
    }
         }
       
}
