<?php
class settings_controller extends CI_Controller{
function index(){
         $session_data = $this->session->userdata('logged_in');
         $data=array();
        $data['Email'] = $session_data['Email'];
        $data['UserName'] = $session_data['UserName'];
        $data['Phone'] = $session_data['Phone'];
        
          if($this->input->post('submit')){
          $this->load->library('form_validation');
          $this->form_validation->set_rules('email','Email','required|valid_email');
          $this->form_validation->set_rules('username','UserName','required');
          $this->form_validation->set_rules('Phone', 'Phone number', 'required|regex_match[/^[0-9().-]+$/]'); 
         
           if ($this->form_validation->run()===FALSE){
               
                 $this->load->view('header');
                 $this->load->view('sidebar');
                 $this->load->view('settings_view',$data);
                 $this->load->view('footer');
            }
            else{
                $Email=$this->input->post('email');
                $UserName=$this->input->post('email');
                $dat['Email'] = $this->input->post('email');
                $dat['UserName'] = $this->input->post('username');
                $dat['Phone'] = $this->input->post('Phone');
                $this->load->model('Settings_Model');
                $this->settings_model->edit_info();
                $this->session->set_userdata('Email', $Email);
                 $this->session->set_userdata('UserName', $UserName);
                $this->load->view('header','refresh');
                 $this->load->view('sidebar');
                 $this->load->view('settings_view',$dat);
                 $this->load->view('footer');
                $this->session->set_flashdata('feedback', '<div class="alert alert-success" <a class="close pull-right" data-dismiss="alert">×</a>Your Details Changed Sucsessfully...!!!</div>'); 
                 //redirect('Settings_Controller/settings'); 
            }
          
    }
}
function ChangePwd(){
     if($this->input->post('submit')){
          $this->load->library('form_validation');
          
          $this->form_validation->set_rules('pwd','your New password','required');
          $this->form_validation->set_rules('npwd', 'Password', 'required|matches[pwd]'); 
          
                
           if ($this->form_validation->run()===FALSE){
               
                $this->settings();
            }
            else{
                $this->load->model('settings_model');
                $this->settings_model->Change_Password();
                $this->settings();
                $this->session->set_flashdata('feedback', '<div class="alert alert-success" <a class="close pull-right" data-dismiss="alert">×</a>Password Changed Sucsessfully...!!!</div>'); 
               // echo '<div class="alert alert-success" <a class="close pull-right" data-dismiss="alert"></a>Successfully added!</div>';
            }

          }
}
function settings(){
     
     $session_data = $this->session->userdata('logged_in');
     $data['Email'] = $session_data['Email'];
     $data['UserName'] = $session_data['UserName'];
     $data['Phone'] =$session_data['Phone']; 
    
     $this->load->view('header');
     $this->load->view('sidebar');
    $this->load->view('settings_view',$data);
    $this->load->view('footer');
}
}
?>
