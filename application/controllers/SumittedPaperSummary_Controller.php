<?php

class SumittedPaperSummary_Controller extends CI_Controller {

    function index() {
        $this->load->model('authorPaperEdit_model');
        $data['row']=$this->authorPaperEdit_model->getAuthorPaperDetails();
        $this->load->view('header');
        $this->load->view('sidebar');
        $this->load->view('ViewSubmitPaperSummary_View',$data);
        $this->load->view('footer');  
    }

}

?>
