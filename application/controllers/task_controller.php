<?php
class task_controller extends CI_Controller{
    function CommitteEmail(){
        $this->load->model('task_model');
        $data['email']=$this->task_model->GetCommittemember();
        $this->load->view('header');
        $this->load->view('sidebar');
        $this->load->view('createTask_view',$data);
        $this->load->view('footer');
    }
   function AddCommittee(){
             
              $this->load->library('email','form_validation');
               $this->load->view('header');		
                       $this->load->view('sidebar');
		       $this->load->view('addcommitee_view');
                        $this->load->view('footer');
             $this->load->model('task_model');
             if($this->input->post('submit')){
               
	          $this->form_validation->set_rules('Email','Email','required|valid_email');
                 if($this->form_validation->run()==FALSE)
	         {
		      
                      $this->session->set_flashdata('feedback', '<div class="alert alert-danger" <a class="close pull-right" data-dismiss="alert">×</a>Invalid email....!!</div>');
                       redirect('task_controller/load');
		}
                else{
                    $email=$this->input->post('Email');
		    $type='4';
                    $pwd=$this->RandomPassword();
                    $this->Task_Model->add($email,$type,$pwd);
                   // $this->load->view('');
                    $this->email->from('your@example.com');
                    $this->email->to($email);
                    $this->email->subject('Login Details');
                    $this->email->message("Dear User,\n\n You are added to conference management system.Your login details are\n\n Password :" .$pwd."\n\n Email :" .$email.  "\n\n Thank You");
                    $this->email->send();
                    $this->session->set_flashdata('feedback', '<div class="alert alert-success" <a class="close pull-right" data-dismiss="alert">×</a>Comittee member Added Sucsessfully....!!</div>');
                    redirect('task_controller/load');
                }
                    
             }
             
         }
         function load(){
           $this->load->view('header');
           $this->load->view('sidebar');
           $this->load->view('addcommitee_view');
           $this->load->view('footer');
           
       }
       function RandomPassword(){
            $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
            $pass = array(); 
            $alphaLength = strlen($alphabet) - 1; 
            for ($i = 0; $i < 8; $i++) {
             $n = rand(0, $alphaLength);
             $pass[] = $alphabet[$n];
    }
         return implode($pass); 
       }
       function AddingTask(){
             $this->load->model('Task_Model');
              $data['email']=$this->Task_Model->GetCommittemember();
          if($this->input->post('submit')){
             $this->load->library('form_validation');
             $this->form_validation->set_rules('name', 'Task name', 'required');
               $this->form_validation->set_rules('desc', 'Task description', 'required');
             if($this->form_validation->run() == FALSE)
              {
                   $this->load->view('header');  
                   $this->load->view('sidebar');  
                  $this->load->view('createTask_view',$data);  
                   $this->load->view('footer');                    
                 
              }
              else{
                   $this->Task_Model->addTask(); 
                   $this->Email();
                   $this->session->set_flashdata('feedback', '<div class="alert alert-success" <a class="close pull-right" data-dismiss="alert">×</a>Task Added Sucsessfully....!!</div>');
                    redirect('Task_Controller/CommitteEmail');
                         
                  
                   
              }
          }
       }
        function Email(){
         $this->load->library('email');
         $Email=$this->input->post('email');
         $this->email->from('admin@cmt.com');
         $this->email->to($Email);
         $this->email->subject('Task Details');
         $this->email->message("Dear User\n\n your are assigned to a new task. Please login to the system for more details\n\n Thank you");
         $this->email->send();
        
    }
       function GetTask(){
            $this->load->model('Task_Model');
            $data['task']=$this->Task_Model->GetTask();
            $this->load->view('header');  
            $this->load->view('sidebar');  
            $this->load->view('ViewTask_view',$data);  
             $this->load->view('footer');       
       }
        
       function confiset()
       {
           $this->load->library('form_validation');
                        $this->form_validation->set_rules('upload', 'Path','required', 'trim|required|xss_clean');
                        $this->form_validation->set_rules('Type', 'Type','required', 'trim|required|xss_clean');  
                        $this->form_validation->set_rules('Size', 'Size','required', 'trim|required|xss_clean');  
                        $this->form_validation->set_rules('Width', 'Width','required', 'trim|required|xss_clean');  
                        $this->form_validation->set_rules('Height', 'Height','required', 'trim|required|xss_clean');
                         $session_data = $this->session->userdata('logged_in');	
                        $data['Email'] = $session_data['Email'];
                        $data['ConName']=$session_data['ConName'];
                        if (!$this->form_validation->run())
                        {
                             $this->load->view('header');
        $this->load->view('sidebar');
        $this->load->view('confi_view',$data);
        $this->load->view('footer');
                        }
       
       
       else{
              
                        $upload  =   $this->input->post('upload');
                        $Type   =   $this->input->post('Type');
                        $Size   =   $this->input->post('Size');
                        $Width      =   $this->input->post('Width');
                         $Height      =   $this->input->post('Height');
                         
                         
                            $this->load->model('task_model');
                        
                        $datas = $this->task_model->confis($upload,$Type,$Size, $Width,$Height);
                       
                         if($datas)
                                 {               
                                     echo "you have successfully made your paper submission";
                                    //$this->session->set_flashdata('feedback', '<div class="alert alert-success" <a class="close pull-right" data-dismiss="alert">×</a>you have successfully made your paper submission....!!</div>');
                                    //redirect('upload/test');
                                }
                                  else
                                     {
                                           echo "faillled";
                                     }
                         
                     
                         
       }
      
       
}

    function emailhistory()
    {
        $this->load->model('task_model');
        $data['row']=$this->task_model->emailhis();
        $this->load->view('header');
        $this->load->view('sidebar');
        $this->load->view('Emailhistory_view',$data);
        $this->load->view('footer');
        
        
        
        
    }

       
       
}
?>
