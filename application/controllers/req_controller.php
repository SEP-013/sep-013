<?php

class req_controller extends CI_Controller{
    
            
function index($id){
        $this->load->library('email');
        $this->load->model('req_model');
        $email= $this->req_model->getEmail($id);
        $ShortNmae= $this->req_model->ShortNmae($id);
        $type='1';
        $date=date('Y-m-d H:i:s');
        $pwd=$this->randomPassword();
        $this->req_model->addchair($email,$type,$pwd,$id);
        $this->req_model->approverequest($id);
         $this->req_model->adddeadline($id,$date,$date,$date);
        $this->email->from('admin@admin.com');
        $this->email->to($email);
        $this->email->subject('Login Details');
        $this->email->message($pwd . anchor('req_controller/loginPage/' . $ShortNmae));
        $this->email->send();
        $this->session->set_flashdata('feedback', '<div class="alert alert-success" <a class="close pull-right" data-dismiss="alert">× </a>Conference Approved Sucsefully</div>');
        redirect('req_controller/pendingreq');
    
   
}
function loginPage(){
     $id = $this->uri->segment(3);
    $this->load->model('req_model');
     $data['row']= $this->req_model->ll($id);
      $this->load->view('login_view',$data);
    
}

function reject($id){

     $this->load->model('req_Model');
     $this->req_model->rejectrequest($id);
     $this->session->set_flashdata('feedback', '<div class="alert alert-info" <a class="close pull-right" data-dismiss="alert">×</a>Conference Rejected</div>');
     redirect('home');
}

function pendingreq(){
        $this->load->model('req_model');
        $data['row'] = $this->req_model->GetConference();     
        $this->load->view('header');
        $this->load->view('sidebar');
        $this->load->view('pendingCon_view',$data);
        $this->load->view('footer');
 }
 function ApprovedConference(){
         $this->load->model('req_model');
         $data['row'] = $this->req_model->GetApprovedConference();
         $this->load->view('header');
         $this->load->view('sidebar');
         $this->load->view('approvedReq_view',$data);
         $this->load->view('footer');
        
    }
    function RejectedConference(){
         $this->load->model('req_model');
         $data['row'] = $this->req_model->GetRejectedConference();
         $this->load->view('header');
         $this->load->view('sidebar');
         $this->load->view('rejectedReq_view',$data);
         $this->load->view('footer');
    }
    function randomPassword() {
    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
    $pass = array(); 
    $alphaLength = strlen($alphabet) - 1; 
    for ($i = 0; $i < 8; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); 
}
}
?>
