<?php
class paperbids_controller extends CI_Controller{
    function DisplayForBids(){
        $this->load->model('paperbids_model');
        $data['paper']=$this->paperbids_model->GetPaper();
        
          $this->load->model('deadlinelast');
    $end=$this->deadlinelast->getdeadlineforBid();
	
	$status=$this->deadlinelast->bidstatus();
  
   	date_default_timezone_set('Asia/Colombo');

    $today=date('Y-m-d H:i:s');
    if(($end>$today) && ($status==1)){
        $this->load->view('header');
        $this->load->view('sidebar');
        $this->load->view('paperbids_view',$data);
        $this->load->view('footer');
    }
     else{
      
    $this->load->view('header');
    $this->load->view('sidebar');
     $this->load->view('deadLine_view');
     $this->load->view('footer');
    
    }
    }
    function bid($pid){
        
      if($this->input->post('submit')){
      $this->load->model('paperbids_model');
      $this->paperbids_model->addbid($pid);  
      $this->session->set_flashdata('feedback', '<div class="alert alert-info" <a class="close pull-right" data-dismiss="alert">×</a>Your bid added sucseefully...</div>');
      redirect('paperbids_controller/DisplayForBids');
      }
      
    }
    function papersforbid(){
         $this->load->model('paperbids_model');
         $data['paper']=$this->paperbids_model->GetPaperForBid();
        $this->load->view('header');
        $this->load->view('sidebar');
        $this->load->view('paper_view',$data);
        $this->load->view('footer');
    }
    function Viewbids($pid){
        $this->load->model('paperbids_model');
         $data['bids']=$this->paperbids_model->GetBidCout($pid);
          $this->load->view('header');
        $this->load->view('sidebar');
        $this->load->view('bids_view',$data);
        $this->load->view('footer');
    }
    
}

?>
