<?php
class SiteSetup_controller extends CI_Controller{
    function  index(){
  
	     $this->load->model('SiteSetup_Model');
         $data['row'] = $this->SiteSetup_Model->getdeadline();
         $this->load->view('header');
         $this->load->view('sidebar');
         $this->load->view('SiteSetup_View',$data);
         $this->load->view('footer');
		 
         if($this->input->post('save'))
		 {
		        $this->SiteSetup_Model->update(); 
            $this->load->view('SiteSetup_View',$data);
            redirect('SiteSetup_Controller/index'); 
		 }
   
    }
    function AuthorDeadLine(){
         $this->load->model('SiteSetup_Model');
         $data['rr'] = $this->SiteSetup_Model->getdeadline();
         $this->load->view('header');
         $this->load->view('sidebar');
         $this->load->view('AuthorDeadLine_View',$data);
         $this->load->view('footer');
    }
    function getConDetails(){
        $this->load->model('SiteSetup_Model');
        $data['row'] = $this->SiteSetup_Model->getConDetails();
        $this->load->view('header');
        $this->load->view('sidebar');
        $this->load->view('ConDetails_view',$data);
        $this->load->view('footer');
    }
    function Addtrack(){
         $this->load->model('SiteSetup_Model');
          if($this->input->post('submit')){
               $this->load->library('form_validation');
               $this->form_validation->set_rules('fields[track][]', 'Track', 'required');
                if($this->form_validation->run() == FALSE)
              {
                   $this->load->view('header');  
                   $this->load->view('sidebar');  
                   $this->load->view('addtrack_view');  
                   $this->load->view('footer');                    
                 
              }
               else{
                  $this->SiteSetup_Model->Addtrack();
                   $this->session->set_flashdata('feedback', '<div class="alert alert-success" <a class="close pull-right" data-dismiss="alert">×</a>Track Added Sucsessfully....!!</div>');
                  redirect('siteSetup_controller/loadtrack');
                  
          }
          }
         
    }
    function loadtrack(){
        $this->load->view('header');  
        $this->load->view('sidebar');  
        $this->load->view('addtrack_view');  
        $this->load->view('footer');   
    }
}

?>
