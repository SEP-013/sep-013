<?php
class reviewConf_controller extends CI_Controller{
    function InsertQuestions(){
       
         $this->load->model('reviewConf_model');
          if($this->input->post('submit')){
            $this->load->library('form_validation');
             $this->form_validation->set_rules('fields[stop][]', 'Question', 'required');
              if($this->form_validation->run() == FALSE)
              {
                 
                 $this->load->view('header');
                 $this->load->view('sidebar');
                 $this->load->view('review_form');
                 $this->load->view('footer');
                                      
              }
              else{
            $this->reviewConf_model->InsertQuestion();
            $this->reviewConf_model->InsertDis();
            $this->session->set_flashdata('feedback','<div class="alert alert-success" <a class="close pull-right" data-dismiss="alert">×</a>Question added to reviewer Sucsessfully....!!</div>');
            redirect('reviewConf_controller/load');
            }
       }
    }
    function load(){
        $this->load->view('header');
        $this->load->view('sidebar');
        $this->load->view('review_form');
        $this->load->view('footer');
    }
}

?>
