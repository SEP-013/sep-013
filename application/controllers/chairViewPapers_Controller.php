<?php

class chairViewPapers_controller extends CI_Controller{
    function index(){
        $this->load->model('chairViewPapers_model');
       $data['row']=  $this->chairViewPapers_model->getPapers();
      
        $this->load->view('header');
        $this->load->view('sidebar');
        $this->load->view('viewActive_papers',$data);
        $this->load->view('footer');
    }
    function download($id){
        $this->load->helper('download');
        $this->load->model('chairViewPapers_model');
        $path=  $this->chairViewPapers_model->getPaperPath($id);
        $con='application/views/folder/';
       
        $name =$this->chairViewPapers_model->getPaperNmae($id);
        $data = file_get_contents("application/views/folder/$name");
        force_download($name,$data);
        
    }
    function UpdeateDecision(){
        if($this->input->post('save')){
         $this->load->model('chairViewPapers_model');
         $id=$this->input->post('decc');
         $this->chairViewPapers_model->UpdateDecision($id);
         $data['row']= $this->chairViewPapers_model->GetDetails($id);
        $data['rev']= $this->chairViewPapers_model->GetReviews($id);
          $this->load->view('header');
        $this->load->view('sidebar');
        $this->load->view('viewRview_view',$data);
        $this->load->view('footer');
        
         
        }
    }
    function PaperDetails($id){
         $this->load->model('chairViewPapers_model');
        $data['row']= $this->chairViewPapers_model->GetDetails($id);
        $data['rev']= $this->chairViewPapers_model->GetReviews($id);
          $this->load->view('header');
        $this->load->view('sidebar');
        $this->load->view('viewRview_view',$data);
        $this->load->view('footer');
    }
            function DeletedPapers(){
        $this->load->model('chairViewPapers_model');
        $data['row']=  $this->chairViewPapers_model->getDetetedPapers();
        $this->load->view('header');
        $this->load->view('sidebar');
        $this->load->view('viewDelete_papers',$data);
        $this->load->view('footer');
    }
   
    }
    
    
   

?>
