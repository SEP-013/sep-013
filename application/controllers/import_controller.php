<?php
class import_controller extends CI_Controller{
     function __construct() {
        parent::__construct();
        $this->load->model('import_model');
        $this->load->library('csvimport');
         $this->load->library('parser');
          $this->load->helper('string');  
      

    }

    function index() {
        $data['addressbook'] = $this->import_model->get_addressbook();
        $this->load->view('header');
        $this->load->view('sidebar');
        $this->load->view('bulkinvite_view', $data);
         $this->load->view('footer');
        
    }
    function RejectReq(){
        $data['addressbook'] = $this->import_model->GetRjectedRequest();
        $this->load->view('header');
        $this->load->view('sidebar');
        $this->load->view('rejectedinvitation_view', $data);
         $this->load->view('footer');
    }
    function AcceptReq(){
        $data['addressbook'] = $this->import_model->GetApprovedRequest();
        $this->load->view('header');
        $this->load->view('sidebar');
        $this->load->view('acceptinvitation_view', $data);
         $this->load->view('footer');
    }


    function importcsv() {
         $this->load->helper('string');  
        $data['addressbook'] = $this->import_model->get_addressbook();
        $data['error'] = '';   

        $config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'csv';
        $config['max_size'] = '1000';

        $this->load->library('upload', $config);


        
        if (!$this->upload->do_upload()) {
            $data['error'] = $this->upload->display_errors();

           
            $this->load->view('header');
            $this->load->view('sidebar');
             $this->load->view('bulkinvite_View', $data);
            $this->load->view('footer');
        } else {
            $file_data = $this->upload->data();
            $file_path =  './uploads/'.$file_data['file_name'];
            
            if ($this->csvimport->get_array($file_path)) {
                $csv_array = $this->csvimport->get_array($file_path);
                 $result=  $this->import_model-> CheckReviwerExist();
                foreach ($csv_array as $row) {
                  $activationcode =  random_string('alnum', 16);
                   $session_data = $this->session->userdata('logged_in');
                  $Conid= $session_data['ConID'];
                  $email=$row['email'];
                 $result=  $this->import_model-> CheckReviwerExist($email,$Conid);
                 if($result){
                    $insert_data = array(
                        'firstname'=>$row['firstname'],
                        'lastname'=>$row['lastname'],
                        'phone'=>$row['phone'],
                        'email'=>$row['email'],
                        'activationcode'=>$activationcode,
                        'accept'=>'P',
                        'ConID'=>$Conid,
                        'added'=>'N'
                        
                    );
                    $this->import_model->insert_csv($insert_data);
                 
                     $email= $row['email'];
                     $fname=$row['firstname'];
                     $lname=$row['lastname'];
                      
                       
                        
                  $this->load->library('email');
                     
                      $this->email->from('hiflan.rox@gmail.com');
                     $this->email->to($email);
                     $this->email->subject('Paper Details');
                   // $this->email->message("Dear  $fname $lname\n\n.Your are invited as a reviwer for\n\n . anchor('import_controller/account_activation/' . $activationcode,'Accept Invitation') 
                    //  .anchor('import_controller/rejectInvitation/' . $activationcode,  '<br/><br/>Reject Invitation')");
                     $this->email->message('Dear'  .$fname .$lname .'r\n\ Your are invited as a reviwer for\n\n' . anchor('import_controller/account_activation/' . $activationcode,'Accept Invitation') 
                      .anchor('import_controller/rejectInvitation/' . $activationcode,  '<br/><br/>Reject Invitation'));
                    $this->email->send();
                   
                    
                
                }
               
                }
                
               $this->session->set_flashdata('success', "Data Imported Succesfully");
                    
                    redirect('import_controller/index');
     
               
                
               
               
            } else 
                $data['error'] = "Error occured";
              
               redirect('import_controller/index');
               echo $email;
        
            }
            
        } 
        function add_user($id){
        
             $password =  random_string('alnum', 8);
             $uname=$this->import_model->insert($id)->firstname;
            $lname=$this->import_model->insert($id)->lastname;
             $email=$this->import_model->insert($id)->email;
             $phone=$this->import_model->insert($id)->phone;
             $conid=$this->import_model->insert($id)->conid;
             $insert_data=array(
                 'firstname' => $uname,
                  'lastname' => $lname,
                 'email'=>$email,
                 'phone'=>$phone,
                 'UserType'=>'2',
                 'password'=>$password,
                 'ConID'=>$conid
                 
                 
             );
            $this->import_model->insert_user($insert_data);
             $this->import_model->updateAdded($id);
             $this->email($email,$password);
            $this->session->set_flashdata('success', 'Reviwer added Succesfully');
            redirect('import_Controller/AcceptReq');
        }
                function account_activation() {
            $this->load->model('import_model');
            $register_code = $this->uri->segment(3);
            if ($register_code == '')    {
                
                echo 'error';
                exit();
            }
            $reg_confirm = $this->import_model->confirm_registration($register_code);
         
            if($reg_confirm)    {
                echo 'You have Accepted the invitation.You will receve login information shortly';
            }
            else {
                echo 'you have failed to Accept';
            }
            
        } 
        function rejectInvitation() {
            $this->load->model('import_model');
            $register_code = $this->uri->segment(3);
            if ($register_code == '')    {
                
                echo 'error';
                exit();
            }
            $reg_confirm = $this->import_model->rejectInvitation($register_code);
         
            if($reg_confirm)    {
                echo 'You have rejected the Invitation';
            }
            else {
                echo 'Erorr';
            }
            
        } 
        function email($msg,$email){
            $this->load->library('email');
            $this->email->from('hiflan.rox@gmail.com');
            $this->email->to($email);
            $this->email->subject('Login Details');
            $this->email->message("Dear User\n\n Thank you for accept our request.Your login details\n\n $email\n\n $msg");
            $this->email->send();
        }
        //email
        function Bulkemail(){
          //  $this->load->view('header');
          //  $this->load->view('sidebar');
          //  $this->load->view('sendemai_viewl');
          //  $this->load->view('footer');
        if($this->input->post('submit')){
			
			$this->load->library('form_validation');
			$this->form_validation->set_rules('subject','subject','required');
                        $this->form_validation->set_rules('msg','mesage','required');

				if($this->form_validation->run()==FALSE)
					{
                                                $this->load->view('header');
                                                $this->load->view('sidebar');
						$this->load->view('sendemai_viewl');
                                                $this->load->view('footer');
					}
					else{
			
			$this->load->model('import_model');
			$type=$this->input->post('type');
                        $message=  $this->input->post('msg');
                        $sub=$this->input->post('subject');
                        if ($type==1){
                            
                        
                           $data=$this->import_model->getReviwers();  
                          
                                               
                           
                           foreach ($data as  $address) {
                               
                           $mail = $address->Email;
                            $this->load->library('email');
                            $this->email->from('chair@cmt.com');
                            $this->email->to($mail);
                            $this->email->subject($sub);
                            $this->email->message($message);
                            $this->email->send();
                            $this->import_model->insert_email($mail,$sub,$message);
                            $this->session->set_flashdata('feedback', '<div class="alert alert-success" <a class="close pull-right" data-dismiss="alert">×</a>Email sent Sucsessfully</div>');
                            redirect('import_controller/loadEmail');
                            
                            }
                            
                        }
                        elseif($type==2){
                            
                            $authorEmail=  $this->import_model-> getAuthors();
                            foreach ($authorEmail as $address){
                             $mail=$address->Email;
                            $this->load->library('email');
                            $this->email->from('chair@cmt.com');
                            $this->email->to($mail);
                            $this->email->subject($sub);
                            $this->email->message( $message);
                            $this->email->send();
                            $this->import_model->insert_email($mail,$sub,$message);
                            $this->session->set_flashdata('feedback', '<div class="alert alert-success" <a class="close pull-right" data-dismiss="alert">×</a>Email sent Sucsessfully</div>');
                            redirect('import_controller/loadEmail');
                           
                        }
			
                        }
		}
	
	}
        }
        function loadEmail(){
            $this->load->view('header');
            $this->load->view('sidebar');
            $this->load->view('sendemai_viewl');
            $this->load->view('footer');
        }
        function export(){
       $this->load->library('export');
       $this->load->model('viewReviwer_model');
      $sql = $this->viewReviwer_model->ExportRevs();
      $this->export->to_excel($sql, 'viewReviwer_view'); 
       }
        

        
}

?>
