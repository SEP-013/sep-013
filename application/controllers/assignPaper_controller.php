<?php
class AssignPaper_Controller extends CI_Controller{
    function PaperTitle(){
        $this->load->model('assignPaper_model');
        $data['title']=$this->assignPaper_model->GetPaperTitle();
         $data['email']=$this->assignPaper_model->GetRewiver();
        $this->load->view('header');
        $this->load->view('sidebar');
        $this->load->view('assignPaper_view',$data);
        $this->load->view('footer');
    }
    function AssignPaper(){
        $this->load->model('assignPaper_model');
          if($this->input->post('submit')){
            $this->load->library('form_validation');
             $this->form_validation->set_rules('email', 'Email', 'required|callback_CheckPaper');
              if($this->form_validation->run() == FALSE)
              {
                  $data['title']=$this->assignPaper_model->GetPaperTitle();
                 $data['email']=$this->assignPaper_model->GetRewiver();
                 $this->load->view('header');
                 $this->load->view('sidebar');
                 $this->load->view('assignPaper_view',$data);
                 $this->load->view('footer');
                                      
              }
              else{
            $this->assignPaper_model->AssignPaper();
            $this->EmailRev();
            $this->session->set_flashdata('feedback','<div class="alert alert-success" <a class="close pull-right" data-dismiss="alert">×</a>Paper Assigned to reviewer Sucsessfully....!!</div>');
            redirect('assignPaper_controller/PaperTitle');
            }
       }
        
    }
    function EmailRev(){
          
         $this->load->library('email');
         $Email=$this->input->post('email');
         $Title=$this->input->post('Paper');
         $this->load->model('AssignPaper_Model');
         $id=$this->assignPaper_model->GetPaperId();
         $this->email->from('hiflan.rox@gmail.com');
         $this->email->to($Email);
         $this->email->subject('Paper Details');
         $this->email->message("Dear User\n\n your are assigned to a new paper for reviewing Process. Paper details are as follow\n\n,paper id: $id\n\n paper Title: $Title\n\n Thank you");
         $this->email->send();
        
    }
    //reviewer code
    function GetAssignedPaper(){
        $this->load->model('assignPaper_model');
        $data['row']=$this->assignPaper_model->GetAssignedPaper();
        $this->load->view('header');
        $this->load->view('sidebar');
        $this->load->view('viewAssignedpapers_view',$data);
        $this->load->view('footer');
    }
    function GetPpaerDetails($pid){
        $this->load->model('assignPaper_model');
        $data['row']=$this->assignPaper_model->GetAssignedPaperDetails($pid);
        $this->load->view('header');
        $this->load->view('sidebar');
        $this->load->view('viewpaper',$data);
        $this->load->view('footer');
        
    }
    function PapersForUnassigned(){
        $this->load->model('assignPaper_model');
        $data['row']=$this->assignPaper_model-> GetpapersForUnassigned();
        $this->load->view('header');
        $this->load->view('sidebar');
        $this->load->view('assignedPaper_view',$data);
        $this->load->view('footer');
        
    }
    function Unassign($id){

     $this->load->model('assignPaper_model');
     $this->assignPaper_model->UnassignPaper($id);
     $this->session->set_flashdata('feedback', '<div class="alert alert-info" <a class="close pull-right" data-dismiss="alert">×</a>Paper unassigned</div>');
     redirect('assignPaper_controller/PapersForUnassigned');
    }
     function CheckPaper(){
           $this->load->model('assignPaper_model');
            $result = $this->assignPaper_model->CheckPaperAssigned();
 
        if($result)
        {
            return TRUE;    
        }
 
        else
        {
            $this->form_validation->set_message('CheckPaper', 'This paper alredy assigned to this user!');
            return FALSE;
        }
    }
    function download($id){
      
        $this->load->model('chairViewPapers_model');
        $path=  $this->chairViewPapers_model->getPaperPath($id);
        $con='application/views/folder/';
        $name =$this->chairViewPapers_model->getPaperNmae($id);
        $data = file_get_contents("application/views/folder/$name");
        force_download($name,$data);
        
    }
}

?>
