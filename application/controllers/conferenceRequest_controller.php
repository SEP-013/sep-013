<?php
class conferenceRequest_controller extends CI_Controller{
    function index(){
        $this->load->view('conferenceRequest_view');
    }
    function ConReq(){
        $this->load->model('conferenceRequest_model');
        if($this->input->post('submit'))
        {
             $this->load->library('form_validation');
             
             
             $this->form_validation->set_rules('ConName','Conference Name','required|callback_CheckName');
             $this->form_validation->set_rules('SName','Conference Short Name','required|callback_CheckName');
             $this->form_validation->set_rules('Web','Conference Web Site','required|callback_valid_url_format');
             $this->form_validation->set_rules('Oname','Oranizer Name','required');
             $this->form_validation->set_rules('Cnum','Organizer Contact Number','required|regex_match[/^[0-9().-]+$/]');
             $this->form_validation->set_rules('country','Country','required|callback_country_check');
             $this->form_validation->set_rules('Date','Conference Date','required|callback_CheckDate');
             $this->form_validation->set_rules('Email','Email','required|valid_email');
             $this->form_validation->set_rules('info','Additiona Information','max_length[500]');
             if ($this->form_validation->run()===FALSE){
                $this->load->view('conferenceRequest_view');
            }
            
            else{
            $this->conferenceRequest_model->ConReq();
            $this->load->view('login_view');
            }
        }
      
    }
     
    public function CheckName()
    {
        $this->load->model('conferenceRequest_model');
        $result = $this->conferenceRequest_model->ConferenceName();
 
        if($result)
        {
            return TRUE;    
        }
 
        else
        {
            $this->form_validation->set_message('CheckName', 'This Conference Name already exists!');
            return FALSE;
        }  
    }
    function CheckDate(){
        $today=date('Y-m-d');
        $date=$this->input->post('Date');
        if($today<$date){
            return TRUE;
        }
        else{
          $this->form_validation->set_message('CheckDate', 'Invalid Date!');
            return FALSE;  
        }
        
    }
     function valid_url_format($str){
        $pattern = "|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i";
        if (!preg_match($pattern, $str)){
         $this->form_validation->set_message('valid_url_format', 'The URL you entered is not correctly formatted.');
            return FALSE;
        }
 
        return TRUE;
    }  
     public function country_check()
    {
            if ($this->input->post('country') == 'Select Country')  {
            $this->form_validation->set_message('country_check', 'Please choose a country.');
            return FALSE;
        }
        else {
            return TRUE;
        }
    }
}

?>
