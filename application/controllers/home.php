<?php
session_start();
class home extends CI_Controller{
function index()
 {
   if($this->session->userdata('logged_in'))
   {
     $session_data = $this->session->userdata('logged_in');
     $UserType=$session_data['UserType'];
     if ($UserType==1){
        // $this->load->view('header');
        //  $this->load->view('sidebar');
        //  $this->load->view('test-1');
        //  $this->load->view('footer');
         redirect('siteSetup_controller/getConDetails');  
         // redirect('import_controller/index');      
        //   redirect('impersonate_controller/test');
     }
     elseif($UserType==0){
          $this->load->model('req_model');
          $data['row']=$this->req_model->GetConference();
          $this->load->view('header');
          $this->load->view('sidebar');
          $this->load->view('pendingCon_view',$data);
          $this->load->view('footer');
        
     }
     elseif($UserType==2){
         
          //$this->load->view('header');
       //   $this->load->view('sidebar');
          //$this->load->view('test-1');
         // $this->load->view('footer');
         redirect('siteSetup_controller/getConDetails');  
                 
        
     }
     elseif($UserType==3){
       redirect('siteSetup_controller/AuthorDeadLine');  
       //  $this->load->view('ss');
     }
     elseif($UserType==4){
       redirect('task_controller/GetTask');  
       
     }
   }
   else
   {
   
    
        redirect('req_controller/loginPage');
   }
   
 }
  function logout()
 {
      
  
 
   redirect('home/ss');
   $this->session->unset_userdata('logged_in');
   session_destroy();
 
 }
 function admin(){
    $this->load->view('admin_login');
 }
 function ss(){
      $session_data = $this->session->userdata('logged_in');
      $id=$session_data['ConID'];
      $this->load->model('req_model');
      $data['row']= $this->req_model->test($id);
    
      $this->load->view('login_view',$data);
 }
 
}
?>
