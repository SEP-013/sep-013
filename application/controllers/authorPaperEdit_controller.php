<?php
class authorPaperEdit_controller extends CI_Controller{
    function GetPaper(){
        $this->load->model('authorPaperEdit_model');
        $data['row']=$this->authorPaperEdit_model->getpaperdetails();
        
        
            $this->load->model('deadlinelast');
    $end=$this->deadlinelast->getdeadlineforedit();
	
	$status=$this->deadlinelast->bidedit();
  
   	date_default_timezone_set('Asia/Colombo');

    $today=date('Y-m-d H:i:s');
    if(($end>$today) && ($status==1)){
        $this->load->view('header');
        $this->load->view('sidebar');
        $this->load->view('authorEdit_view',$data);
        $this->load->view('footer');  
    }
    
     else{
        
    
    $this->load->view('header');
    $this->load->view('sidebar');
     $this->load->view('deadLine_view');
     $this->load->view('footer');
    
    }
        
    }
     function download($id){
      
        $this->load->model('authorPaperEdit_model');
        $path='application/views/folder/';
        $name =$this->authorPaperEdit_model->Download($id);
        force_download($name,$path);
        
    }
    function Delete($id){
     $this->load->model('authorPaperEdit_model');
     $this->authorPaperEdit_model->Delete($id);
     redirect('authorPaperEdit_controller/GetPaper'); 
    }
     function Get_PaperAuthor($id){
        
//        print_r($id);
//        exit();
     $this->load->model('authorPaperEdit_model');
    $re = $this->authorPaperEdit_model->GetPaperAuthor($id);
     //redirect('authorPaperEdit_controller/GetPaper'); 
    
    foreach ($re as $key => $value) {
            $role[$key] = $value;
        }
        
        
        echo json_encode($re);
    
    }
    function Edit_paper(){
   
     $this->load->model('authorPaperEdit_model');
      
     if($this->input->post('submit')){
             $this->load->library('form_validation');
             $this->form_validation->set_rules('title', 'title', 'required');
             $this->form_validation->set_rules('abstract', 'abstract', 'required');
             $this->form_validation->set_rules('pc', 'primary Contact', 'required|email'); 
              if($this->form_validation->run() == FALSE){
                 
                 
                 redirect('authorPaperEdit_controller/GetPaper');
                  // $this->session->set_flashdata('feedback', '<div class="alert alert-success" <a class="close pull-right" data-dismiss="alert">×</a>Email sent Sucsessfully</div>');
              }
              else{
                  
                  $this->authorPaperEdit_model->Edit();
                  redirect('authorPaperEdit_controller/GetPaper');
                 
              }
     }
     
        
    }
     function Edit_PaperAuthor(){
   
     $this->load->model('authorPaperEdit_model');
      
//     print_r($_POST);
//     exit();
     
     if($this->input->post('submit')){
//             $this->load->library('form_validation');
//             $this->form_validation->set_rules('name', 'name', 'required');
//             $this->form_validation->set_rules('number', 'Contact Number', 'required');
//             $this->form_validation->set_rules('email', 'email', 'required|email');
//             
//             
//              if($this->form_validation->run() == FALSE){
//                 
//                  print_r($_POST);
//                  exit();
//                 redirect('authorPaperEdit_controller/GetPaper');
//                  // $this->session->set_flashdata('feedback', '<div class="alert alert-success" <a class="close pull-right" data-dismiss="alert">×</a>Email sent Sucsessfully</div>');
//              }
//              else
                  {
                  
                  $this->authorPaperEdit_model->EditPaperAuthor($_POST);
                  redirect('authorPaperEdit_controller/GetPaper');
                 
              }
     }
     
        
    }
    function Edit_PaperTracks(){
   
     $this->load->model('authorPaperEdit_model');
      
//     print_r($_POST['id']);
//     exit();
     
     if($this->input->post('submit')){
         
         $this->authorPaperEdit_model->DeletePaperTracks($_POST['id']);
               
                        $this->load->model('file_model');
                        $this->file_model->Insert_Track();
         
                 // $this->authorPaperEdit_model->EditPaperAuthor($_POST);
                  redirect('authorPaperEdit_controller/GetPaper');
     }
     
        
    }
    function load(){
         $id = $this->uri->segment(3);
     $this->load->model('authorPaperEdit_model');
      $data['authors']=$this->authorPaperEdit_model->GetPaperAuthors($id);
      
//      print_r($id);
//      exit();
      
      $this->load->model('paperSub_model');
         $tracks  =$this->paperSub_model->GetTrack();
         $paperTracks  =$this->authorPaperEdit_model->GetPaperTracks($id);
         
         $selected[0]='';
         $t = 0;
        foreach ($paperTracks as $key => $track) {
          $selected[$t] = $track->track;
          $t++;
        }
         
        $a=0;
         foreach ($tracks as $key => $value) {
             
             if(in_array($value->trackname, $selected)){ 
                  $newTracks[$a] = array('trackName' => $value->trackname,
                        'id' => $value->id,
                        'selected' => 1);
                 
             }
             else{ 
                    
               $newTracks[$a] =  array('trackName' => $value->trackname,
                        'id' => $value->id,
                        'selected' => 0);
             }
             
             $a++;
         }
         
         $data['track'] = $newTracks;
    // $id = $this->uri->segment(3);
     $this->load->model('authorPaperEdit_model');
     $data['row']=$this->authorPaperEdit_model->EditPaper($id);
     $this->load->view('header');
     $this->load->view('sidebar');
     $this->load->view('authorPaperEdit_form',$data);
     $this->load->view('footer'); 
        
    }
   
    
}

?>
