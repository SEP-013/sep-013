<?php
class ViewReviwer_Model extends CI_Model{
    function getReviwers(){
     $session_data = $this->session->userdata('logged_in');
     $id= $session_data['ConID'];
    $data = array();
    $this->db->select('id,Email,UserName,Phone,firstname,lastname');
    $this->db->from('user');
    $this->db->where(array('ConID'=>$id));
     $this->db->where(array('UserType'=>'2'));
    $query=  $this->db->get();
    if($query->num_rows()>0){
        foreach ($query->result() as $row)
      {
        $data[] = $row;
      }
      return $data;
    }
    else{
        return FALSE;
    }
    }
    function getEmail($id){   
    $this->db->select('Email');
    $this->db->from('user');
    $this->db->where(array('id'=>$id));
    $query=  $this->db->get();
    if($query->num_rows()>0){
        foreach ($query->result() as $row)
      {
        $data[] = $row;
      }
      return $data;
    }
    else{
        return FALSE;
    }
    }
    function InsertEmail(){
        $session_data = $this->session->userdata('logged_in');
        $id= $session_data['ConID'];
        $mail=$session_data['Email'];
        $email=$this->input->post('email');
        $subject=$this->input->post('subject');
        $msg=$this->input->post('msg');
        $date=  date('Y-m-d h:i:s');
         $data = array(
               'CoID' => $id,
               'SentTo' => $email,
               'Subject'=>$subject,
               'Message'=>$msg,
               'Date'=>$date
            );
         
	$this->load->library('email');
        $this->email->from($mail);
	$this->email->to($email);
	$this->email->subject($subject);
	$this->email->message($msg);
	$this->email->send();
        $this->db->insert('email', $data); 
    }
     function getAuthors($pid){
     $session_data = $this->session->userdata('logged_in');
     $id= $session_data['ConID'];
     //$pid=$this->input->post('pid');
    // $pid=43;
    $data = array();
    $this->db->select('id,Email,Name,Phone');
    $this->db->from('author');
    $this->db->where(array('ConID'=>$id));
    $this->db->where(array('pid'=>$pid));
     
    $query=  $this->db->get();
    if($query->num_rows()>0){
        foreach ($query->result() as $row)
      {
        $data[] = $row;
      }
      return $data;
    }
    else{
        return FALSE;
    }
    }
    function add($email,$type,$pwd){
		
		$session_data = $this->session->userdata('logged_in');
		
		$Conid = $session_data['ConID'];
		
		$data = array(
		'Email' => $email,
		'UserType' => $type,
		'ConID' => $Conid,
		'Password'=>$pwd
		);
		
		$this->db->insert('user', $data);
		}
                function ExportRevs() {  
                 $session_data = $this->session->userdata('logged_in');
                 $id= $session_data['ConID'];
                $this->db->select('Email,firstname,lastname,Phone'); 
                //$ConfID = $this->session->userdata['LoggedIn']['ConID'];
               $this->db->from('user');
                $this->db->where('ConID',$id);
                $this->db->where(array('UserType'=>'2'));
               $query = $this->db->get();
                return $query; 
                }
              
}
?>
