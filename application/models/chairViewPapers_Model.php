<?php
class ChairViewPapers_Model extends CI_Model{
    function getPapers(){
     $session_data = $this->session->userdata('logged_in');
     $id= $session_data['ConID'];
    $data = array();
    $this->db->select('paper.ID,paper.FileName,paper.Title,paper.Primarycontact,paper.Decission,author.Name,author.Email');
    $this->db->from('paper');
    $this->  db->join('author', 'author.pid = paper.ID');
    $this->db->where(array('paper.ConID'=>$id));
     $this->db->where(array('paper.Status'=>'A'));
    $query=  $this->db->get();
    if($query->num_rows()>0){
        foreach ($query->result() as $row)
      {
        $data[] = $row;
      }
      return $data;
    }
    else{
        return FALSE;
    }
    }
    function getPaperPath($id){
       
    
    $this->db->select('Path');
    $this->db->from('paper');
    $this->db->where(array('ID'=>$id));
    $query=$this->db->get(); 
    return $query->row()->Path;
    }
    function getPaperNmae($id){
    $this->db->select('FileName');
    $this->db->from('paper');
    $this->db->where(array('ID'=>$id));
    $query=$this->db->get(); 
    return $query->row()->FileName;
    }
    
    function update($id){
      
     $data = array(
	            'Title'=>$this->input->post('title'),
                    'Author'=>$this->input->post('author'),
                    'Primarycontact'=>$this->input->post('pc')
	            
	          );
	  $this->db->where('pid',$id);
	  $this->db->update('paper',$data);  
    }
     function getDetetedPapers(){
     $session_data = $this->session->userdata('logged_in');
     $id= $session_data['ConID'];
    $data = array();
    $this->db->select('ID,FileName,Title,Primarycontact,Decission');
    $this->db->from('paper');
    $this->db->where(array('ConID'=>$id));
     $this->db->where(array('Status'=>'D'));
    $query=  $this->db->get();
    if($query->num_rows()>0){
        foreach ($query->result() as $row)
      {
        $data[] = $row;
      }
      return $data;
    }
    else{
        return FALSE;
    }
    }
    function GetReviews($pid){
     $session_data = $this->session->userdata('logged_in');
     $id= $session_data['ConID'];
    $data = array();
    $this->db->select('reviewans.answer,reviewans.decission,user.Email,user.UserName,reviewquestion.question');
    $this->db->from('reviewans');
    $this->db->join('user', 'user.id=reviewans.submittedby');
    $this->db->join('reviewquestion', 'reviewquestion.id=reviewans.qid');
    $this->db->where(array('reviewans.pid'=>$pid));
    
    $query=  $this->db->get();
    if($query->num_rows()>0){
        foreach ($query->result() as $row)
      {
        $data[] = $row;
      }
      return $data;
    }
    else{
        return FALSE;
    }
    }
    function GetDetails($pid) {
        $session_data = $this->session->userdata('logged_in');
     $id= $session_data['ConID'];
    $data = array();
    $this->db->select('paper.ID,paper.FileName,paper.Title,paper.Abstract,paper.Primarycontact,paper.Decission,author.Name,author.Email');
    $this->db->from('paper');
    $this->db->join('author', 'author.pid=paper.ID');
    $this->db->where(array('paper.ID'=>$pid));
    
    $query=  $this->db->get();
    if($query->num_rows()>0){
        foreach ($query->result() as $row)
      {
        $data[] = $row;
      }
      return $data;
    }
    else{
        return FALSE;
    }
    }
            function UpdateDecision($id){
         $data = array(
	            'Decission'=>$this->input->post('dec')
                    
	          );
	  $this->db->where('ID',$id);
	  $this->db->update('paper',$data);  
    
    }
   
}

?>
