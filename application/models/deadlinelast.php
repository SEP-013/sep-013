<?php

class deadlinelast extends CI_Model{
     function getpapersub(){  
     $session_data = $this->session->userdata('logged_in');
     $id= $session_data['ConID'];
     $data = array();
    $this->db->select('PaperSub');
    $this->db->from('deadline');
    $this->db->where(array('ConID'=>$id));
    $query=$this->db->get(); 
    return $query->row()->PaperSub;
    }
	
	     function bid(){  
     $session_data = $this->session->userdata('logged_in');
     $id= $session_data['ConID'];
     $data = array();
    $this->db->select('status');
    $this->db->from('deadline');
    $this->db->where(array('ConID'=>$id));
    $query=$this->db->get(); 
    return $query->row()->status;
    }
	
	    function getafterpapersub(){  
     $session_data = $this->session->userdata('logged_in');
     $id= $session_data['ConID'];
     $data = array();
    $this->db->select('fromPaperSub');
    $this->db->from('deadline');
    $this->db->where(array('ConID'=>$id));
    $query=$this->db->get(); 
    return $query->row()->fromPaperSub;
    }
	
	 function getdeadlineforedit(){  
     $session_data = $this->session->userdata('logged_in');
     $id= $session_data['ConID'];
     $data = array();
    $this->db->select('EditPaper');
    $this->db->from('deadline');
    $this->db->where(array('ConID'=>$id));
    $query=$this->db->get(); 
    return $query->row()->EditPaper;
    }
         function bidedit(){  
     $session_data = $this->session->userdata('logged_in');
     $id= $session_data['ConID'];
     $data = array();
    $this->db->select('statusedit');
    $this->db->from('deadline');
    $this->db->where(array('ConID'=>$id));
    $query=$this->db->get(); 
    return $query->row()->statusedit;
    }
		 function getafterforedit(){  
     $session_data = $this->session->userdata('logged_in');
     $id= $session_data['ConID'];
     $data = array();
    $this->db->select('fromEditPaper');
    $this->db->from('deadline');
    $this->db->where(array('ConID'=>$id));
    $query=$this->db->get(); 
    return $query->row()->fromEditPaper;
    }
	
	 function getdeadlineforBid(){  
     $session_data = $this->session->userdata('logged_in');
     $id= $session_data['ConID'];
     $data = array();
    $this->db->select('ReviewBid');
    $this->db->from('deadline');
    $this->db->where(array('ConID'=>$id));
    $query=$this->db->get(); 
    return $query->row()->ReviewBid;
    }
    	 function bidstatus(){  
     $session_data = $this->session->userdata('logged_in');
     $id= $session_data['ConID'];
     $data = array();
    $this->db->select('statusbid');
    $this->db->from('deadline');
    $this->db->where(array('ConID'=>$id));
    $query=$this->db->get(); 
    return $query->row()->statusbid;
    }
		 function getafterstartbid(){  
     $session_data = $this->session->userdata('logged_in');
     $id= $session_data['ConID'];
     $data = array();
    $this->db->select('fromReviewBid');
    $this->db->from('deadline');
    $this->db->where(array('ConID'=>$id));
    $query=$this->db->get(); 
    return $query->row()->fromReviewBid;
    }
	
}
?>
