<?php
class Req_model extends CI_Model{
    
function GetConference(){
    $data = array();
    $this->db->select('id,ConName,ShortName,Country,Oname,Onum,Web,AddInfo,ConDate,Email,');
    $this->db->from('conferencereq');
    $this->db->where(array('IsApprove'=>'0'));
    $query=  $this->db->get();
    if($query->num_rows()>0){
        foreach ($query->result() as $row)
      {
        $data[] = $row;
      }
      return $data;
    }
    else{
        return FALSE;
    }
}
function approverequest($id){
    $data = array(
               'IsApprove' => '1',
            );

    $this->db->where(array('id'=>$id));
    $this->db->update('conferencereq', $data); 
}
function rejectrequest($id){
    $data = array(
               'IsApprove' => '2',
            );

    $this->db->where(array('id'=>$id));
    $this->db->update('conferencereq', $data); 
}
function GetApprovedConference(){
    $data = array();
    $this->db->select('id,ConName,ConDate,Email,SubArea,');
    $this->db->from('conferencereq');
    $this->db->where(array('IsApprove'=>'1'));
    $query=  $this->db->get();
    if($query->num_rows()>0){
        foreach ($query->result() as $row)
      {
        $data[] = $row;
      }
      return $data;
    }
    else{
        return FALSE;
    }
    
}
function GetRejectedConference(){
    $data = array();
    $this->db->select('id,ConName,ConDate,Email,SubArea,');
    $this->db->from('conferencereq');
    $this->db->where(array('IsApprove'=>'2'));
    $query=  $this->db->get();
    if($query->num_rows()>0){
        foreach ($query->result() as $row)
      {
        $data[] = $row;
      }
      return $data;
    }
    else{
        return FALSE;
    }
    
}
function addchair($email,$type,$pw,$Conid){
    //$session_data = $this->session->userdata('logged_in');
		
		//$Conid = $session_data['ConID'];
		
		$data = array(
		'Email' => $email,
		'UserType' => $type,
		'ConID' => $Conid,
		'Password'=>$pw
		);
		
		$this->db->insert('user',$data);
}
function adddeadline($conid,$papsub,$editpap,$rewpap){
    //$session_data = $this->session->userdata('logged_in');
		
		//$Conid = $session_data['ConID'];
		
		$data = array(
		'ConID' => $conid,
		'PaperSub' => $papsub,
		'EditPaper' => $editpap,
		'ReviewSub'=>$rewpap
		);
		
		$this->db->insert('deadline',$data);
}
 function getEmail($id){   
    $this->db->select('Email');
    $this->db->from('conferencereq');
    $this->db->where(array('id'=>$id));
    $query=  $this->db->get();
    return $query->row()->Email;
    }
    function ShortNmae($id){   
    $this->db->select('ShortName');
    $this->db->from('conferencereq');
    $this->db->where(array('id'=>$id));
    $query=  $this->db->get();
    return $query->row()->ShortName;
    }
    function ll($id){
    $data = array();
    $this->db->select('ConName');
    $this->db->from('conferencereq');
    $this->db->where(array('ShortName'=>$id));
    $query=  $this->db->get();
    return $query->row()->ConName;
    }
    function test($id){
   // $data = array();
    $this->db->select('ConName');
    $this->db->from('conferencereq');
    $this->db->where(array('id'=>$id));
    $query=  $this->db->get();
    return $query->row()->ConName;
    }
}
?>
