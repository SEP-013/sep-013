<?php

class ConferenceRequest_Model extends CI_Model{
    function ConReq(){
        $ConName=  $this->input->post('ConName');
        $SName=  $this->input->post('SName');
        $Country=$this->input->post('country');
        $Web=$this->input->post('Web');
        $Oname=$this->input->post('Oname');
        $Cnum=$this->input->post('Onum');
        $addInfo=$this->input->post('info');
        $ConDate=$this->input->post('Date');
        $Email=  $this->input->post('Email');
      
        $data=array(
            'ConName'=>$ConName,
            'ShortName'=>$SName,
            'Country'=>$Country,
            'Web'=>$Web,
            'Oname'=>$Oname,
            'Onum'=>$Cnum,
            'AddInfo'=>$addInfo,
            'ConDate'=>date('Y-m-d', strtotime($ConDate)),
            'Email'=>$Email
           
            );
            $this->db->insert('conferencereq',$data);
    }
    function ConferenceName(){
         $ConName=$this->input->post('ConName');
        $result=  $this->db->get_where('conferencereq',array('ConName'=>$ConName));
        if($result->num_rows>0){
            
            return FALSE;
        }
        else{
            return TRUE;
        }
        
    }
}
?>
