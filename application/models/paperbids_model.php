<?php
class Paperbids_Model extends CI_Model{
    function GetPaper(){
     $session_data = $this->session->userdata('logged_in');
     $id= $session_data['ConID'];
     $data = array();
     $this->db->select('ID,Title');
     $this->db->from('paper');
     $this->db->where(array('ConID'=>$id));
     $this->db->where(array('Status'=>'A'));
     $this->db->where(array('Decission'=>'P'));
     $query=  $this->db->get();
    if($query->num_rows()>0){
        foreach ($query->result() as $row)
      {
        $data[] = $row;
      }
      return $data;
    }
    else{
        return FALSE;
    }
    }
  
    
    function addbid($pid){
		
		$session_data = $this->session->userdata('logged_in');
		$Conid = $session_data['ConID'];
                $uid = $session_data['id'];
		$bid=$this->input->post('bid');
                $pid=$this->input->post('paper');
		$data = array(
		'cid' => $Conid,
		'uid' => $uid,
		'bids' => $bid,
                'pid' => $pid
		
		);
		
		$this->db->insert('bid', $data);
		}
                function GetPaperForBid(){
                    $session_data = $this->session->userdata('logged_in');
                    $Conid = $session_data['ConID'];
                     $data = array();
                    $this->db->select('ID,Title');
                    $this->db->from('paper');
                    $this->db->where(array('ConID'=>$Conid));
                     $this->db->where(array('Status'=>'A'));
                    $query=  $this->db->get();
                    if($query->num_rows()>0){
                    foreach ($query->result() as $row)
                    {
                        $data[] = $row;
                    }
                    return $data;
                    }
                    else{
                        return FALSE;
                    }
                    
                }
                function GetBidCout($pid){
                $session_data = $this->session->userdata('logged_in');
                $Conid = $session_data['ConID'];
                 $data = array();
                $this->db->select('user.Email,user.UserName, bid.bids');
                $this->db->from('user');
                 $this->  db->join('bid', 'bid.uid = user.id');
                
                $this->db->where(array('bid.cid'=>$Conid));
                  $this->db->where(array('bid.pid'=>$pid));
                 $query=  $this->db->get();
                if($query->num_rows()>0){
                 foreach ($query->result() as $row)
                {
                    $data[] = $row;
                }
                return $data;
                 }
                 else{
                     return FALSE;
                }
  }
}

?>
