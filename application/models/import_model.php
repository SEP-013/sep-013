<?php
class Import_model extends CI_Model{
     function get_addressbook() {
     $session_data = $this->session->userdata('logged_in');
     $Conid= $session_data['ConID'];
   //  $query = $this->db->get('reviewreq');
     $this->db->select('firstname,lastname,phone,email');
     $this->db->from('reviewreq');
     $this->db->where(array('accept'=>'P'));
     $this->db->where(array('ConID'=>$Conid));
     $query=  $this->db->get();
        if ($query->num_rows() > 0) {
        return $query->result_array();
        } else {
        return FALSE;
        }
    }
     function insert_csv($data) {
        $this->db->insert('reviewreq', $data);
}
 function confirm_registration ($register_code)    {
            
            $val_code = "SELECT email from reviewreq where activationcode = ?";
            $result = $this->db->query($val_code, $register_code);
            
            if ($result->num_rows() == 1) {
                
            $update_activated = "UPDATE reviewreq SET accept = 'Y' WHERE activationcode = ?";
        
            $this->db->query($update_activated, $register_code);
            
            return TRUE;
            }
            else {
                return FALSE;
            }
            
        }  
       function rejectInvitation($code){
            $val_code = "SELECT email from reviewreq where activationcode = ?";
            $result = $this->db->query($val_code, $code);
            
            if ($result->num_rows() == 1) {
                
            $update_activated = "UPDATE reviewreq SET accept = 'N' WHERE activationcode = ?";
        
            $this->db->query($update_activated, $code);
            
            return TRUE;
            }
            else {
                return FALSE;
            }
            
           
       }
       function GetApprovedRequest(){
     $session_data = $this->session->userdata('logged_in');
     
     $Conid= $session_data['ConID'];
     $data = array();
     $this->db->select('id,firstname,lastname,phone,email');
     $this->db->from('reviewreq');
     $this->db->where(array('accept'=>'Y'));
     $this->db->where(array('added'=>'N'));
     $this->db->where(array('ConID'=>$Conid));
     $query=  $this->db->get();
    if($query->num_rows()>0){
        foreach ($query->result() as $row)
      {
        $data[] = $row;
      }
      return $data;
    }
    else{
        return FALSE;
    }
       }
         function GetRjectedRequest(){
   $session_data = $this->session->userdata('logged_in');
     $Conid= $session_data['ConID'];
   //  $query = $this->db->get('reviewreq');
     $this->db->select('id,firstname,lastname,phone,email');
     $this->db->from('reviewreq');
     $this->db->where(array('accept'=>'N'));
     $this->db->where(array('ConID'=>$Conid));
     $query=  $this->db->get();
        if ($query->num_rows() > 0) {
        return $query->result_array();
        } else {
        return FALSE;
        }
       }
       function insert_user($data) {
        $this->db->insert('user', $data);
}
function insert($id){
     $session_data = $this->session->userdata('logged_in');
     
     $Conid= $session_data['ConID'];
     $data = array();
     $this->db->select('id,firstname,lastname,phone,email');
     $this->db->from('reviewreq');
      $this->db->where(array('id'=>$id));
     $this->db->where(array('accept'=>'Y'));
     $this->db->where(array('ConID'=>$Conid));
     $query=  $this->db->get();
    if($query->num_rows()>0){
        foreach ($query->result() as $row)
      {
       // $data['name'] = $row->firstname;
        $this->firstname = $row->firstname;
         $this->firstname = $row->lastname;
        $this->email = $row->email;
        $this->phone = $row->phone;
        $this->ConID = $row->ConID;

      }
      return $row;
    }
    else{
        return FALSE;
    }
    
}
function updateAdded($id){
    $data = array(
               'added' => 'Y',
            );

    $this->db->where(array('id'=>$id));
    $this->db->update('reviewreq', $data); 
}
 function CheckReviwerExist($Email,$ConfID){
            
                               
                $this->db->select('user.Email,reviewreq.email');
                $this->db->from('reviewreq');
                 $this->  db->join('user', 'user.ConID =reviewreq.ConID');
                $this->db->where('reviewreq.ConID',$ConfID);
                 $this->db->where('reviewreq.email',$Email);
                $query = $this->db->get();
 
                if ($query->num_rows()>0)
                {
                return FALSE;
        }
 
        else
        {
                return TRUE;
        }
        }
        //email
       function getReviwers(){
     $session_data = $this->session->userdata('logged_in');
     $id= $session_data['ConID'];
    $data = array();
    $this->db->select('Email');
    $this->db->from('user');
    $this->db->where(array('ConID'=>$id));
     $this->db->where(array('UserType'=>'2'));
    $query=  $this->db->get();
    if($query->num_rows()>0){
        foreach ($query->result() as $row)
      {
        $data[] = $row;
      }
      return $data;
    }
    else{
        return FALSE;
    }
    }
     function getAuthors(){
     $session_data = $this->session->userdata('logged_in');
     $id= $session_data['ConID'];
    $data = array();
    $this->db->select('Email');
    $this->db->from('user');
    $this->db->where(array('ConID'=>$id));
     $this->db->where(array('UserType'=>'3'));
    $query=  $this->db->get();
    if($query->num_rows()>0){
        foreach ($query->result() as $row)
      {
        $data[] = $row;
      }
      return $data;
    }
    else{
        return FALSE;
    }
    }
      function insert_email($email,$sub,$Message) {
          $session_data = $this->session->userdata('logged_in');
          $Conid = $session_data['ConID'];
          date_default_timezone_set('Asia/Colombo');
	  $Date=date('Y-m-d H:i:s');	
		$data = array(
		'SentTo' => $email,
		'CoID' => $Conid,
		'Subject' => $sub,
		'Message'=>$Message,
                'Date'=>$Date    
		);
		
		$this->db->insert('email', $data);
    }


}

?>
