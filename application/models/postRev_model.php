
<?php
class postRev_Model extends CI_Model{
    function GetAssignedPaperID(){
     $session_data = $this->session->userdata('logged_in');
     $id= $session_data['ConID'];
     $uid=$session_data['id'];
     $data = array();
     $sql='select asign.pid,p.Title from assignedpaper asign,paper p where asign.ConID='. $id .' and asign.Active= "Y"and asign.Assignedto='. $uid.' and asign.pid= p.ID' ;
     $query = $this->db->query($sql);

         if($query->num_rows()>0){
             foreach ($query->result() as $row)
           {
             $data[] = $row;
           }

           return $data;
         }
         else{
             return FALSE;
         }
    }
    function InsertReview(){
     $decisson=0;
        if(isset($_POST['Accept'])){
            $decisson='A';
        }
        if(isset($_POST['Reject'])){
            $decisson='R';
        }
     $session_data = $this->session->userdata('logged_in');
     $id= $session_data['ConID'];
     $uid=$session_data['id'];
     $pid=$this->input->post('pid');
     $chaircomment=$this->input->post('ccom');
     $authorcomment=$this->input->post('ccom');
     $date=date('Y-m-d H:i:s',now());
     $data=array('conid'=>$id,'pid'=>$pid,'postby'=>$uid,'commentchair'=>$chaircomment,'commentauthors'=>$authorcomment,'Decission'=>$decisson,'date'=>$date);
     $this->db->insert('comment',$data);
     return true;
        
    }
    function GetComments(){
     $session_data = $this->session->userdata('logged_in');
     $id= $session_data['ConID']; 
     $this->db->select('comment.commentchair,comment.commentauthors,comment.pid,paper.Title,user.Email,user.UserName,comment.date');
     $this->db->from('comment');
     $this->  db->join('paper', 'comment.pid=paper.ID');
     $this->  db->join('user', 'comment.postby=user.id');
     $this->db->where(array('comment.conid'=>$id));
     $query=  $this->db->get();
    if($query->num_rows()>0){
        foreach ($query->result() as $row)
      {
        $data[] = $row;
      }
      return $data;
    }
    else{
        return FALSE;
    }
    }
    function GetReviewedPapers(){
        $session_data = $this->session->userdata('logged_in');
        $id= $session_data['ConID'];
        $sql='SELECT DISTINCT c.pid,p.Title FROM comment c,paper p WHERE c.conid='.$id.' and c.pid=p.ID and p.Decission="P"' ;
        $query = $this->db->query($sql);

        if($query->num_rows()>0){
            foreach ($query->result() as $row)
            {
                $data[] = $row;
            }
            return $data;
        }
        else{
            return FALSE;
        }
    }
    function GetPaperComments($para){
        $session_data = $this->session->userdata('logged_in');
        $id= $session_data['ConID'];
        $this->db->select('comment.commentchair,comment.commentauthors,comment.pid,paper.Title,user.Email,user.UserName,comment.date,comment.Decission');
        $this->db->from('comment');
        $this->  db->join('paper', 'comment.pid=paper.ID');
        $this->  db->join('user', 'comment.postby=user.id');
        $this->db->where(array('comment.conid'=>$id));
        $this->db->where(array('comment.pid'=>$para));
        $query=  $this->db->get();


        if($query->num_rows()>0){
            foreach ($query->result() as $row)
            {
                $data[] = $row;
            }
            return $data;
        }
        else{
            return FALSE;
        }
    }
    function CheckPaperStatus($para){
        $session_data = $this->session->userdata('logged_in');
        $uid=$session_data['id'];
        $sql='SELECT COUNT(*) as count FROM comment WHERE postby = '. $uid .' and pid='. $para ;
        $query = $this->db->query($sql);

        return $query->result();
    }
    function GetAssignedPaperCount($para){
        $sql='SELECT count(pid)as count FROM assignedpaper WHERE pid='.$para.' and Active="Y"' ;
        $query = $this->db->query($sql);
        return $query->result();
    }
    function SetPaperStatus($para1){
      
        $sql='UPDATE paper SET Decission="A" WHERE ID='.$para1 ;
        $query = $this->db->query($sql);
        return $query;
    }
    function SetRevSub($para1){
        $session_data = $this->session->userdata('logged_in');
        $uid=$session_data['id'];
        $sql='UPDATE assignedpaper  SET RevSubmitted="Y" WHERE Assignedto='.$uid.' and pid='.$para1 ;
        $query = $this->db->query($sql);
        return $query;
    }
    function GetSubmittedReviewCount($pid){
       
        $this->db->where('RevSubmitted',"N");
         $this->db->where('pid',$pid);
        $this->db->from('assignedpaper');
        $q=  $this->db->get();
        return $q->num_rows();




       
    }
    
            function GetAccept($pid){
            $this->db->where('decission',1);
            $this->db->where('pid',$pid);
            $this->db->from('reviewans');
            $q=  $this->db->get();
            return $q->num_rows();
        
    }
   function GetReject($pid){
            $this->db->where('decission',2);
            $this->db->where('pid',$pid);
            $this->db->from('reviewans');
            $q=  $this->db->get();
            return $q->num_rows();
        
    }
     function GetNeaturel($pid){
            $this->db->where('decission',3);
            $this->db->where('pid',$pid);
            $this->db->from('reviewans');
            $q=  $this->db->get();
            return $q->num_rows();
        
    }

}

?>
