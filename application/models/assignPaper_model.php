<?php
class AssignPaper_Model extends CI_Model{
    //code for chair panel
    function GetPaperTitle(){
     $session_data = $this->session->userdata('logged_in');
     $id= $session_data['ConID'];
     $data = array();
    $this->db->select('ID,Title');
    $this->db->from('paper');
 
    $this->db->where(array('ConID'=>$id));
     $this->db->where(array('Status'=>'A'));
     $this->db->where(array('Decission'=>'P'));
    $query=  $this->db->get();
    if($query->num_rows()>0){
        foreach ($query->result() as $row)
      {
        $data[] = $row;
      }
      return $data;
    }
    else{
        return FALSE;
    }
    }
    function GetRewiver(){
     $session_data = $this->session->userdata('logged_in');
     $id= $session_data['ConID'];
    $this->db->select('Email');
      $this->db->from('user');
       $this->db->where(array('ConID'=>$id));
     $this->db->where(array('UserType'=>'2'));
      $query=  $this->db->get();
      if($query->num_rows()>0){
        foreach ($query->result() as $row)
      {
        $data[] = $row;
      }
      return $data;
    }
    else{
        return FALSE;
    }
      
    }
    function GetUserId(){
     $session_data = $this->session->userdata('logged_in');
     $id= $session_data['ConID'];
     $Email=$this->input->post('email');
     $this->db->select('id');
     $this->db->from('user');
     $this->db->where(array('Email'=>$Email));
     $this->db->where(array('ConID'=>$id));
     $query = $this->db->get();
     return $query->row()->id; 
     
    }
    function  GetPaperId(){
      $session_data = $this->session->userdata('logged_in');
     $id= $session_data['ConID'];
     $Title=$this->input->post('Paper');
     $this->db->select('ID');
     $this->db->from('paper');
     $this->db->where(array('Title'=>$Title));
     $this->db->where(array('ConID'=>$id));
     $query = $this->db->get();
     return $query->row()->ID; 
    }
     function AssignPaper()
        {
 
                
                $session_data = $this->session->userdata('logged_in');
                $id= $session_data['ConID'];
                $pid = $this->GetPaperId();
                $uid = $this->GetUserId();
                $data=array('conid'=>$id,
                    'pid'=>$pid,
                    'Assignedto'=>$uid,
                    'RevSubmitted'=>'N',
                    'Active'=>'Y');
                $this->db->insert('assignedpaper',$data);
                return true;
        }
        //code for reviewer 
    function GetAssignedPaper(){
        $session_data = $this->session->userdata('logged_in');
        $id= $session_data['ConID'];
        $uid=$session_data['id'];
        $this->db->select('paper.ID,paper.Title');
        $this->db->from('paper');
         $this->  db->join('assignedpaper', 'assignedpaper.pid = paper.ID');
        $this->db->where(array('assignedpaper.conid'=>$id));
        $this->db->where(array('assignedpaper.Active'=>'Y'));
         $this->db->where(array('assignedpaper.Assignedto'=>$uid));
        $query=  $this->db->get();
        if($query->num_rows()>0){
            foreach ($query->result() as $row)
      {
        $data[] = $row;
      }
      return $data;
    }
    else{
        return FALSE;
    }
        
    }
    //assigned papers for reviwer
    function  GetAssignedPaperDetails($pid){
        $session_data = $this->session->userdata('logged_in');
        $id= $session_data['ConID'];
        $this->db->select('ID,Title,Abstract,Primarycontact');
        $this->db->from('paper');
        $this->db->where(array('ConID'=>$id));
         $this->db->where(array('ID'=>$pid));
          $query=  $this->db->get();
        if($query->num_rows()>0){
            foreach ($query->result() as $row)
      {
        $data[] = $row;
      }
      return $data;
    }
    else{
        return FALSE;
    }
    }
    //paper details for chair
    function GetpapersForUnassigned(){
        $session_data = $this->session->userdata('logged_in');
        $id= $session_data['ConID'];
        $this->db->select('assignedpaper.id,assignedpaper.pid ,user.UserName,user.Email,paper.Title');
        $this->db->from('assignedpaper');
         $this->  db->join('user', 'assignedpaper.Assignedto = user.id');
        $this->  db->join('paper', 'assignedpaper.pid = paper.ID');
        $this->db->where(array('assignedpaper.conid'=>$id));
        $this->db->where(array('assignedpaper.Active'=>'Y'));
        $query=  $this->db->get();
        if($query->num_rows()>0){
            foreach ($query->result() as $row)
      {
        $data[] = $row;
      }
      return $data;
    }
    else{
        return FALSE;
    }
        
    }
    //user user id for validation
    function RetUID($email){
         $session_data = $this->session->userdata('logged_in');
         $id= $session_data['ConID'];
        $this->db->select('id');
        $this->db->from('user');
        $this->db->where(array('Email'=>$email));
        $this->db->where(array('ConID'=>$id));
        $query = $this->db->get();
        return $query->row()->id; 
        
    }
    //validation
    function CheckPaperAssigned(){
              $Paper=$this->input->post('Paper');
              $Email=$this->input->post('email');
              $uid= $this->RetUID($Email);
              $pid= $this->GetPaperId($Paper);                
                $this->db->select('*');
                $this->db->from('assignedpaper');
                $this->db->where('pid',$pid);
                 $this->db->where('Active','y');
                 $this->db->where('Assignedto',$uid);
                $query = $this->db->get();
 
                if ($query->num_rows()>0)
                {
                return FALSE;
        }
 
        else
        {
                return TRUE;
        }
        }
        function UnassignPaper($id){
             $data = array(
               'Active' => 'N',
            );

        $this->db->where(array('id'=>$id));
        $this->db->update('assignedpaper', $data); 
        }
    
}

?>
