<?php
class AuthorPaperEdit_Model extends CI_Model{
     function getpaperdetails(){
     $session_data = $this->session->userdata('logged_in');
     $id= $session_data['id'];
     $Conid= $session_data['ConID'];
     $data = array();
     $this->db->select('ID,FileName,Title,Abstract,Primarycontact,Decission');
     $this->db->from('paper');
     $this->db->where(array('AuthorID'=>$id));
     $this->db->where(array('ConID'=>$Conid));
     $this->db->where(array('Status'=>'A'));
     $query=  $this->db->get();
    if($query->num_rows()>0){
        foreach ($query->result() as $row)
      {
        $data[] = $row;
      }
      return $data;
    }
    else{
        return FALSE;
    }
    }
    function Download($id){
       $this->db->select('FileName');
       $this->db->from('paper');
       $this->db->where(array('ID'=>$id));
       $query=$this->db->get(); 
       return $query->row()->FileName;
    }
    function Delete($id){
          $data = array(
               'Status' => 'D',
            );

    $this->db->where(array('id'=>$id));
    $this->db->update('paper', $data); 
        
    }
    
    function EditPaper($id){
     $session_data = $this->session->userdata('logged_in');
     $Conid= $session_data['ConID'];
     $data = array();
     $this->db->select('ID,FileName,Title,Abstract,Primarycontact');
     $this->db->from('paper');
     $this->db->where(array('ID'=>$id));
     $this->db->where(array('ConID'=>$Conid));
     $query=  $this->db->get();
    if($query->num_rows()>0){
        foreach ($query->result() as $row)
      {
        $data[] = $row;
      }
      return $data;
    }
    else{
        return FALSE;
    }
        
    }
    
    
    function GetPaperAuthors($pid){
     $session_data = $this->session->userdata('logged_in');
     $Conid= $session_data['ConID'];
     $data = array();
     $this->db->select('id, Email,pid,Phone,Name ');
     $this->db->from('author');
     $this->db->where(array('pid'=>$pid));
     $this->db->where(array('ConID'=>$Conid));
     $query=  $this->db->get();
    if($query->num_rows()>0){
        foreach ($query->result() as $row)
      {
        $data[] = $row;
      }
      return $data;
    }
    else{
        return FALSE;
    }
        
    }
    
    
    
    function GetPaperTracks($pid){
     $session_data = $this->session->userdata('logged_in');
     $Conid= $session_data['ConID'];
     $data = array();
     $this->db->select('id,track,pid,conid');
     $this->db->from('papertrack');
     $this->db->where(array('pid'=>$pid));
     $this->db->where(array('ConID'=>$Conid));
     $query=  $this->db->get();
    if($query->num_rows()>0){
        foreach ($query->result() as $row)
      {
        $data[] = $row;
      }
      return $data;
    }
    else{
        return $data;
    }
        
    }
    
    
    
    function GetPaperAuthor($id){
     $session_data = $this->session->userdata('logged_in');
    // $Conid= $session_data['ConID'];
     $data = array();
     $this->db->select('id, Email,pid,Phone,Name ');
     $this->db->from('author');
     $this->db->where(array('id'=>$id));
    // $this->db->where(array('ConID'=>$Conid));
     $query=  $this->db->get();
       if($query->num_rows()>0){
      foreach ($query->result() as $row)
      {
        $data[] = $row;
      }
      return $data;
    }
    else{
        return FALSE;
    }
        
    }
    
    
    function Edit(){
        $id=$this->input->post('id');
        $title=$this->input->post('title');
        $abstract=$this->input->post('abstract');
        $Primarycontact=$this->input->post('pc');
         $data = array(
               'Title' => $title,
               'Abstract' => $abstract,
               'Primarycontact'=>$Primarycontact
            );

    $this->db->where(array('ID'=>$id));
    $this->db->update('paper', $data); 
        
    }
    
    function EditPaperAuthor($details){
        $id=$details['authorList'];
                //$this->input->post('id');
        $name=$details['nameText'];
                //$this->input->post('name');
        $email=$details['emailText'];
                //$this->input->post('email');
        $number=$details['numberText'];
                //$this->input->post('number');
         $data = array(
               'Name' => $name,
               'Email' => $email,
               'Phone'=>$number
            );

         
         
    $this->db->where(array('id'=>$id));
    $this->db->update('author', $data); 
        
    }
    
    
     function DeletePaperTracks($pid) {
        $this->db->query("Delete FROM papertrack WHERE pid = ".$pid);
    }
    
    
    
    function getAuthorPaperDetails(){
     $session_data = $this->session->userdata('logged_in');
     $id= $session_data['id'];
     $Conid= $session_data['ConID'];
     $data = array();
     $query=  $this->db->query("SELECT p.ID,p.FileName,p.Title,p.Abstract,p.Primarycontact,p.Decission,c.commentchair, c.commentauthors FROM `paper`as p, `comment` as c  
                                WHERE p.AuthorID = ".$id." and p.conid = ".$Conid." and p.Status = 'A' and c.pid = p.ID AND p.ShowComment = true");
     $query1=  $this->db->query("SELECT p.ID,p.FileName,p.Title,p.Abstract,p.Primarycontact,p.Decission,'--' as commentchair ,'--' as commentauthors  FROM `paper`as p, `comment` as c  
                                WHERE p.AuthorID = ".$id." and p.conid = ".$Conid." and p.Status = 'A' and c.pid = p.ID AND p.ShowComment = FALSE");
    if($query->num_rows()>0 || $query1->num_rows()>0){
      foreach ($query->result() as $row)
      {
        $data[] = $row;
      }
      
      foreach ($query1->result() as $row1)
      {
        $data[] = $row1;
      }
      
      return $data;
    }
    else{
        return FALSE;
    }
    }
}

?>
