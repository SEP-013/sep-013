<?php

class Notes_Model extends CI_Model{
    function GetPaperTitle(){
    $session_data = $this->session->userdata('logged_in');            
    $id = $session_data['id'];
    $ConID = $session_data['ConID'];
    $this->db->select('Title');
    $this->db->from('paper');
    $this->db->where(array('AuthorID'=>$id));
    $this->db->where(array('ConID'=>$ConID));
    $query=$this->db->get(); 
    return $query->result();
    }
     function RetPaperID($Title) { 
           
           $this->db->select('ID'); 
           $this->db->from('paper'); 
           $this->db->where('Title',$Title); 
           $query = $this->db->get();
           return $query->row()->ID; 
     }
     function AddNotes(){
         $session_data = $this->session->userdata('logged_in');            
         $id = $session_data['id'];
         $ConID = $session_data['ConID'];
         $Title=$this->input->post('Title');
         $PID = $this->RetPaperID($Title);
         $Note=$this->input->post('Note');
         $data=array('CID'=>$ConID,'PID'=>$PID,'UID'=>$id,'Note'=>$Note,'Title'=>$Title);
         $this->db->insert('note',$data);
     }
     function GetNotes(){
        
        
    $session_data = $this->session->userdata('logged_in');            
    $id = $session_data['id'];
    $ConID = $session_data['ConID'];
    $this->db->select('id,Title,Note');
    $this->db->from('note');
    $this->db->where(array('UID'=>$id));
    $this->db->where(array('CID'=>$ConID));
    $query=$this->db->get(); 
    return $query->result();
                 
         
     }
     
     function DeleteNote($id){
         $this->db->where(array('id'=>$id));
         $this->db->delete('note'); 
     }
        
}
?>
