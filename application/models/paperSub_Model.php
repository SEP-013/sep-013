<?php

class PaperSub_Model extends CI_Model{
     function getConName(){  
     $session_data = $this->session->userdata('logged_in');
     $id= $session_data['ConID'];
     $data = array();
    $this->db->select('PaperSub');
    $this->db->from('deadline');
    $this->db->where(array('ConID'=>$id));
    $query=$this->db->get(); 
    return $query->row()->PaperSub;
    }
     public function insert_file($filename,$title, $abstract,$author,$pcontact,$track)
    {
      $session_data = $this->session->userdata('logged_in');	
      $ConID = $session_data['ConID'];
     
    $data = array('Filename' => $filename,
            'Title'         => $title,
             'Abstract'      =>$abstract,
             'Author'        =>$author,
             //'Track'     =>$track,
             'Primarycontact'=>$pcontact,
             'ConID'   =>$ConID);
              
       $res= $this->db->insert('paper', $data);
        //return $this->db->insert_id();
        
        return $res;
        
  }
  function GetTrack(){
   $session_data = $this->session->userdata('logged_in');
     $id= $session_data['ConID'];
     $data = array();
    $this->db->select('id,trackname');
    $this->db->from('track');
 
    $this->db->where(array('ConID'=>$id));
    $query=  $this->db->get();
    if($query->num_rows()>0){
        foreach ($query->result() as $row)
      {
        $data[] = $row;
      }
      return $data;
    }
    else{
        return FALSE;
    }
  }
}
?>
