<?php
class SiteSetup_Model extends CI_Model{
   function update(){
      $session_data = $this->session->userdata('logged_in');	
      $ConID = $session_data['ConID'];
      
	  $bid=$this->input->post('bid');
	  $bid1=$this->input->post('bid1');
	  $bid2=$this->input->post('bid2');
	  $bid3=$this->input->post('bid3');
	  $bid4=$this->input->post('bid4');
	
			$papsub=$this->input->post('from');
		$papsubend=$this->input->post('datetimepicker');
	  
		  if(($papsub)<($papsubend)){
	   $data = array(
				'PaperSub'=>$this->input->post('datetimepicker'),
				'fromPaperSub'=>$this->input->post('from'),
				'status'=>$this->input->post('bid')
					          );
	  $this->db->where('ConID',$ConID);
	  $this->db->update('deadline',$data); 
           $this->session->set_flashdata('feedback', '<div class="alert alert-success" <a class="close pull-right" data-dismiss="alert">× </a>Deadline updated Successfully for the submission</div>');
          
	  } 
          elseif(($papsub)>($papsubend))
              {
                         $this->session->set_flashdata('feedback', '<div class="alert alert-danger" <a class="close pull-right" data-dismiss="alert">× </a>paper submission end date less than start date</div>');
              }
	  
		 
	  
		     $editpap= $this->input->post('from1');
		$editpapend= $this->input->post('datetimepicker-1');
	 
                if($editpap<$editpapend){
			  if($editpap>$papsub){
	   			$data = array(
	            'EditPaper'=>$this->input->post('datetimepicker-1'),
				'fromEditPaper'=>$this->input->post('from1'),
				'statusedit'=>$this->input->post('bid1')	          );
				  $this->db->where('ConID',$ConID);
				  $this->db->update('deadline',$data);
                              $this->session->set_flashdata('feedback1', '<div class="alert alert-success" <a class="close pull-right" data-dismiss="alert">× </a>Deadline updated for the edit submission</div>');
					  }
                            elseif($editpap<$papsub)
                                {
                                $this->session->set_flashdata('feedback1', '<div class="alert alert-danger" <a class="close pull-right" data-dismiss="alert">× </a>edit submission date less than papersubmission</div>');
                                }
                              }
                  elseif($editpap>$editpapend)
                                {
                                $this->session->set_flashdata('feedback1', '<div class="alert alert-danger" <a class="close pull-right" data-dismiss="alert">× </a>edit submission end date less than start date</div>');
                                }
	  

	  
			$reviewpap= $this->input->post('from2');
		$reviewpapend= $this->input->post('datetimepicker-2');

                if($reviewpap<$reviewpapend){
				  if($reviewpap>$papsub){
	   							$data = array(
	           					 'ReviewSub'=>$this->input->post('datetimepicker-2'),
								'fromReviewSub'=>$this->input->post('from2'),
								'statusreview'=>$this->input->post('bid2')	          );
								  $this->db->where('ConID',$ConID);
								  $this->db->update('deadline',$data);
                                                                 $this->session->set_flashdata('feedback2', '<div class="alert alert-success" <a class="close pull-right" data-dismiss="alert">× </a>Deadline updated for the review submission</div>');

					  }
                                  elseif($reviewpap<$papsub)
                                      {
                                        $this->session->set_flashdata('feedback2', '<div class="alert alert-danger" <a class="close pull-right" data-dismiss="alert">× </a>review submission date less than papersubmission</div>');
                                      }
	  		}
              elseif($reviewpap>$reviewpapend)
                  {
                       $this->session->set_flashdata('feedback2', '<div class="alert alert-danger" <a class="close pull-right" data-dismiss="alert">× </a>review submission end date less than start date</div>');

                  }
	
	  
	    	$bidpap= $this->input->post('from3');
		$bidpapend= $this->input->post('datetimepicker-3');
	 
		  	if($bidpap<$bidpapend){
			 		 if($bidpap>$papsub){
								   $data = array(
	            					'ReviewBid'=>$this->input->post('datetimepicker-3'),
									'fromReviewBid'=>$this->input->post('from3'),
									'statusbid'=>$this->input->post('bid3')		          );
									  $this->db->where('ConID',$ConID);
									  $this->db->update('deadline',$data);
                                                                          $this->session->set_flashdata('feedback3', '<div class="alert alert-success" <a class="close pull-right" data-dismiss="alert">× </a>Deadline updated for the bid submission</div>');

					  }
                                          elseif($bidpap<$papsub)
                                              {
                                                  $this->session->set_flashdata('feedback3', '<div class="alert alert-danger" <a class="close pull-right" data-dismiss="alert">× </a>bid submission date less than papersubmission</div>');

                                              }
                                }
                            elseif($bidpap>$bidpapend)
                                {
                                            $this->session->set_flashdata('feedback3', '<div class="alert alert-danger" <a class="close pull-right" data-dismiss="alert">× </a>bid submission end date less than start date</div>');

                                }
			
	  
	
	  
	    	$campap= $this->input->post('from4');
		$campapend= $this->input->post('datetimepicker-4');
	  
		  if($campap<$campapend){
				  if(($campap>$bidpap)&& ($campap>$reviewpap) && ($campap>$editpap)){
					  		if(($campapend>$bidpapend) && ($campapend>$reviewpapend) && ($campapend>$editpapend) && ($campapend>$papsubend)){
	   						$data = array(
	            				'CamSub'=>$this->input->post('datetimepicker-4'),
								'fromCamSub'=>$this->input->post('from4'),
								'statuscam'=>$this->input->post('bid4')	          );
								  $this->db->where('ConID',$ConID);
								  $this->db->update('deadline',$data); 
                                                              $this->session->set_flashdata('feedback4', '<div class="alert alert-success" <a class="close pull-right" data-dismiss="alert">× </a>Deadline updated for the Camera ReadySubmission</div>');

                                                        }
                                                        elseif(($campapend<$bidpapend) || ($campapend<$reviewpapend) || ($campapend<$editpapend) || ($campapend<$papsubend))
                                                        {
                                                          $this->session->set_flashdata('feedback4', '<div class="alert alert-danger" <a class="close pull-right" data-dismiss="alert">× </a>make sure that Camera Ready Submission end date is greater than all end dates</div>');

                                                        }
                                        
                                     
                                   }
                                   elseif(($campap<$bidpap) || ($campap<$reviewpap) || ($campap<$editpap))
                                   {
                                        $this->session->set_flashdata('feedback4', '<div class="alert alert-danger" <a class="close pull-right" data-dismiss="alert">× </a>make sure that Camera Ready Submission start date is greater than all start dates</div>');
   
                                   }
	  	}
                elseif($campap>$campapend)
                {
                $this->session->set_flashdata('feedback4', '<div class="alert alert-danger" <a class="close pull-right" data-dismiss="alert">× </a>camera ready submission end date less than start date</div>');
                }

	  
   }
   function getdeadline(){
        $session_data = $this->session->userdata('logged_in');	
        $ConID = $session_data['ConID'];
        $data = array();
        $this->db->select('PaperSub,EditPaper,ReviewSub,ReviewBid,CamSub,fromPaperSub,fromEditPaper,fromReviewSub,fromReviewBid,fromCamSub,status,statusedit,statusreview,statusbid,statuscam');
		
        $this->db->from('deadline');
        $this->db->where(array('ConID'=>$ConID));
            $query=  $this->db->get();
        if($query->num_rows()>0){
        foreach ($query->result() as $row)
        {
            $data[] = $row;
        }
        return $data;
        }
        else{
            return FALSE;
        }
   }
   function getConDetails(){
        $session_data = $this->session->userdata('logged_in');	
        $ConID = $session_data['ConID'];
          $data = array();
        $this->db->select('ConName,ConDate,SubArea');
        $this->db->from('conferencereq');
        $this->db->where(array('id'=>$ConID));
            $query=  $this->db->get();
        if($query->num_rows()>0){
        foreach ($query->result() as $row)
        {
            $data[] = $row;
        }
        return $data;
        }
        else{
            return FALSE;
        }
   }
           function getAuthorDeadLine(){
       $session_data = $this->session->userdata('logged_in');	
        $ConID = $session_data['ConID'];
          $data = array();
        $this->db->select('PaperSub,EditPaper');
        $this->db->from('deadline');
        $this->db->where(array('ConID'=>$ConID));
            $query=  $this->db->get();
        if($query->num_rows()>0){
        foreach ($query->result() as $row)
        {
            $data[] = $row;
        }
        return $data;
        }
        else{
            return FALSE;
        }
   }
   function Addtrack(){
 $numFields = count($_POST['fields']['track']);
 $session_data = $this->session->userdata('logged_in');      
 $ConID = $session_data['ConID'];
 

 for ($i = 0; $i < $numFields; $i++) {
   
    
    
    $field = array(
       'trackname' => $_POST['fields']['track'][$i],
        'ConID' =>$ConID
        
       
       
    );

  
    $this->db->insert('track',$field);
}
   }
}

?>
