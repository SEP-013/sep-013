<?php

class File_model extends CI_Model{
   
    public function insert_file($filename,$title, $abstract,$primarycontact)
    {
       $session_data = $this->session->userdata('logged_in');	
      $ConID = $session_data['ConID'];
      $author=$session_data['id'];
      $upload_path='application/views/folder/';
      
     
    $data = array('FileName' => $filename,
            'Title'         => $title,
             'Abstract'      =>$abstract,
             'AuthorID'        =>$author,
              'Primarycontact' => $this->input->post('primarycontact'),
              'Status'=>'A',
              'Decission'=>'P',
              'Path'=> $upload_path,
              'ConID'   =>$ConID);
              
    $res= $this->db->insert('paper', $data);
        //return $this->db->insert_id();
      
        return $res;
        
  }
  function Insert_Track(){
      $session_data = $this->session->userdata('logged_in');	
      $ConID = $session_data['ConID'];
      $numFields = count($_POST['fields']['track']);
      $title   =   $this->input->post('title');
      $pid=  $this->SelectPaperID($title);
       for ($i = 0; $i < $numFields; $i++) {
             $data = array(
               'track'=>$_POST['fields']['track'][$i],
                'pid'=>$pid,
                 'conid'=>$ConID
          ); 
          $this->db->insert('papertrack', $data);   
       }
          
      
      
  }
  
  function MultipleAuthors(){
 $numFields = count($_POST['fields']['stop']);
 $session_data = $this->session->userdata('logged_in');      
 $ConID = $session_data['ConID'];
 $title   =   $this->input->post('title');
 $pid=  $this->SelectPaperID($title);
 for ($i = 0; $i < $numFields; $i++) {
   
    // Pack the field up in an array for ease-of-use.
    
    $field = array(
       'name' => $_POST['fields']['stop'][$i],
        'email' => $_POST['fields1']['email'][$i],
         'phone' => $_POST['fields2']['num'][$i],
        'ConID' =>$ConID,
        'pid' =>$pid
       
       
    );

  
    $this->db->insert('author',$field);
}
}
function SelectPaperID($Title){
    $session_data = $this->session->userdata('logged_in');
    $id= $session_data['ConID'];
    $this->db->select('ID');
    $this->db->from('paper');
    $this->db->where(array('Title'=>$Title));
    $this->db->where(array('ConID'=>$id));
    $query = $this->db->get();
    return $query->row()->ID; 
}
    
      }
