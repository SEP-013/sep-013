<?php
class View_statistics_Model extends CI_Model{
    function getstatistics(){
     $session_data = $this->session->userdata('logged_in');
     $id= $session_data['ConID'];
    $data = array();
    $this->db->select('id,UserName,UserType');
    $this->db->from('user');
    $this->db->where(array('ConID'=>$id));
//	$this->db->where(array('UserType'=>'2'));
//$this->db->or_where(array('UserType'=>'3'));

    $query=  $this->db->get();
    if($query->num_rows()>0){
        foreach ($query->result() as $row)
      {
        $data[] = $row;
      }
      return $data;
    }
    else{
        return FALSE;
    }
	
	}
	
	//for reviewed 
	function getreviewed($id){
     $session_data = $this->session->userdata('logged_in');
     $ConId= $session_data['ConID'];
    $data = array();
    $this->db->select('id,cid,bids,uid,pid');
    $this->db->from('bid');
    $this->db->where(array('cid'=>$ConId));
	$this->db->where(array('uid'=>$id));
    $query=$this->db->get();
    if($query->num_rows()>0){
        foreach ($query->result() as $row)
      {
        $data[] = $row;
      }
      return $data;
    }
    else{
        return FALSE;
    }
    }
	
	//for paper deleted
	function getDetetedPapers($id){
     $session_data = $this->session->userdata('logged_in');
     $ConId= $session_data['ConID'];
    $data = array();
    $this->db->select('ID,FileName,Title,Decission');
    $this->db->from('paper');
    $this->db->where(array('ConID'=>$ConId));
	$this->db->where(array('AuthorID'=>$id));
     $this->db->where(array('Status'=>'D'));
    $query=  $this->db->get();
    if($query->num_rows()>0){
        foreach ($query->result() as $row)
      {
        $data[] = $row;
      }
      return $data;
    }
    else{
        return FALSE;
    }
    }
	
	//for paper uploaded
	function getauthorupload($id){
     $session_data = $this->session->userdata('logged_in');
     $ConId= $session_data['ConID'];
    $data = array();
    $this->db->select('ID,Title,FileName,Decission');
    $this->db->from('paper');
    $this->db->where(array('ConID'=>$ConId));
	$this->db->where(array('AuthorID'=>$id));
	$this->db->where(array('Status'=>'A'));
    $query=  $this->db->get();
    if($query->num_rows()>0){
        foreach ($query->result() as $row)
      {
        $data[] = $row;
      }
      return $data;
    }
    else{
        return FALSE;
    }
	
	}
	
	function getpaper(){
     $session_data = $this->session->userdata('logged_in');
     $ConId= $session_data['ConID'];
    $data = array();
    $this->db->select('ID,ConID,FileName,Title,Status');
    $this->db->from('paper');
    $this->db->where(array('ConID'=>$ConId));
    $query=$this->db->get();
    if($query->num_rows()>0){
        foreach ($query->result() as $row)
      {
        $data[] = $row;
      }
      return $data;
    }
    else{
        return FALSE;
    }
    }
	
    }
?>